# Spencer's Coding Projects, Experience, and Background #

This repository contains many of the projects I've worked on since I began coding.


## Computer Science ##
I've taken three classes in C++: BYU's CS 142 (Introduction), CS 235 (Data Structures), and CS 236 (Discrete Mathematics). 
In my most recent CS class, CS 240 (Advanced Programming) I'm learning Java. See the java_projects folder for some of the 
projects I've completed in this class.

## Machine Learning ##
I joined a Machine Learning Research group at BYU the summer of 2017. This group exclusively uses python. At the time, I had
little programming experience and I found there was a steep learning curve. My latest projects, which apply recent advances in deep learning to problems in low level topology, can 
be found in the python_projects/Research folder.


## Applied and Computational Mathematics ##
I began BYU's Applied and Computational Mathematical Emphasis (ACME) major in the fall of 2018. For an overview see

http://www.acme.byu.edu/

Among other things, the ACME major emphasizes programming in python. The projects I've completed so far can be viewed in the 
python_projects/ACME folder.


## Coursera ##
I've also completed a few online classes through Coursera, an subscription-based online learning environment with courses from
both Standford, the University of Michigan, and the National Research University Higher School of Economics. These courses have 
mainly focused on Machine Learning and Data Science. Links to my completed courses and their corresponding course certificates 
are given below.

### Introduction to Deep Learning ###
National Research University Higher School of Economics

https://www.coursera.org/learn/intro-to-deep-learning
https://www.coursera.org/account/accomplishments/certificate/U8ED4BUK77U8

### Introduction to Data Science in Python ###
University of Michigan

https://www.coursera.org/learn/python-data-analysis
https://www.coursera.org/account/accomplishments/certificate/LK2QQGWLNW66

### Machine Learning ###
Standford University

https://www.coursera.org/learn/machine-learning
https://www.coursera.org/account/accomplishments/certificate/BH4CEMEKRVWE

### Convolutional Neural Networks ###
deeplearning.ai

https://www.coursera.org/learn/convolutional-neural-networks?specialization=deep-learning
https://www.coursera.org/account/accomplishments/certificate/GE44A3MN2ZF8

### Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization ###
deeplearning.ai

https://www.coursera.org/learn/deep-neural-network?specialization=deep-learning
https://www.coursera.org/account/accomplishments/certificate/47AL82GRGFS6

### Neural Networks and Deep Learning ###
deeplearning.ai

https://www.coursera.org/learn/neural-networks-deep-learning?specialization=deep-learning
https://www.coursera.org/account/accomplishments/certificate/KS34E3V22HCC