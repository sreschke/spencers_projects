import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class PPM {
    private int width;
    private int height;
    private int max_val;
    private int[][][] image;


    public PPM(File ppm_file) throws IOException {
        /*PPM Constructor. Parses file and stores values for members width, height, max_val, and image*/
        
        // Create scanner for parsing
        FileInputStream f_input_stream = new FileInputStream(ppm_file);
        BufferedInputStream b_input_stream = new BufferedInputStream(f_input_stream);
        Scanner scanner = new Scanner(b_input_stream);

        String token = null;

        //Parse P3
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        System.out.println(token);
            
        //Parse width
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.width = Integer.parseInt(token);
        System.out.println("Width " + width);

        //Parse height
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.height = Integer.parseInt(token);
        System.out.println("Length " + height);

        //Parse max color value
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.max_val = Integer.parseInt(token);
        System.out.println("Max val " + max_val);

        //parse pixel values
        image = new int[width][height][3];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < 3; z++) {
                    token = scanner.next();
                    while (token.equals("#")) {
                        System.out.println("found a #");
                        scanner.nextLine();
                        token = scanner.next();
                    }
                    image[x][y][z] = Integer.parseInt(token);
                }
            }
        } 
        
    }
}