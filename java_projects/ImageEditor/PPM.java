import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;

public class PPM {
    private int width;
    private int height;
    private int max_val;
    private int[][][] image;
    private static int[][][] original_image;


    public PPM(File ppm_file) throws IOException {
        /*PPM Constructor. Parses file and stores values for members width, height, max_val, and image*/
        
        // Create scanner for parsing
        FileInputStream f_input_stream = new FileInputStream(ppm_file);
        BufferedInputStream b_input_stream = new BufferedInputStream(f_input_stream);
        Scanner scanner = new Scanner(b_input_stream);

        String token = null;

        //Parse P3
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        //System.out.println(token);
            
        //Parse width
        token = scanner.next();
        while (token.equals("#")) {
            //System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.width = Integer.parseInt(token);
        //System.out.println("Width " + width);

        //Parse height
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.height = Integer.parseInt(token);
        //System.out.println("Length " + height);

        //Parse max color value
        token = scanner.next();
        while (token.equals("#")) {
            System.out.println("found a #");
            scanner.nextLine();
            token = scanner.next();
        }
        this.max_val = Integer.parseInt(token);
        //System.out.println("Max val " + max_val);

        //parse pixel values
        image = new int[width][height][3];
        original_image = new int[width][height][3];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                for (int z = 0; z < 3; z++) {
                    token = scanner.next();
                    while (token.equals("#")) {
                        System.out.println("found a #");
                        scanner.nextLine();
                        token = scanner.next();
                    }
                    image[x][y][z] = Integer.parseInt(token);
                    original_image[x][y][z] = Integer.parseInt(token);
                }
            }
        } 
        
    }

    public void invert() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < 3; z++) {
                    image[x][y][z] = max_val - image[x][y][z];
                }
            }
        } 
    }


    public void grayscale() {
        int ave_val = -1;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                ave_val = (image[x][y][0] + image[x][y][1] + image[x][y][2]) / 3;
                image[x][y][0] = ave_val;
                image[x][y][1] = ave_val;
                image[x][y][2] = ave_val;
            }
        }     
    }


    public void emboss() {
        int half_max = 128;
        int red_diff;
        int green_diff;
        int blue_diff;
        int v;
 
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (x == 0 || y == 0) {
                    image[x][y][0] = half_max;
                    image[x][y][1] = half_max;
                    image[x][y][2] = half_max;
                } else { 
                    red_diff = image[x][y][0] - original_image[x-1][y-1][0];
                    green_diff = image[x][y][1] - original_image[x-1][y-1][1];
                    blue_diff = image[x][y][2] - original_image[x-1][y-1][2];

                    if (java.lang.Math.abs(red_diff) >= java.lang.Math.abs(green_diff) & java.lang.Math.abs(red_diff) >= java.lang.Math.abs(blue_diff)) {
                        v = red_diff + half_max;
                    } else if (java.lang.Math.abs(green_diff) >= java.lang.Math.abs(blue_diff)) {
                        v = green_diff + half_max;
                    } else {
                        v = blue_diff + half_max;
                    }
                    if (v < 0) {
                        v = 0;
                    }
                    if (v > max_val) {
                        v = max_val;
                    }
                    image[x][y][0] = v;
                    image[x][y][1] = v;
                    image[x][y][2] = v;                    
                }
            }
        } 
    }

    public void motionblur(int n) {
        int red_tot = 0;
        int green_tot = 0;
        int blue_tot = 0;
        int num_pix = 0;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (x + n > width) {
                    num_pix = width - x;
                } else {
                    num_pix = n;
                }
                red_tot = 0;
                green_tot = 0;
                blue_tot = 0;
                for (int i = 0; i < num_pix; i++) {
                    red_tot += original_image[x + i][y][0];
                    green_tot += original_image[x + i][y][1];
                    blue_tot += original_image[x + i][y][2];
                }
                image[x][y][0] = red_tot/num_pix;
                image[x][y][1] = green_tot/num_pix;
                image[x][y][2] = blue_tot/num_pix;               
            }
        } 
        
    }

    public void writeToFile(String out_name) throws IOException {
        /*Writes the data stored in PPM object to a ppm file with name out_name*/

        PrintWriter pw = null;
	FileWriter fw = null;
	fw = new FileWriter(out_name);
	BufferedWriter bw = new BufferedWriter(fw);
	pw = new PrintWriter(bw);
        
        pw.println("P3");
        pw.println(this.width + " " + this.height);
        pw.println(this.max_val);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                for (int z = 0; z < 3; z++) {
                    pw.println(image[x][y][z]);
                }
            }
        } 
        pw.close();
    }
}