import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ImageEditor {
    public static void main(String[] args) throws IOException{
        if ((args.length < 3) ||
            (args.length > 4) || 
            (!args[2].equals("grayscale") & !args[2].equals("invert") & !args[2].equals("emboss") & !args[2].equals("motionblur"))) {
            System.out.println("USAGE: java ImageEditor in-file out-file (grayscale|invert|emboss|motionblur motion-blur-length)");
            System.exit(0);            
        }
        int n = 0;
        if (args.length == 4) {
            if ((Integer.parseInt(args[3]) <= 0) || (!args[2].equals("motionblur"))) {
                System.out.println("USAGE: java ImageEditor in-file out-file (grayscale|invert|emboss|motionblur motion-blur-length)");
                System.exit(0);    
            }
            n = Integer.parseInt(args[3]);            
        }
        if (args[2].equals("motionblur")) {
            if ((args.length < 4) || (Integer.parseInt(args[3]) <= 0)) {
                System.out.println("USAGE: java ImageEditor in-file out-file (grayscale|invert|emboss|motionblur motion-blur-length)");
                System.exit(0);
            }            
        }        
        //Store file names from command line
        String in_file_name = args[0];
        String out_file_name = args[1];
        String func_to_call = args[2];

        //parse ppm file
        FileInputStream f_input_stream = null;
        File in_file = new File(in_file_name);
        PPM ppm = new PPM(in_file);

        if (func_to_call.equals("grayscale")) {
            ppm.grayscale();
        } else if (func_to_call.equals("invert")) {
            ppm.invert();
        } else if (func_to_call.equals("emboss")) {
            ppm.emboss();
        } else if (func_to_call.equals("motionblur")) {
            ppm.motionblur(n);
        } else {
            ;//FIXME throw error
        }

        ppm.writeToFile(out_file_name);

        //System.out.println("Finished!");
    }
}
