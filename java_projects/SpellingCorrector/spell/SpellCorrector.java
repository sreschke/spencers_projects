package spell;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SpellCorrector implements ISpellCorrector {
    public Trie trie = new Trie();

    /**
     * Constructor
     */
    public SpellCorrector() {
    }
    /**
     * Tells this <code>SpellCorrector</code> to use the given file as its dictionary
     * for generating suggestions.
     * @param dictionaryFileName File containing the words to be used
     * @throws IOException If the file cannot be read
     */
    public void useDictionary(String dictionaryFileName) throws IOException {
        trie.readDictionary(dictionaryFileName);
    }

    /**
     * Suggest a word from the dictionary that most closely matches
     * <code>inputWord</code>
     * @param inputWord
     * @return The suggestion or null if there is no similar word in the dictionary
     */
    public String suggestSimilarWord(String inputWord) {
        ITrie.INode node;
        String suggestion;
        String curWord;
        if (trie.find(inputWord) != null) {
            return inputWord.toLowerCase();
        }
        List<String> edit1 = getEditDistance1InDict(inputWord);
        if (edit1.size() > 0) {
            int max_times = 0;
            int count;
            suggestion = edit1.get(0);
            for (int i = 0; i < edit1.size(); i++) {
                curWord = edit1.get(i);
                node = trie.find(curWord);
                count = node.getValue();
                if (count > max_times) {
                    max_times = count;
                    suggestion = curWord;
                }
            }
            return suggestion;
        }
        else {
            List<String> edit2 = getEditDistance2InDict(inputWord);
            int max_times = 0;
            int count;
            suggestion = edit2.get(0);
            for (int i = 0; i < edit2.size(); i++) {
                curWord = edit2.get(i);
                node = trie.find(curWord);
                count = node.getValue();
                if (count > max_times) {
                    max_times = count;
                    suggestion = curWord;
                }
            }
            return suggestion;
        }
    }

    /**
     * Creates a list of all the words with deletion distance 1 that can be generated from
     * inputWord
     * @param inputWord
     * @return The list of all words with a deletion distance 1 from inputWord
     */
    public String[] getDeletionDistance1(String inputWord) {
        if (inputWord.length() == 0 || inputWord.length() == 1) {
            return null;
        }
        inputWord = inputWord.toLowerCase();
        String[] result = new String[inputWord.length()];
        StringBuilder sb;
        for (int i = 0; i < inputWord.length(); i++) {
            sb = new StringBuilder(inputWord);
            result[i] = sb.deleteCharAt(i).toString();
        }
        return result;
    }

    /**
     * Creates a list of all the words with transposition distance 1 that can be generated from
     * inputWord
     * @param inputWord
     * @return The list of all words with a transposition distance 1 from inputWord
     */
    public String[] getTranspositionDistance1(String inputWord) {
        if (inputWord.length() == 0 || inputWord.length() == 1) {
            return null;
        }
        inputWord = inputWord.toLowerCase();
        String[] result = new String[inputWord.length()-1];
        StringBuilder sb;
        for (int i = 0; i < inputWord.length()-1; i++) {
            sb = new StringBuilder(inputWord);
            char temp = sb.charAt(i);
            sb.setCharAt(i, sb.charAt(i+1));
            sb.setCharAt(i+1, temp);
            result[i] = sb.toString();
        }
        return result;
    }

    /**
     * Creates a list of all the words with alteration distance 1 that can be generated from
     * inputWord
     * @param inputWord
     * @return The list of all words with a alteration distance 1 from inputWord
     */
    public String[] getAlterationDistance1(String inputWord) {
        inputWord = inputWord.toLowerCase();
        String[] result = new String[inputWord.length()*25];
        StringBuilder sb;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int index = 0;
        for (int i = 0; i < inputWord.length(); i++) {
            for (int j = 0; j < 26; j++) {
                sb = new StringBuilder(inputWord);
                char curChar = sb.charAt(i);
                if (curChar == alphabet.charAt(j)) {
                    continue;
                } else {
                    sb.setCharAt(i, alphabet.charAt(j));
                    result[index] = sb.toString();
                    index++;
                }
            }
        }

        return result;
    }

    /**
     * Creates a list of all the words with insertion distance 1 that can be generated from
     * inputWord
     * @param inputWord
     * @return The list of all words with a insertion distance 1 from inputWord
     */
    public String[] getInsertionDistance1(String inputWord) {
        inputWord = inputWord.toLowerCase();
        String[] result = new String[(inputWord.length()+1)*26];
        StringBuilder sb;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int index = 0;
        for (int i = 0; i < inputWord.length()+1; i++) {
            for (int j = 0; j < 26; j++) {
                sb = new StringBuilder(inputWord);
                if (i == inputWord.length()+1) {
                    sb.append(alphabet.charAt(j));
                }
                else {
                    sb.insert(i, alphabet.charAt(j));
                }
                result[index] = sb.toString();
                index++;
            }
        }

        return result;
    }

    /**
     * Gets all the edit distance 1 words generated from inputWord.
     * @param inputWord
     * @return a string array containing all the edit distance 1 words generated from inputWord
     */
    public String[] getEditDistance1(String inputWord) {
        if (inputWord.length() > 1) {

            inputWord = inputWord.toLowerCase();
            String[] delDis = getDeletionDistance1(inputWord);
            String[] trsDis = getTranspositionDistance1(inputWord);
            String[] altDis = getAlterationDistance1(inputWord);
            String[] insDis = getInsertionDistance1(inputWord);

            int size = delDis.length + trsDis.length + altDis.length + insDis.length;
            String[] result = new String[size];

            int index = 0;
            for (int i = 0; i < delDis.length; i++) {
                result[index] = delDis[i];
                index++;
            }
            for (int i = 0; i < trsDis.length; i++) {
                result[index] = trsDis[i];
                index++;
            }
            for (int i = 0; i < altDis.length; i++) {
                result[index] = altDis[i];
                index++;
            }
            for (int i = 0; i < insDis.length; i++) {
                result[index] = insDis[i];
                index++;
            }
            return result;
        }
        else { //input word is a single letter
            inputWord = inputWord.toLowerCase();
            String[] altDis = getAlterationDistance1(inputWord);
            String[] insDis = getInsertionDistance1(inputWord);

            int size = altDis.length + insDis.length;
            String[] result = new String[size];

            int index = 0;
            for (int i = 0; i < altDis.length; i++) {
                result[index] = altDis[i];
                index++;
            }
            for (int i = 0; i < insDis.length; i++) {
                result[index] = insDis[i];
                index++;
            }
            return result;
        }
    }

        /**
         * Gets all the edit distance 1 words generated from inputWord that are in trie
         * @param inputWord
         * @return a string array containing all the edit distance 1 words generated from inputWord that
         * are in trie. The list is returned in alphabetical order.
         */
        public List<String> getEditDistance1InDict(String inputWord) {
            String[] edit1 = getEditDistance1(inputWord);
            List<String> inDict = new ArrayList<>();
            String word;
            for (int i = 0; i < edit1.length; i++) {
                word = edit1[i];
                if (trie.find(word) != null) {
                    inDict.add(word);
                }
            }
            java.util.Collections.sort(inDict);
            return inDict;
        }

    //    /**
    //     * Gets all the edit distance 1 words generated from inputWord that are in trie
    //     * @param inputWord
    //     * @return a string array containing all the edit distance 1 words generated from inputWord that
    //     * are in trie. The list is returned in alphabetical order.
    //     */
    //    public TreeMap<String, Integer> getEditDistance1InDict(String inputWord) {
    //        String[] edit1 = getEditDistance1(inputWord);
    //        TreeMap inDict = new TreeMap<>();
    //        String word;
    //        SpellingCorrector.ITrie.INode node;
    //        for (int i = 0; i < edit1.length; i++) {
    //            word = edit1[i];
    //            node = trie.find(word);
    //            if (node != null) {
    //                inDict.put(word, node.getValue());
    //            }
    //        }
    //        return inDict;
//    }

    public List<String> getEditDistance2(String inputWord) {
        String[] edit1 = getEditDistance1(inputWord);
        List<String> edit2 = new ArrayList<>();
        String[] temp;
        String curWord;
        for (int i = 0; i < edit1.length; i++) {
            curWord = edit1[i];
            temp = getEditDistance1(curWord);
            for (int j = 0; j < temp.length; j++) {
                edit2.add(temp[j]);
            }
        }
        return edit2;
    }

    public List<String> getEditDistance2InDict(String inputWord) {
        List<String> edit2 = getEditDistance2(inputWord);
        List<String> inDict = new ArrayList<>();
        String word;
        for (int i = 0; i < edit2.size(); i++) {
            word = edit2.get(i);
            if (trie.find(word) != null) {
                inDict.add(word);
            }
        }
        java.util.Collections.sort(inDict);
        return inDict;

        //FIXME
    }
}
