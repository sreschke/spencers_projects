package spell;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Trie implements ITrie {

    private Node root = new Node();
    private int word_count = 0;
    private int node_count = 1;

    /**
     * Trie constructor
     */
    public Trie() {}
    /**
     Reads the words in in_file_name and adds them to the Trie*/
    public void readDictionary(String in_file_name) throws IOException {
        File in_file = new File(in_file_name);
        FileInputStream f_input_stream = new FileInputStream(in_file);
        BufferedInputStream b_input_stream = new BufferedInputStream((f_input_stream));
        Scanner scanner = new Scanner(b_input_stream);
        String token;

        while (scanner.hasNext()) {
            token = scanner.next();
            if (isWord(token)) {
                add(token.toLowerCase());
            }
        }
    }

    /**
     Checks whether in_string is a word (i.e. whether in_string is a sequence of 1 or more
     alphabetic characters) returns true if so, returns false otherwise*/
    public boolean isWord(String in_string) {
        char[] chars = in_string.toCharArray();
        for (char c: chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds the specified word to the trie (if necessary) and increments the word's frequency count
     * @param word The word being added to the trie
     */
    public void add(String word) {
        add_helper(root, word);
    }

    /**
     * recursive helper function for add()
     * @param node
     * @param word
     */
    public void add_helper(Node node, String word) {
        char current_char = word.charAt(0);
        int a_index = (int) 'a';
        int index = (int) current_char - a_index;
        if (node.nodes[index] == null) {
            node.nodes[index] = new Node();
            node.hasChildren = true;
            node_count++;
        }
        if (word.length() == 1) {
            node.nodes[index].count++;
            if (node.nodes[index].count == 1) {
                word_count++;
            }
            return;
        }
        word = word.substring(1);
        add_helper(node.nodes[index], word);
    }

    /**
     * Searches the trie for the specified word
     *
     * @param word The word being searched for
     *
     * @return A reference to the trie node that represents the word,
     * 			or null if the word is not in the trie
     */
    public INode find(String word) {
        return find_helper(root, word.toLowerCase());
    }

    /**
     * recursive function called by find() function
     * @param node
     * @param word
     * @return
     */
    public INode find_helper(Node node, String word) {
        char current_char = word.charAt(0);
        int a_index = (int) 'a';
        int index = (int) current_char - a_index;
        if (node.nodes[index] == null) {
            return null;
        }
        if (word.length() == 1) {
            if (node.nodes[index].count > 0) {
                return node.nodes[index];
            }
            else {
                return null;
            }
        }
        else {
            word = word.substring(1);
            return find_helper(node.nodes[index], word);
        }
    }

    /**
     * Returns the number of unique words in the trie
     *
     * @return The number of unique words in the trie
     */
    public int getWordCount() {
        return word_count;
    }

    /**
     * Returns the number of nodes in the trie
     *
     * @return The number of nodes in the trie
     */
    public int getNodeCount() {
        return node_count;
    }

    /**
     * The toString specification is as follows:
     * For each word, in alphabetical order:
     * <word>\n
     */
    @Override
    public String toString() {
        StringBuilder curWord = new StringBuilder();
        StringBuilder output = new StringBuilder();
        String out = toString_Helper(root, curWord, output).toString();
        return out.substring(0, out.length()-1);
    }

    public StringBuilder toString_Helper(Node curNode, StringBuilder curWord, StringBuilder output) {
        if (curNode.count > 0) {
            output.append(curWord.toString() +  "\n");
        }
        if (curNode.hasChildren) {
            for (int i = 0; i < curNode.nodes.length; i++) {
                if (curNode.nodes[i] != null) {
                    curWord.append((char)(i + 'a'));
                    toString_Helper(curNode.nodes[i], curWord, output);
                }
            }
            if (curWord.length() != 0) {
                curWord.deleteCharAt(curWord.length()-1);
            }
        }
        else {
            curWord.deleteCharAt(curWord.length()-1);
        }
        return output;
    }

    @Override
    public int hashCode() {
        return 19*node_count + 13*word_count;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ITrie)) {
            return false;
        }
        if (word_count != ((Trie) o).word_count || node_count != ((Trie) o).node_count) {
            return false;
        }
        if (hashCode() != o.hashCode()) {
            return false;
        }
        if (root == null && ((Trie) o).root == null) {
            return true;
        }
        return equals_helper(root, ((Trie) o).root);

    }

    public boolean equals_helper(Node A, Node B) {
        if ((A.count != B.count) ||
                (A.hasChildren && !B.hasChildren) ||
                (!A.hasChildren && B.hasChildren)) {
            return false;
        } else if (!A.hasChildren && !B.hasChildren) {
            return true;
        } else {
            boolean temp = false;
            for (int i = 0; i < A.nodes.length; i++) {
                if ((A.nodes[i] != null && B.nodes[i] == null) ||
                        (A.nodes[i] == null && B.nodes[i] != null)) {
                    return false;
                } else if (A.nodes[i] == null && B.nodes[i] == null) {
                    continue;
                } else {
                    temp = equals_helper(A.nodes[i], B.nodes[i]);
                    if (!temp) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    /**
     * Your trie node class should implement the ITrie.INode interface
     */
    public class Node implements INode {

        public int count = 0;
        public Node[] nodes = new Node[26];
        public boolean hasChildren = false;


        /**
         * Returns the frequency count for the word represented by the node
         *
         * @return The frequency count for the word represented by the node
         */
        public int getValue() {
            return count;
        }
    }
}
