package spell;

import java.io.IOException;

/**
 * A simple main class for running the spelling corrector. This class is not
 * used by the passoff program.
 */
public class Main {
	
	/**
	 * Give the dictionary file name as the first argument and the word to correct
	 * as the second argument.
	 */
	public static void main(String[] args) throws IOException {
		
		String dictionaryFileName = args[0];
		String inputWord = args[1];
		
		/**
		 * Create an instance of your corrector here
		 */
		ISpellCorrector corrector = new SpellCorrector();
		ISpellCorrector corrector2 = new SpellCorrector();

		try {
            corrector.useDictionary(dictionaryFileName);
            corrector2.useDictionary("small_test2.txt");
            String suggestion = corrector.suggestSimilarWord(inputWord);
            if (suggestion == null) {
                suggestion = "No similar word found";
            }

            System.out.println("Suggestion is: " + suggestion);
            //System.out.println(((SpellCorrector)corrector).trie.getNodeCount());
            //System.out.println(((SpellCorrector)corrector).trie.getWordCount());
            //System.out.println(((SpellCorrector)corrector).trie.toString());
//            ITrie.INode n = ((SpellCorrector)corrector).trie.find(inputWord);
//            if (n == null) {
//                System.out.println("Nothing found");
//            } else {
//                System.out.println("Found input word");
//            }
            //System.out.println(((SpellCorrector)corrector).trie.hashCode());
            //System.out.println(((SpellCorrector)corrector).trie.equals(((SpellCorrector)corrector2).trie));
            //System.out.println(Arrays.toString(((SpellCorrector)corrector).getEditDistance1InDict("ofs").toArray()));
            //System.out.println(Arrays.toString(((SpellCorrector)corrector).getEditDistance1("apple")));
            //System.out.println(Arrays.toString(((SpellCorrector)corrector).getEditDistance2("apple").toArray()));
        }
        catch (IOException e) {
            System.out.println("Got an IOException");
        }
	}
}
