package Generator;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.junit.Assert.*;

public class locationsTest {
    locations locs = null;

    @Before
    public void setUp() throws FileNotFoundException {
        String path = "C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\Server\\json\\locations.json";
        FileReader fileReader = new FileReader(path);
        Gson gson = new Gson();
        locs = gson.fromJson(fileReader, locations.class);
    }

    @Test
    public void test_locs() throws FileNotFoundException {
        for (location lc: locs.data) {
            System.out.println(lc.city);
            System.out.println(lc.country);
            System.out.println(lc.latitude);
            System.out.println(lc.longitude);
            System.out.println();
        }

    }

}