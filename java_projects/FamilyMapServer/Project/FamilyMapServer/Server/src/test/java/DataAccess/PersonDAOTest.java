package DataAccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.UUID;

import Model.Person;

import static org.junit.Assert.*;

public class PersonDAOTest {
    private DataBase db = null;
    private PersonDAO pdao = null;
    private Person p = null;
    private Person p2 = null;
    private Person p3 = null;
    @Before
    public void setUp() throws Exception {
        db = new DataBase("test.db");
        db.openConnection();
        pdao = new PersonDAO();

        //Make sure table is clear
        pdao.clear(db.getConn());


        p = new Person(UUID.randomUUID().toString(),
                "sreschke",
                "Pam",
                "Reschke",
                "F",
                "Lewis Kofford",
                "Maryln Kofford",
                "Phil Reschke");
        p2 = new Person(UUID.randomUUID().toString(),
                "sreschke",
                "Phil",
                "Reschke",
                "M",
                "Mr. Reschke",
                "Mrs. Reschke",
                "Pam Reschke");
        p3 =  new Person(UUID.randomUUID().toString(),
                "rroy",
                "Mom",
                "Roy",
                "F",
                "Mr. Roy",
                "Mrs. Roy",
                "Dad");
    }

    @After
    public void tearDown() throws Exception {
        pdao.clear(db.getConn());
        db.closeConnection(true);
        db = null;
    }

    @Test
    public void createPersonPositive() throws DatabaseException {
        //Insert a person into the database
        pdao.createPerson(p, db.getConn());
    }

    @Test
    public void createPersonNegative() {
        try {
            //Try insert a person twice into the database
            pdao.createPerson(p, db.getConn());
            pdao.createPerson(p, db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getPersonPositive() throws DatabaseException {
        //Create row
        pdao.createPerson(p, db.getConn());

        //Select that row
        p = pdao.getPerson(p.getPersonID(), db.getConn());

        //Create row
        pdao.createPerson(p2, db.getConn());

        //Select that row
        p = pdao.getPerson(p2.getPersonID(), db.getConn());
    }

    @Test
    public void getPersonNegative() {
        try {
            //Select a row from an empty database
            p = pdao.getPerson(p.getPersonID(), db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }



    @Test
    public void getAllPeoplePositive() throws DatabaseException {
        //Insert rows
        pdao.createPerson(p, db.getConn());
        pdao.createPerson(p2, db.getConn());
        pdao.createPerson(p3, db.getConn());

        //Select rows with a common descendant
        Set<Person> ps = pdao.getAllPeople(p.getDescendant(), db.getConn());
        assertEquals(ps.size(), 2);
    }

    @Test
    public void getAllPeopleNegative() throws DatabaseException {
        //Insert rows
        pdao.createPerson(p, db.getConn());
        pdao.createPerson(p2, db.getConn());
        pdao.createPerson(p3, db.getConn());

        //Select rows with a common descendant
        Set<Person> ps = pdao.getAllPeople("bogusDescendent", db.getConn());
        assertNull(ps);
    }

    @Test
    public void clearPositive1() throws DatabaseException {
        //Clear empty database
        pdao.clear(db.getConn());
    }

    @Test
    public void clearPositive2() throws DatabaseException {
        //Insert rows
        pdao.createPerson(p, db.getConn());
        pdao.createPerson(p2, db.getConn());
        pdao.createPerson(p3, db.getConn());

        //Clear non-empty database
        pdao.clear(db.getConn());
    }

    @Test
    public void deletePersonPositive() throws DatabaseException {
        //create User to delete
        pdao.createPerson(p, db.getConn());
        pdao.deletePerson(p.getPersonID(), db.getConn());
    }

    @Test
    public void deletePersonNegative() {
        try {
            //create User to delete
            pdao.createPerson(p, db.getConn());

            //Try to delete a user not in the database
            pdao.deletePerson(p2.getPersonID(), db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }
}