package Service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import Request.RegisterRequest;
import Response.FillResponse;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class FillServiceTest {
    FillService fs = null;
    RegisterService rs = null;

    //Fill Response
    private FillResponse frs = null;

    //Register Request
    private RegisterRequest rr1 = new RegisterRequest();

    //Register Response
    private RegisterResponse rrs1 = null;

    //UserNames
    String userName1;
    String userName2;

    @Before
    public void setUp() throws Exception {
        fs = new FillService();
        rs = new RegisterService();

        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        //Clear database
        DataBase db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @After
    public void tearDown() throws Exception {
        //Clear database
        DataBase db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @Test
    public void servePositive1() {
        //Fill service with 4 generations
        //Register user1
        rrs1 = rs.serve(rr1);

        int num_generations = 4;
        frs = fs.serve(userName1, num_generations);

        String message = frs.getMessage();
        String[] split = message.split(" ");
        assertEquals(split[0], "Successfully");

        int people_added = ((int) (Math.pow(2, num_generations+1)-1));
        assertEquals(split[2], Integer.toString(people_added));

        int events_added = people_added*2 - 1;
        assertEquals(split[5], Integer.toString(events_added));
    }

    @Test
    public void servePositive2() {
        //Fill service with 0 generations
        //Register user1
        rrs1 = rs.serve(rr1);

        int num_generations = 0;
        frs = fs.serve(userName1, num_generations);

        String message = frs.getMessage();
        String[] split = message.split(" ");
        assertEquals(split[0], "Successfully");

        int people_added = ((int) (Math.pow(2, num_generations+1)-1));
        assertEquals(split[2], Integer.toString(people_added));

        int events_added = people_added*2 - 1;
        assertEquals(split[5], Integer.toString(events_added));
    }

    @Test
    public void serveNegative1() {
        //Try to fill a user not in the database
        int num_generations = 4;
        frs = fs.serve(userName1, num_generations);

        String message = frs.getMessage();
        assertEquals(message, "Invalid username");
    }

    @Test
    public void serveNegative2() {
        //Try to fill a user with a negative number of generations

        //Register user1
        rrs1 = rs.serve(rr1);

        int num_generations = -1;
        frs = fs.serve(userName1, num_generations);

        String message = frs.getMessage();
        assertEquals(message, "The number of generations must be positive");
    }

    @Test
    public void servePositive() {

    }
}