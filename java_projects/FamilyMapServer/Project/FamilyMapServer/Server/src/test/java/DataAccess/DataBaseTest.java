package DataAccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;
import Model.AuthToken;
import Model.Event;
import Model.Person;
import Model.User;

public class DataBaseTest {
    private DataBase db;
    private User u;
    private Person p;
    private Event e;
    private AuthToken a;


    @Before
    public void setUp() throws DatabaseException {
        String db_name = "test.db";
        db = new DataBase(db_name);
        db.openConnection();

        //Create users to be inserted
        u = new User("sreschke",
                "password1",
                "spencerreschke@gmail.com",
                "Spencer",
                "Reschke",
                "M",
                UUID.randomUUID().toString());

        p = new Person(UUID.randomUUID().toString(),
                "sreschke",
                "Pam",
                "Reschke",
                "F",
                "Lewis Kofford",
                "Maryln Kofford",
                "Phil Reschke");

        e = new Event("some_e_id",
                "sreschke",
                UUID.randomUUID().toString(),
                "100",
                "50",
                "USA",
                "Alpine",
                "Birth",
                "1970");

        a = new AuthToken(UUID.randomUUID().toString(), "sreschke");

    }

    @After
    public void tearDown() throws DatabaseException {
        db.closeConnection(false);
        db = null;
    }

    @Test
    public void clearPositive() throws DatabaseException {
        //clear an empty database
        db.clear();
    }

    @Test
    public void clearPositive2() throws DatabaseException {
        //clear a nonempty database

        //Fill each table
        db.getUserDao().createUser(u, db.getConn());
        db.getPersonDao().createPerson(p, db.getConn());
        db.getEventDao().createEvent(e, db.getConn());
        db.getAuthTokDao().createAuthTok(a, db.getConn());
        db.clear();
    }

}