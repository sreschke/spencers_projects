package Service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import Request.RegisterRequest;
import Response.AllEventResponse;
import Response.AllPersonResponse;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class AllEventServiceTest {
    private AllEventService es = new AllEventService();
    private RegisterService rs = new RegisterService();

    //Requests
    private RegisterRequest rr1 = new RegisterRequest();

    //Responses
    private AllEventResponse er1 = null;
    private RegisterResponse rrs1 = null;

    //UserNames
    private String userName1;

    @Before
    public void setUp() throws Exception {
        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        //Clear Database
        DataBase db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @After
    public void tearDown() throws Exception {
        //Clear Database
        DataBase db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @Test
    public void servePositive() {
        //Register user1
        rrs1 = rs.serve(rr1);

        String authTok = rrs1.getAuthToken();

        er1 = es.serve(authTok);

        assertEquals(er1.getData().length, 61);
        assertEquals(er1.getMessage(), null);
    }

    @Test
    public void serveNegative1() {
        //Bogus authTok
        er1 = es.serve("garbage");

        assertEquals(er1.getMessage(), "Invalid authorization token");
        assertEquals(er1.getData(), null);
    }

    @Test
    public void serveNegative2() {
        //null auth token
        //Register user1
        rrs1 = rs.serve(rr1);

        er1 = es.serve(null);

        assertEquals(er1.getData(), null);
        assertEquals(er1.getMessage(), "The authTok was empty.");
    }
}