package Service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import Request.LoginRequest;
import Request.RegisterRequest;
import Response.LoginResponse;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class LoginServiceTest {

    LoginService ls = null;
    RegisterService rs = null;

    //Requests
    private LoginRequest lr1 = new LoginRequest();
    private LoginRequest lr2 = new LoginRequest();

    //Responses
    private LoginResponse lrs1 = null;
    private LoginResponse lrs2 = null;

    //Requests
    private RegisterRequest rr1 = new RegisterRequest();
    private RegisterRequest rr2 = new RegisterRequest();

    //UserNames
    String userName1;
    String userName2;

    @Before
    public void setUp() throws Exception {
        ls = new LoginService();
        rs = new RegisterService();

        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        userName2 = UUID.randomUUID().toString();
        String password2 = "password";
        String email2 = "s@gmail.com";
        String fn2 = "Rach";
        String ln2 = "Roy";
        String gender2 = "f";

        rr2.setUserName(userName2);
        rr2.setPassword(password2);
        rr2.setEmail(email2);
        rr2.setFirstName(fn2);
        rr2.setLastName(ln2);
        rr2.setGender(gender2);

        lr1.setUserName(userName1);
        lr1.setPassword(password1);

        lr2.setUserName(userName2);
        lr2.setPassword(password2);
    }

    @After
    public void tearDown() throws Exception {
        DataBase db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @Test
    public void servePositive() {
        //Register user1
        RegisterResponse rr = rs.serve(rr1);

        //Log in user1
        lrs1 = ls.serve(lr1);

        //Check values
        assertNotEquals(lrs1.getAuthToken(), null);
        assertEquals(lrs1.getUserName(),  userName1);
        assertEquals(lrs1.getPersonID(), rr.getPersonID());
        assertEquals(lrs1.getMessage(), null);
    }

    @Test
    public void serveNegative1() {
        //Try to log in a user not in the database

        //Log in user2
        lrs2 = ls.serve(lr2);

        //Check values
        assertEquals(lrs2.getAuthToken(), null);
        assertEquals(lrs2.getUserName(), null);
        assertEquals(lrs2.getPersonID(), null);
        assertEquals(lrs2.getMessage(), "Invalid username");
    }

    @Test
    public void serveNegative2() {
        //Try to log in a user not in the database

        //Log in user2
        lrs2 = ls.serve(lr2);

        //Check values
        assertEquals(lrs2.getAuthToken(), null);
        assertEquals(lrs2.getUserName(), null);
        assertEquals(lrs2.getPersonID(), null);
        assertEquals(lrs2.getMessage(), "Invalid username");
    }
}