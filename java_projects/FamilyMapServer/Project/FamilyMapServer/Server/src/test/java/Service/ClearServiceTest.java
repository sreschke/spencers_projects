package Service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import Request.RegisterRequest;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class ClearServiceTest {
    RegisterService rs = null;
    ClearService cs = null;

    //Database
    DataBase db = null;

    //Requests
    private RegisterRequest rr1 = new RegisterRequest();
    private RegisterRequest rr2 = new RegisterRequest();

    //Responses
    private RegisterResponse rrs1 = null;
    private RegisterResponse rrs2 = null;

    //UserNames
    String userName1;

    @Before
    public void setUp() throws Exception {
        cs = new ClearService();
        rs = new RegisterService();

        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        //Make sure database is empty
        db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void servePositive() {
        //Clear an empty database
        cs.serve();
    }

    @Test
    public void servePositive2() {
        //Clear a nonempty database

        //Register user1
        rrs1 = rs.serve(rr1);

        cs.serve();
    }
}