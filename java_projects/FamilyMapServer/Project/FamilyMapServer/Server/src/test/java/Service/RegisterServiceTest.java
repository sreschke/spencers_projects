package Service;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import Request.RegisterRequest;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class RegisterServiceTest {
    private RegisterService rs = null;

    //Requests
    private RegisterRequest rr1 = new RegisterRequest();
    private RegisterRequest rr2 = new RegisterRequest();

    //Responses
    private RegisterResponse rrs1 = null;
    private RegisterResponse rrs2 = null;

    //UserNames
    private String userName1;
    private String userName2;

    private DataBase db = null;

    @Before
    public void setUp() throws Exception {
        rs = new RegisterService();

        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        userName2 = UUID.randomUUID().toString();
        String password2 = "password";
        String email2 = "s@gmail.com";
        String fn2 = "Rach";
        String ln2 = "Roy";
        String gender2 = "f";

        rr2.setUserName(userName2);
        rr2.setPassword(password2);
        rr2.setEmail(email2);
        rr2.setFirstName(fn2);
        rr2.setLastName(ln2);
        rr2.setGender(gender2);

        //Clear database
        db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);

    }

    @After
    public void tearDown() throws Exception {
        //Clear database
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @Test
    public void servePositive() {
        //Register user1
        rrs1 = rs.serve(rr1);

        //Check response 1
        assertNotEquals(rrs1.getAuthToken(), null);
        assertTrue(rrs1.getUserName().equals(userName1));
        assertEquals(rrs1.getMessage(), null);

        //Register user2
        rrs2 = rs.serve(rr2);

        //Check response 2
        assertNotEquals(rrs2.getAuthToken(), null);
        assertTrue(rrs2.getUserName().equals(userName2));
        assertEquals(rrs2.getMessage(), null);
    }

    @Test
    public void serveNegative() {
        //Register user 1
        rrs1 = rs.serve(rr1);

        //Check response 1
        assertNotEquals(rrs1.getAuthToken(), null);
        assertTrue(rrs1.getUserName().equals(userName1));
        assertEquals(rrs1.getMessage(), null);

        //Try to register same user
        rrs2 = rs.serve(rr1);

        //Check response 2
        assertEquals(rrs2.getAuthToken(), null);
        assertEquals(rrs2.getUserName(), null);
        assertEquals(rrs2.getMessage(), "Username already taken by another user.");
    }


}