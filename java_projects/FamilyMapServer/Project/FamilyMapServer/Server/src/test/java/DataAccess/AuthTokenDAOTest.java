package DataAccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.UUID;

import Model.AuthToken;
import Model.User;

import static org.junit.Assert.*;

public class AuthTokenDAOTest {
    private DataBase db = null;
    private AuthTokenDAO adao = null;
    private AuthToken a = null;
    private AuthToken a2 = null;
    private AuthToken a3 = null;
    private UserDAO udao = null;
    private User u = null;

    @Before
    public void setUp() throws Exception {
        db = new DataBase("test.db");
        db.openConnection();
        adao = new AuthTokenDAO();
        adao.clear(db.getConn());

        a = new AuthToken(UUID.randomUUID().toString(), "sreschke");
        a2 = new AuthToken(UUID.randomUUID().toString(), "sreschke");
        a3 = new AuthToken(UUID.randomUUID().toString(), "rroy");

        //Create row in User
        udao = new UserDAO();
        udao.clear(db.getConn());
        u = new User("jreschke",
                "password",
                "spencerreschke@gmail.com",
                "Spencer",
                "Reschke",
                "M",
                UUID.randomUUID().toString());

    }

    @After
    public void tearDown() throws Exception {
        adao.clear(db.getConn());
        udao.clear(db.getConn());
        db.closeConnection(true);
        db = null;
    }

    @Test
    public void createAuthTokPositive() throws DatabaseException {
        //Create one row
        adao.createAuthTok(a2, db.getConn());
    }

    @Test
    public void createAuthTokNegtive() {
        try {
            //try to insert same token twice
            adao.createAuthTok(a2, db.getConn());
            adao.createAuthTok(a2, db.getConn());
        } catch (DatabaseException e){
            assertTrue(true);
        }
    }

    @Test
    public void getAuthToksPositive() throws DatabaseException {
        //Select rows with userName sreschke
        //Add three rows to AuthTok table
        adao.createAuthTok(a, db.getConn());
        adao.createAuthTok(a2, db.getConn());
        adao.createAuthTok(a3, db.getConn());

        Set<AuthToken> ps = adao.getAuthToks(a.getUserName(), db.getConn());
        assertEquals(2, ps.size());
    }

    @Test
    public void getAuthToksNegative() throws DatabaseException {
        //Select rows with userName sreschke
        //Add three rows to AuthTok table
        adao.createAuthTok(a, db.getConn());
        adao.createAuthTok(a2, db.getConn());
        adao.createAuthTok(a3, db.getConn());

        //Select using bogus username
        Set<AuthToken> ps = adao.getAuthToks("bogus", db.getConn());
        assertNull(ps);
    }

    @Test
    public void getUserPositive() throws DatabaseException {
        //Create one row in authToks table
        adao.createAuthTok(a, db.getConn());

        //Create one row in User table
        udao.createUser(u, db.getConn());

        //Select that row
        String u2 = adao.getUser(a.getToken(), db.getConn());
        assertEquals(u2, "sreschke");
    }

    @Test
    public void getUserNegative() throws DatabaseException {
        //Create one row in authToks table
        adao.createAuthTok(a, db.getConn());

        //Create one row in User table
        udao.createUser(u, db.getConn());

        //Select row using bogus token
        String u2 = adao.getUser("bogus", db.getConn());
        assertNull(u2);
    }

    @Test
    public void clearPositive1() throws DatabaseException {
        //Create two row in authToks table
        adao.createAuthTok(a2, db.getConn());
        adao.createAuthTok(a3, db.getConn());
        adao.clear(db.getConn());
    }

    @Test
    public void clearPositive2() throws DatabaseException {
        //clear empty table
        adao.clear(db.getConn());
    }

    @Test
    public void deleteAuthTokPositive() throws DatabaseException {
        //create authtok to delete
        adao.createAuthTok(a, db.getConn());
        adao.deleteAuthTok(a, db.getConn());
    }

    @Test
    public void deleteAuthTokNegative() {
        try {
            //create authtok to delete
            adao.createAuthTok(a, db.getConn());
            //Try to delete a token not in the database
            adao.deleteAuthTok(a2, db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }
}