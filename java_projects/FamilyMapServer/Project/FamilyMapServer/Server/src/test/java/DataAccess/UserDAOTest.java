package DataAccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import Model.User;

import static org.junit.Assert.*;

public class UserDAOTest {
    private UserDAO udao;
    private User u;
    private User u2;
    private DataBase db;

    @Before
    public void setUp() throws DatabaseException {
        //Initialize test database
        db = new DataBase("test.db");
        db.openConnection();

        //Make sure user Table is clear
        udao = new UserDAO();
        udao.clear(db.getConn());


        //Create users to be inserted
        u = new User("sreschke",
                "password1",
                "spencerreschke@gmail.com",
                "Spencer",
                "Reschke",
                "M",
                "someID");

        u2 = new User("jreschke",
                "password2",
                "jmr@gmail.com",
                "Jeremy",
                "Reschke",
                "M",
                "someID");
    }

    @After
    public void tearDown() throws DatabaseException {
        udao.clear(db.getConn());
        db.closeConnection(true);
        db = null;
    }

    @Test
    public void createUser1()throws DatabaseException {
        //Insert one user
        assertTrue(udao.createUser(u, db.getConn()));
    }

    @Test public void createUser2() throws DatabaseException {
        //Insert two users
        assertTrue(udao.createUser(u, db.getConn()));
        assertTrue(udao.createUser(u2, db.getConn()));
    }

    @Test public void createUserNegative() {
        try {
            //Try to insert the same user twice
            assertTrue(udao.createUser(u, db.getConn()));
            assertTrue(udao.createUser(u2, db.getConn()));
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getUserPositive() throws DatabaseException {
        //Create row
        udao.createUser(u, db.getConn());

        //Select that row
        u = udao.getUser(u.getUserName(), db.getConn());
    }

    @Test
    public void getUserNegative() throws DatabaseException {
        //try to get a user from an empty database
        assertNull(udao.getUser(u.getUserName(), db.getConn()));

        //Create row
        udao.createUser(u, db.getConn());
        //try to get a missing user from an non-empty database
        assertNull(udao.getUser(u2.getUserName(), db.getConn()));

    }

    @Test
    public void clearPositive() throws DatabaseException {
        //Clear an empty database
        udao.clear(db.getConn());
        assertTrue(true);
    }

    @Test
    public void clearPositive2() throws DatabaseException {
        //create User to delete
        udao.createUser(u, db.getConn());
        udao.createUser(u2, db.getConn());

        //Clear database
        udao.clear(db.getConn());

        //Insert same users again
        udao.createUser(u, db.getConn());
        udao.createUser(u2, db.getConn());

        //Clear again
        udao.createUser(u, db.getConn());
        udao.createUser(u2, db.getConn());
    }

    @Test
    public void deleteUserPositive() throws DatabaseException {
        //create User to delete
        udao.createUser(u, db.getConn());
        udao.deleteUser(u.getUserName(), db.getConn());
    }

    @Test
    public void deleteUserNegative() {
        try {
            //try to delete from an empty database
            udao.deleteUser(u.getUserName(), db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkForUserPositive() throws DatabaseException {
        //create User to delete
        udao.createUser(u, db.getConn());
        assertTrue(udao.checkForUser(u.getUserName(), db.getConn()));

        assertFalse(udao.checkForUser("randomUser", db.getConn()));
    }

    @Test
    public void checkForUserNegative() throws DatabaseException {
        //Check for a user in an empty database
        assertFalse(udao.checkForUser(u.getUserName(), db.getConn()));
    }

    @Test
    public void validateUserPositive() throws DatabaseException {
        String userName = u.getUserName();
        String password = u.getPassword();
        //Add user
        udao.createUser(u, db.getConn());

        //Validate user just entered
        assertTrue(udao.validateUser(userName, password, db.getConn()));
    }

    @Test
    public void validateUserNegative() throws DatabaseException {
        String userName = u.getUserName();
        String password = u.getPassword();

        //Invalidate user since the database is empty
        assertFalse(udao.validateUser(userName, password, db.getConn()));

        //Insert user into database
        udao.createUser(u, db.getConn());
        //Try to validate user with wrong password
        assertFalse(udao.validateUser(userName, "bogusPassword", db.getConn()));
    }
}