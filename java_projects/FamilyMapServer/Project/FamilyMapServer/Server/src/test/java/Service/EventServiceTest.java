package Service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Event;
import Request.RegisterRequest;
import Response.EventResponse;
import Response.RegisterResponse;

import static org.junit.Assert.*;

public class EventServiceTest {

    private EventService es = new EventService();
    private RegisterService rs = new RegisterService();

    //Requests
    private RegisterRequest rr1 = new RegisterRequest();

    //Responses
    private EventResponse er1 = null;
    private RegisterResponse rrs1 = null;

    //UserNames
    private String userName1;

    private DataBase db = null;

    @Before
    public void setUp() throws Exception {
        userName1 = UUID.randomUUID().toString();
        String password1 = "password";
        String email1 = "s@gmail.com";
        String fn1 = "Spencer";
        String ln1 = "Reschke";
        String gender1 = "m";

        rr1.setUserName(userName1);
        rr1.setPassword(password1);
        rr1.setEmail(email1);
        rr1.setFirstName(fn1);
        rr1.setLastName(ln1);
        rr1.setGender(gender1);

        //Clear Database
        db = new DataBase();
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @After
    public void tearDown() throws Exception {
        db.openConnection();
        db.clear();
        db.closeConnection(true);
    }

    @Test
    public void servePositive() throws DatabaseException {
        //Register user1
        rrs1 = rs.serve(rr1);

        //Create a new event and add it to the database
        String eventID = UUID.randomUUID().toString();
        Event e = new Event(eventID,
                userName1,
                rrs1.getPersonID(),
                "100",
                "50",
                "USA",
                "Alpine",
                "Birth",
                "1970");

        db.openConnection();
        db.getEventDao().createEvent(e, db.getConn());
        db.closeConnection(true);



        String authTok = rrs1.getAuthToken();
        er1 = es.serve(authTok, eventID);

        assertEquals(er1.getDescendant(), userName1);
        assertEquals(er1.getEventID(), eventID);

    }

    @Test
    public void serveNegative() throws DatabaseException {
        //Garbage eventID
        //Register user1
        rrs1 = rs.serve(rr1);


        String authTok = rrs1.getAuthToken();
        er1 = es.serve(authTok, "Garbage");

        assertEquals(er1.getMessage(), "Invalid eventID");

    }
}