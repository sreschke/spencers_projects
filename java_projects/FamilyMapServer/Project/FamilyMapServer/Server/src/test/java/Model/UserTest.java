package Model;

import org.junit.Before;
import org.junit.Test;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import DataAccess.UserDAO;
import Model.User;

import static org.junit.Assert.*;

public class UserTest {
    private User u;

    @Before
    public void setUp() {
        u = new User("sreschke",
                "password",
                "spencerreschke@gmailcom",
                "Spencer",
                "Reschke",
                "M",
                "someID");
    }

    @Test
    public void getColumnHeaders() {
        assertEquals(u.getColumnHeaders(), "(Username, Password, Email, First, Last, Gender, Person)");
    }

    @Test
    public void getColumnValues() {
        assertEquals(u.getColumnValues(), "('sreschke', 'password', 'spencerreschke@gmailcom', 'Spencer', 'Reschke', 'M', 'someID')");
    }
}