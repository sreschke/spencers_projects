package DataAccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.UUID;

import javax.xml.crypto.Data;

import Model.Event;

import static org.junit.Assert.*;

public class EventDAOTest {
    private DataBase db = null;
    private EventDAO edao = null;
    private Event e = null;
    private Event e2 = null;
    private Event e3 = null;

    @Before
    public void setUp() throws Exception {
        db = new DataBase("test.db");
        db.openConnection();
        edao = new EventDAO();
        edao.clear(db.getConn());

        e = new Event("some_e_id",
                "sreschke",
                UUID.randomUUID().toString(),
                "100",
                "50",
                "USA",
                "Alpine",
                "Birth",
                "1970");
        e2 = new Event("some_e_id2",
                "sreschke",
                UUID.randomUUID().toString(),
                "100",
                "50",
                "USA",
                "Alpine",
                "Birth",
                "1970");
        e3 =  new Event("some_e_id3",
                "rroy",
                UUID.randomUUID().toString(),
                "100",
                "50",
                "USA",
                "Alpine",
                "Birth",
                "1970");
    }

    @After
    public void tearDown() throws Exception {
        edao.clear(db.getConn());
        db.closeConnection(false);
        db = null;
    }

    @Test
    public void createEventPositve() throws DatabaseException {
        //Create a single event
        edao.createEvent(e, db.getConn());
    }

    @Test
    public void createEventNegatgive() {
        try {
            //Create a single event
            edao.createEvent(e, db.getConn());
            //Try to insert duplciate event
            edao.createEvent(e, db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getEventPositive() throws DatabaseException {
        //Create row
        edao.createEvent(e, db.getConn());

        //Select that row
        e = edao.getEvent(e.getEventID(), db.getConn());
    }

    @Test
    public void getEventNegative() {
        try {
            //Create row
            edao.createEvent(e, db.getConn());

            //Select a row not in the database
            e = edao.getEvent(e2.getEventID(), db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getAllEventsPositve() throws DatabaseException {
        //Insert rows
        edao.createEvent(e, db.getConn());
        edao.createEvent(e2, db.getConn());
        edao.createEvent(e3, db.getConn());

        //Select that row
        Set<Event> ps = edao.getAllEvents(e.getDescendant(), db.getConn());
        assertEquals(ps.size(), 2);
    }

    @Test
    public void getAllEventsNegative() throws DatabaseException {
        //Insert rows
        edao.createEvent(e, db.getConn());
        edao.createEvent(e2, db.getConn());
        edao.createEvent(e3, db.getConn());

        //Select that row
        Set<Event> ps = edao.getAllEvents("garbage", db.getConn());
        assertNull(ps);
    }

    @Test
    public void clearPositve1() throws DatabaseException {
        //Clear an empty database
        edao.clear(db.getConn());
    }

    @Test
    public void clearPositive2() throws DatabaseException {
        //Insert rows
        edao.createEvent(e, db.getConn());
        edao.createEvent(e2, db.getConn());
        edao.createEvent(e3, db.getConn());

        //Clear a non-empty database
        edao.clear(db.getConn());
    }

    @Test
    public void deleteEventPositive() throws DatabaseException {
        //create User to delete
        edao.createEvent(e, db.getConn());
        edao.deleteEvent(e.getEventID(), db.getConn());
    }

    @Test
    public void deleteEventNegative() {
        try {
            //create User to delete
            edao.createEvent(e, db.getConn());
            //try to delete using a bous username
            edao.deleteEvent("bogus", db.getConn());
        } catch (DatabaseException e) {
            assertTrue(true);
        }
    }
}