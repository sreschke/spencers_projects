package Model;

public class Person {
    private String personID;
    private String descendant;
    private String firstName;
    private String lastName;
    private String gender;
    private String father;
    private String mother;
    private String spouse;


    public Person(String personID, String descendant, String firstName, String lastName, String gender, String father, String mother, String spouse) {
        this.personID = personID;
        this.descendant = descendant;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.father = father;
        this.mother = mother;
        this.spouse = spouse;
    }

    public Person() {
        personID = null;
        descendant = null;
        firstName = null;
        lastName = null;
        gender = null;
        father = null;
        mother = null;
        spouse = null;
    }

    /**
     * used to get the column names in the Event table in the database. Called from
     * database
     * @return
     */
    public String getColumnHeaders() {
        return "(Person, Descendant, First, Last, Gender, Father, Mother, Spouse)";
    }

    /**
     * Used to get the values of an Person object for a createPerson statement as used
     * in the Database class
     * @return
     */
    public String getColumnValues () {
        return "('" + getPersonID() + "', "
                + "'" + getDescendant() + "', "
                + "'" +  getFirstName() + "', "
                + "'" +  getLastName() + "', "
                + "'" +  getGender() + "', "
                + "'" +  getFather() + "', "
                + "'" +  getMother() + "', "
                + "'" +  getSpouse()
                + "')";
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getDescendant() {
        return descendant;
    }

    public void setDescendant(String descendant) {
        this.descendant = descendant;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }
}
