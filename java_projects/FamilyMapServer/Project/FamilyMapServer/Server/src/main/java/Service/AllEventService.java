package Service;

import java.util.Set;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Event;
import Response.AllEventResponse;

public class AllEventService {

    /**
     * Returns all events for all family members of the current user. The current
     * user is determined from the provided auth token.
     * @param authTok
     * @return an AllEventResponse object conatining an array of Event objects
     */
    public AllEventResponse serve(String authTok) {

        AllEventResponse aer = new AllEventResponse();

        if (authTok == null) {
            aer.setMessage("The authTok was empty.");
            return aer;        }

        DataBase db = new DataBase();
        try {
            db.openConnection();


            //Get userName with authToken
            String userName = db.getAuthTokDao().getUser(authTok, db.getConn());
            db.closeConnection(true);
            db.openConnection();
            if (userName == null) {
                //We got a bogus auth token
                aer.setMessage("Invalid authorization token");
                return aer;
            }

            //Get all people associated with userName
            Set<Event> events = db.getEventDao().getAllEvents(userName, db.getConn());
            db.closeConnection(true);
            db.openConnection();
            //Load people in response
            aer.setData(events);
            return aer;

        } catch (DatabaseException e) {
            e.printStackTrace();
            aer.setMessage("Internal Server Error");
            return aer;
        }
    }
}
