package Model;

public class AuthToken {
    private String token;
    private String userName;

    public AuthToken(String token, String userName) {
        this.token = token;
        this.userName = userName;
    }

    /**
     * used to get the column names in the Event table in the database. Called from
     * database
     * @return
     */
    public String getColumnHeaders() {
        return "(Token, Username)";
    }

    /**
     * Used to get the values of an Event object for a createEvent statement as used
     * in the Database class
     * @return
     */
    public String getColumnValues () {
        return "('" + getToken() + "', "
                + "'" +  getUserName()
                + "')";
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getUserName() {

        return userName;
    }

    public void setUseName(String userName) {

        this.userName = userName;
    }
}
