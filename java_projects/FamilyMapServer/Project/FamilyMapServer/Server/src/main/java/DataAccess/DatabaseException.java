package DataAccess;

import java.sql.SQLException;

import javax.xml.crypto.Data;

public class DatabaseException extends Exception {
    private String e_message;
    public DatabaseException(String message, SQLException e) {
        e_message = message;
    }
    public DatabaseException(String message) {
        e_message = message;
    }
}
