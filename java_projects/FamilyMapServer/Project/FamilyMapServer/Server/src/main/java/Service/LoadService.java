package Service;

import com.google.gson.Gson;

import javax.xml.crypto.Data;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Event;
import Model.Person;
import Model.User;
import Response.LoginResponse;
import Request.LoadRequest;
import Response.LoadResponse;

public class LoadService {

    /**
     * Clears all data from the database, and then loads the posed user, person,
     * and event data into the database
     * @param req
     * @return
     */
    public LoadResponse serve(LoadRequest req) {
        LoadResponse lr = new LoadResponse();

        DataBase db = new DataBase();

        try {
            db.openConnection();

            //Clear Database
            db.clear();

            //Add users
            int num_users = 0;
            if (req.getUsers() != null) {
                for (User u : req.getUsers()) {
                    db.getUserDao().createUser(u, db.getConn());
                    num_users += 1;
                }
            }

            //Add persons
            int num_people = 0;
            if (req.getPersons() != null) {
                for (Person p : req.getPersons()) {
                    db.getPersonDao().createPerson(p, db.getConn());
                    num_people += 1;
                }
            }

            //Add events
            int num_events = 0;
            if (req.getEvents() != null) {
                for (Event e : req.getEvents()) {
                    db.getEventDao().createEvent(e, db.getConn());
                    num_events += 1;
                }
            }

            db.closeConnection(true);
            lr.setMessage("Successfully added " + num_users + " users, " + num_people + " persons, and " + num_events + " events to the database");
        } catch (DatabaseException e) {
            lr.setMessage("Internal server error");
            try {
                db.closeConnection(false);
            } catch (DatabaseException ex ) {
                e.printStackTrace();
            }
            return lr;
        }
        return lr;
    }
}
