package Service;

import java.util.UUID;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Response.ClearResponse;

public class ClearService {

    /**
     * clears the database
     * @return a ClearResponse object
     */
    public ClearResponse serve() {
        ClearResponse cr = new ClearResponse();


        DataBase db = new DataBase();
        try {
            db.openConnection();
            db.clear();
            db.closeConnection(true);
            cr.setMessage("Clear succeeded");

        } catch (DatabaseException e) {
            e.printStackTrace();
            cr.setMessage("Internal Server Error");
            return cr;
        }

        return cr;


    }
}
