package Response;

public class LoginResponse {
    String authToken;
    String userName;
    String personID;
    String message;

    public LoginResponse(String authToken, String userName, String personID, String message) {
        this.authToken = authToken;
        this.userName = userName;
        this.personID = personID;
        this.message = message;
    }

    public LoginResponse() {
        authToken = null;
        userName = null;
        personID = null;
        message = null;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
