package Service;

import java.util.Set;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import DataAccess.UserDAO;
import Generator.Fill_Generator;
import Model.Event;
import Model.Person;
import Model.User;
import Response.FillResponse;
import Server.InternalServerException;

public class FillService {

    /**
     * Populates the server's database with generated data for the specified user name.
     * @param userName the userName of the person to generate data for
     * @param numGenerations how many generations to add
     * @return a FillResponse object with a message indicating success or failure
     */
    public FillResponse serve(String userName, int numGenerations) {
        //Response to return
        FillResponse res = new FillResponse();

        //Check that numGenerations is nonnegative
        if (numGenerations < 0) {
            res.setMessage("The number of generations must be positive");
            return res;
        }


        DataBase db;
        try {
            db = new DataBase();
            db.openConnection();
            //check if user is in database
            if (!db.getUserDao().checkForUser(userName, db.getConn())) {
                res.setMessage("Invalid username");
                db.closeConnection(false);
                return res;
            }
            //Get user from database
            User u = db.getUserDao().getUser(userName, db.getConn());


            //Generate people and events
            Fill_Generator fg = new Fill_Generator(userName, numGenerations);
            fg.setUser(u);
            fg.setU_person_ID(u.getPersonID());
            fg.generateFamily();
            Set<Event> events_to_delete = fg.getEvents_to_delete();
            Set<Event> events_to_add = fg.getEvents_to_add();
            Set<Person> people_to_delete = fg.getPeople_to_delete();
            Set<Person> people_to_add = fg.getPeople_to_add();
            int num_people_added = people_to_add.size();
            int num_events_added = events_to_add.size();

            //Delete events from database
            for (Event e: events_to_delete) {
                db.getEventDao().deleteEvent(e.getEventID(), db.getConn());
            }

            //Delete people from database
            for (Person p: people_to_delete) {
                db.getPersonDao().deletePerson(p.getPersonID(), db.getConn());
            }

            //Add events to database
            for (Event e: events_to_add) {
                db.getEventDao().createEvent(e, db.getConn());
            }

            //Add people to database
            for (Person p: people_to_add) {
                db.getPersonDao().createPerson(p, db.getConn());
            }

            db.closeConnection(true);
            res.setMessage("Successfully added " + num_people_added + " persons, and " + num_events_added + " events to the database.");
            return res;


        } catch (DatabaseException e) {
            res.setMessage("Internal server error.");
            return res;
        }
    }
}
