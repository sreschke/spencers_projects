package Service;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Person;
import Model.User;
import Response.PersonResponse;

public class PersonService {

    /**
     * Given sufficient authorization, get the single Person object with a specified person
     * ID.
     * @param authTok the authorization token
     * @param personID the person ID
     * @return
     */
    public PersonResponse serve(String authTok, String personID) {

        PersonResponse pr = new PersonResponse();

        if (authTok == null) {
            pr.setMessage("The authTok was empty.");
            return pr;
        }

        if (personID == null) {
            pr.setMessage("The personID was empty.");
            return pr;
        }

        DataBase db = new DataBase();
        try {



            //Get userName with authToken
            db.openConnection();
            String userName = db.getAuthTokDao().getUser(authTok, db.getConn());
            db.closeConnection(true);

            if (userName == null) {
                //We got a bogus auth token
                pr.setMessage("Invalid authorization token");
                return pr;
            }

            //Make sure personID is legit
            db.openConnection();
            Person p = db.getPersonDao().getPerson(personID, db.getConn());
            db.closeConnection(false);
            if (p == null) {
                pr.setMessage("Invalid personID");
                return pr;
            }

            //Make sure userName is associated with the person ID
            db.openConnection();
            boolean legit = db.getPersonDao().validatePerson(userName, personID, db.getConn());
            db.closeConnection(false);
            if (!legit) {
                pr.setMessage("Requested person does not belong to this user");
            } else {
                //The request is legit
                pr.setDescendant(p.getDescendant());
                pr.setPersonID(p.getPersonID());
                pr.setFirstName(p.getFirstName());
                pr.setLastName(p.getLastName());
                pr.setGender(p.getGender());
                pr.setFather(p.getFather());
                pr.setMother(p.getMother());
                pr.setSpouse(p.getSpouse());
                return pr;
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
            pr.setMessage("Internal Server Error");
            return pr;
        }

        return pr;

    }
}