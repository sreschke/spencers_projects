package Handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class DefaultFileHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        boolean success = false;

        try {
            if (exchange.getRequestMethod().toLowerCase().equals("get")) {
                //Get path from exchange
                String path = exchange.getRequestURI().toString();

                if (path.equals("/")) {
                    path = File.separator + "index.html";
                }

                //Check if path length is zero. In this case we want the index.html file
                if (path.length() == 0 ) {
                    path = File.separator + "index.html";
                }

                //Prepend root directory to path
                path = "Server" + File.separator + "web" + path;

                //
                Path filePath = FileSystems.getDefault().getPath(path);
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                Files.copy(filePath, exchange.getResponseBody());
                exchange.getResponseBody().close();
                success = true;
            }
            if (!success) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                exchange.getResponseBody().close();
            }

        } catch (IOException e) {
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
            exchange.getResponseBody().close();
            e.printStackTrace();
        }

    }

}
