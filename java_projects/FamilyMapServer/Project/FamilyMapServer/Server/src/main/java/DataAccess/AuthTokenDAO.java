package DataAccess;

import Model.AuthToken;
import Model.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.sql.*;

public class AuthTokenDAO {
    private String table_Name = "AuthTokens";
    /**
     * add a AuthToken/username pair to the database
     * @param a
     * @return true if successfully added, false otherwise
     */
    public void createAuthTok(AuthToken a, Connection conn)throws DatabaseException  {
        try {
            Statement stmt = null;
            try {
                String sql = "insert into " + table_Name + " VALUES " + a.getColumnValues();
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("createAuthTok failed: could not insert AuthToken");
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("createAuthTok failed", ex);
        }
    }

    /**
     * Get all of the authentication tokens associated with a given username
     * @param userName
     * @return a set of authentication tokens associated with a given username, null
     * if there are none.
     */
    public Set<AuthToken> getAuthToks(String userName, Connection conn) throws DatabaseException {

        Set<AuthToken> es = new HashSet<>();
        AuthToken a = null;
        try {
            Statement stmt = null;
            ResultSet rs;
            try {
                String sql = "SELECT * From " + table_Name + " where Username = '" + userName + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty (i.e. the user is not in the database
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String token = rs.getString("Token");
                    String username = rs.getString("Username");
                    a = new AuthToken(token, userName);
                    es.add(a);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getPerson failed", ex);
        }
        return es;
    }

    /**
     * Get the user associated with a given authentication token
     * @param token
     * @return the user associated with the auth token, null if there is none.
     */
    public String getUser(String token, Connection conn) throws DatabaseException {
        User u = null;
        try {
            Statement stmt = null;
            Statement stmt2 = null;
            ResultSet rs;
            ResultSet rs2;
            try {
                //get userName of corresponding token
                String sql2 = "Select * From " + table_Name + " where Token = '" + token + "'";
                stmt2 = conn.createStatement();
                rs2 = stmt2.executeQuery(sql2);

                if (!rs2.isBeforeFirst()) {
                    //The query was empty (i.e. the token was bogus
                    return null;
                }
                String un = rs2.getString("UserName");
                return un;
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getEvent failed", ex);
        }
    }

    /**
     * clears the database of all authentication/username rows
     */
    public void clear(Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            Statement stmt2;
            try {
                String sql = "Delete from " + table_Name;
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("clear failed", ex);
        }
    }

    /**
     * delete a row from the AuthTok/username database
     * @param a
     * @return true if the row was successfully deleted, false otherwise
     */
    public void deleteAuthTok(AuthToken a, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "DELETE From " + table_Name + " where Token = '" + a.getToken() + "'";
                stmt = conn.createStatement();


//                if (stmt.executeUpdate(sql) != 1) {
//                    throw new DatabaseException("deleteAuthTok failed: could not delete" + a.getToken());
//                }
                stmt.executeUpdate(sql);
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteAuthTok failed", ex);
        }

    }

}
