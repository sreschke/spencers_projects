package Response;

import java.util.Set;

import Model.Person;

public class AllPersonResponse {
    private Person[] data;
    private String message;


    public AllPersonResponse(Person[] data) {
        this.data = data;
    }

    public AllPersonResponse() {this.data = null; }

    public Person[] getData() {
        return data;
    }

    public void setData(Set<Person> data) {
        int n = data.size();
        this.data = new Person[n];

        int i = 0;
        for(Person p: data) {
            this.data[i++] = p;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
