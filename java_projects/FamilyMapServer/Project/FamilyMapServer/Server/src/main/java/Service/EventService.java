package Service;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Event;
import Response.EventResponse;

public class EventService {

    /**
     * Returns the single event object with the specified event ID.
     * @param eventID
     * @return and EventResponse object containing the event info
     */
    public EventResponse serve(String authTok, String eventID) {
        EventResponse er = new EventResponse();

        if (authTok == null) {
            er.setMessage("The authTok was empty.");
            return er;
        }

        if (eventID == null) {
            er.setMessage("The personID was empty.");
            return er;
        }

        DataBase db = new DataBase();
        try {



            //Get userName with authToken
            db.openConnection();
            String userName = db.getAuthTokDao().getUser(authTok, db.getConn());
            db.closeConnection(true);

            if (userName == null) {
                //We got a bogus auth token
                er.setMessage("Invalid authorization token");
                return er;
            }

            //Make sure eventID is legit
            db.openConnection();
            Event e = db.getEventDao().getEvent(eventID, db.getConn());
            db.closeConnection(true);
            if (e == null) {
                er.setMessage("Invalid eventID");
                return er;
            }

            //Make sure userName is associated with the event ID
            db.openConnection();
            boolean legit = db.getEventDao().validateEvent(userName, eventID, db.getConn());
            db.closeConnection(false);

            if (!legit) {
                er.setMessage("Requested event does not belong to this user");
            } else {
                //The request is legit
                //Set values of response
                er.setDescendant(e.getDescendant());
                er.setEventID(e.getEventID());
                er.setPersonID(e.getPerson());
                er.setLatitude(e.getLatitude());
                er.setLongitude(e.getLongitude());
                er.setCountry(e.getCountry());
                er.setCity(e.getCity());
                er.setEventType(e.getEventType());
                er.setYear(e.getYear());
                return er;
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
            er.setMessage("Internal Server Error");
            return er;

        }
        return er;
    }
}
