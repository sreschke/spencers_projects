package Service;

import java.util.UUID;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.AuthToken;
import Model.User;
import Request.LoginRequest;
import Response.LoginResponse;

public class LoginService {

    /**
     * process a LoginRequest
     * @param req the request to process
     * @return a LoginResponse object
     */
    public LoginResponse serve(LoginRequest req) {

        LoginResponse lr = new LoginResponse();
        String userName = req.getUserName();
        String password = req.getPassword();

        if (userName == null) {
            lr.setMessage("The username was empty.");
            return lr;
        }

        if (password == null) {
            lr.setMessage("The password was empty.");
            return lr;
        }

        DataBase db = new DataBase();
        try {
            db.openConnection();

            //Check if userName is in the database
            Boolean indb = db.getUserDao().checkForUser(userName, db.getConn());
            if (!indb) {
                lr.setMessage("Invalid username");
                db.closeConnection(false);
                return lr;
            }


            //Validate user
            Boolean valid = db.getUserDao().validateUser(userName, password, db.getConn());
            if (valid) {
                //Get user that's in the database
                User u = db.getUserDao().getUser(userName, db.getConn());

                //Create new authorization token
                String authTok = UUID.randomUUID().toString();
                AuthToken a = new AuthToken(authTok, u.getUserName());
                db.getAuthTokDao().createAuthTok(a, db.getConn());
                db.closeConnection(true);

                //Set response
                lr.setAuthToken(authTok);
                lr.setUserName(userName);
                lr.setPersonID(u.getPersonID());
                return lr;
            }
            else {
                lr.setMessage("Incorrect password");
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
            lr.setMessage("Internal Server Error");
            return lr;
        }

        return lr;
    }
}
