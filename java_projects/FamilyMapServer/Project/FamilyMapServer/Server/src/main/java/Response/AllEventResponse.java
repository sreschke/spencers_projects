package Response;

import java.util.Set;

import Model.Event;

public class AllEventResponse {
    private Event[] data;
    private String message;


    public AllEventResponse(Event[] data) {
        this.data = data;
    }

    public AllEventResponse() { this.data = null; }

    public Event[] getData() {
        return data;
    }

    public void setData(Event[] data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(Set<Event> events) {
        int n = events.size();
        this.data = new Event[n];

        int i = 0;
        for(Event e: events) {
            this.data[i++] = e;
        }

    }
}
