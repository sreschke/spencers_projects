package Generator;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Class used to generate ancestral data. Reads the json files from the included lab files
 * and stores the data in class objects.
 * Used in fill
 */
public class Generator {
    public fnames fns = new fnames();
    public locations locs = new locations();
    public mnames mns = new mnames();
    public snames sns = new snames();

    public Generator() throws FileNotFoundException {
        //set up fnames, locs, mns, and sns
        String path = "C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\Server\\json\\fnames.json";
        FileReader fileReader = new FileReader(path);
        Gson gson = new Gson();
        fns = gson.fromJson(fileReader, fnames.class);

        path = "C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\Server\\json\\locations.json";
        fileReader = new FileReader(path);
        gson = new Gson();
        locs = gson.fromJson(fileReader, locations.class);

        path = "C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\Server\\json\\mnames.json";
        fileReader = new FileReader(path);
        gson = new Gson();
        mns = gson.fromJson(fileReader, mnames.class);

        path = "C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\Server\\json\\snames.json";
        fileReader = new FileReader(path);
        gson = new Gson();
        sns = gson.fromJson(fileReader, snames.class);
    }
}
