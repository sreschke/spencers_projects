package Service;

import java.util.Set;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.Person;
import Model.User;
import Response.AllPersonResponse;

public class AllPersonService {

    /**
     * Returns all family members of current user. The current user is determined
     * from the provided auth token.
     * @param authTok
     * @return an AllPersonResponse object with a Person array
     */
    public AllPersonResponse serve(String authTok) {

        AllPersonResponse apr = new AllPersonResponse();

        if (authTok == null) {
            apr.setMessage("The authTok was empty.");
            return apr;
        }

        DataBase db = new DataBase();
        try {
            db.openConnection();


            //Get userName with authToken
            String userName = db.getAuthTokDao().getUser(authTok, db.getConn());

            if (userName == null) {
                //We got a bogus auth token
                apr.setMessage("Invalid authorization token");
                db.closeConnection(false);
                return apr;
            }

            //Get all people associated with userName
            Set<Person> people = db.getPersonDao().getAllPeople(userName, db.getConn());

            //Load people in response
            apr.setData(people);
            db.closeConnection(true);
            return apr;

        } catch (DatabaseException e) {
            e.printStackTrace();
            apr.setMessage("Internal Server Error");
            return apr;
        }
    }
}
