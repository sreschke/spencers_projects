package Response;

public class LoadResponse {
    String message;

    public LoadResponse(String message) {
        this.message = message;
    }

    public LoadResponse() {message = null;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
