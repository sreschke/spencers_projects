package Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import DataAccess.DatabaseException;
import Generator.Fill_Generator;
import Model.AuthToken;
import Model.Event;
import Model.Person;
import Model.User;
import Request.RegisterRequest;
import Response.RegisterResponse;
import DataAccess.DataBase;
import Server.InternalServerException;

import com.google.gson.Gson;

public class RegisterService {

    /**
     * Register a RegisterRequest
     * @param req
     * @return a registerResponse object
     */
    public RegisterResponse serve(RegisterRequest req) {
        String user_taken_message = "Username already taken by another user.";

        //Create Response (we'll return this)
        RegisterResponse rr = null;
        DataBase db = null;
        try {
            //Create DataBase object
            db = new DataBase();
            db.openConnection();

            //Create Response (we'll return this)
            rr = new RegisterResponse();

            //Check if username is already in database
            boolean in_db = db.getUserDao().checkForUser(req.getUserName(), db.getConn());
            db.closeConnection(false);
            db.openConnection();
            //If the username is already in use, set the message and return
            if (in_db) {
                rr.setMessage(user_taken_message);
                db.closeConnection(false);
                return rr;
            }

            //Create a new user account
            //Generate personID
            String personID = UUID.randomUUID().toString();

            //Get user from request using Gson
            Gson gson = new Gson();
            String data = gson.toJson(req);
            User u = gson.fromJson(data, User.class);
            //Set person ID since it's not included in the request
            u.setPersonID(personID);

            //Generate 4 generations of ancestor data
            Fill_Generator fg = new Fill_Generator(req.getUserName(), 4);
            fg.setU_person_ID(personID);
            fg.setUser(u);
            fg.generateFamily();

            Set<Event> events_to_add = fg.getEvents_to_add();
            Set<Person> people_to_add = fg.getPeople_to_add();

            //Add user to database
            db.getUserDao().createUser(u, db.getConn());

            //Create new row in AuthToks table
            String authTok = UUID.randomUUID().toString();
            AuthToken at = new AuthToken(authTok, u.getUserName());
            db.getAuthTokDao().createAuthTok(at, db.getConn());


            //Add Events to databse
            for (Event e: events_to_add) {
                db.getEventDao().createEvent(e, db.getConn());
            }

            //Add Persons to databse
            for (Person p: people_to_add) {
                db.getPersonDao().createPerson(p, db.getConn());
            }


            //Set Auth Token, username, and personID in response
            rr.setAuthToken(authTok);
            rr.setUserName(u.getUserName());
            rr.setPersonID(personID);
            db.closeConnection(true);
            return rr;

        }
        catch (DatabaseException e) {
            e.printStackTrace();
            rr.setMessage("Internal Server Error");
        }
        return rr;

    }
}
