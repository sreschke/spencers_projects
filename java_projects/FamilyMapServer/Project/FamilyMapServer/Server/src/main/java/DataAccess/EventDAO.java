package DataAccess;

import Model.Event;
import Model.Person;

import java.util.HashSet;
import java.util.Set;
import java.sql.*;

import javax.xml.bind.DataBindingException;

public class EventDAO {
    private String table_Name = "Event";
    /**
     * Create a new event row in the database
     * @param e the event object that is to be inserted
     * @return true if the addition was successful, false otherwise
     */
    public void createEvent(Event e, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "insert into " + table_Name + " VALUES " + e.getColumnValues();
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("createPerson failed: could not insert user");
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("createPerson failed", ex);
        }
    }

    /**
     * get an event from the database using its ID and the corresponding username
     * @param eventID
     * @return
     */
    public Event getEvent(String eventID, Connection conn) throws DatabaseException {

        Event e = null;
        try {
            Statement stmt = null;
            ResultSet rs;
            try {
                String sql = "SELECT * From " + table_Name + " where Event = '" + eventID + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty (i.e. the user is not in the database
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String event = rs.getString("Event");
                    String descendant = rs.getString("Descendant");
                    String person = rs.getString("Person");
                    String lat = rs.getString("Latitude");
                    String lon = rs.getString("Longitude");
                    String country = rs.getString("Country");
                    String city = rs.getString("City");
                    String eType = rs.getString("EventType");
                    String year = rs.getString("Year");
                    e = new Event(event, descendant, person, lat, lon, country, city, eType, year);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getEvent failed", ex);
        }
        return e;
    }

    /**
     * Get all of the events associated with a given username
     * @param userName
     * @return
     */
    public Set<Event> getAllEvents(String userName, Connection conn) throws DatabaseException {

        Set<Event> es = new HashSet<>();
        Event e = null;
        try {
            Statement stmt = null;
            ResultSet rs;
            try {
                String sql = "SELECT * From " + table_Name + " where Descendant = '" + userName + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty (i.e. the user is not in the database
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String event = rs.getString("Event");
                    String descendant = rs.getString("Descendant");
                    String person = rs.getString("Person");
                    String lat = rs.getString("Latitude");
                    String lon = rs.getString("Longitude");
                    String country = rs.getString("Country");
                    String city = rs.getString("City");
                    String eType = rs.getString("EventType");
                    String year = rs.getString("Year");
                    e = new Event(event, descendant, person, lat, lon, country, city, eType, year);
                    es.add(e);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getPerson failed", ex);
        }
        return es;
    }

    /**
     * Clears the database of all events
     */
    public void clear(Connection conn) throws DatabaseException {

        try {
            Statement stmt = null;
            try {
                String sql = "Delete from " + table_Name;
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("clear failed", ex);
        }
    }

    /**
     * Delete an event (row) from the database given its event ID
     * @param eventID
     * @return true if successful, false otherwise
     */
    public void deleteEvent(String eventID, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "DELETE From " + table_Name + " where Event = '" + eventID + "'";
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("deletePerson failed: could not delete" + eventID);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteEvent failed", ex);
        }

    }

    /**
     * Validates whether a given eventID belongs to a given userName
     * @param userName
     * @param eventID
     * @param conn
     * @return
     * @throws DatabaseException
     */
    public boolean validateEvent(String userName, String eventID, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "SELECT * FROM " + table_Name + " WHERE Descendant = '" + userName + "' AND Event = '" + eventID + "';";
                stmt = conn.createStatement();

                ResultSet rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty. The userName/eventID pair was invalid
                    return false;
                }

                else {
                    //The query was non-empty. The userName/eventID pair was valid
                    return true;
                }

            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteEvent failed", ex);
        }
    }
}
