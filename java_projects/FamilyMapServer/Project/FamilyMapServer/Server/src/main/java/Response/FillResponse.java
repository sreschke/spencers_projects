package Response;

public class FillResponse {
    String message;

    public FillResponse(String message) {
        this.message = message;
    }

    public FillResponse() {message = null;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
