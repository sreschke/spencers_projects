package DataAccess;

import Model.Person;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.crypto.Data;

public class PersonDAO {

    private String table_Name = "Person";

    /**
     * Creates a new person row in the database given Person p
     * @param p
     * @return true if the person was successfully added, false otherwise
     */
    public void createPerson(Person p, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "insert into " + table_Name + " VALUES " + p.getColumnValues();
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("createPerson failed: could not insert user");
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("createPerson failed", ex);
        }
    }

    /**
     * Get person with personID that belongs to User with userName
     * @param personID
     * @return the queried Person if in the database, null otherwise
     */
    public Person getPerson(String personID, Connection conn) throws DatabaseException {
        Person p = null;
        try {
            Statement stmt = null;
            ResultSet rs;
            try {
                String sql = "SELECT * From " + table_Name + " where Person = '" + personID + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty (i.e. the user is not in the database
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String person = rs.getString("Person");
                    String descendant = rs.getString("Descendant");
                    String first = rs.getString("first");
                    String last = rs.getString("Last");
                    String gender = rs.getString("Gender");
                    String father = rs.getString("Father");
                    String mother = rs.getString("Mother");
                    String spouse = rs.getString("Spouse");
                    p = new Person(person, descendant, first, last, gender, father, mother, spouse);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getPerson failed", ex);
        }
        return p;
    }

    /**
     * Get all the people associated with User with specified userName.
     * @param userName
     * @return a set of Person objects
     */
    public Set<Person> getAllPeople(String userName, Connection conn) throws DatabaseException {
        Set<Person> ps = new HashSet<>();
        Person p = null;
        try {
            Statement stmt = null;
            ResultSet rs;
            try {
                String sql = "SELECT * From " + table_Name + " where Descendant = '" + userName + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty (i.e. the user is not in the database
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String person = rs.getString("Person");
                    String descendant = rs.getString("Descendant");
                    String first = rs.getString("first");
                    String last = rs.getString("Last");
                    String gender = rs.getString("Gender");
                    String father = rs.getString("Father");
                    String mother = rs.getString("Mother");
                    String spouse = rs.getString("Spouse");
                    p = new Person(person, descendant, first, last, gender, father, mother, spouse);
                    ps.add(p);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getPerson failed", ex);
        }
        return ps;
    }

    /**
     * clears all People from the database
     * @return true if successful, false otherwise
     */
    public void clear(Connection conn) throws DatabaseException {

        try {
            Statement stmt = null;
            try {
                String sql = "Delete from " + table_Name;
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("clear failed", ex);
        }
    }


    /**
     * Delete a person from the database with personID
     * @param personID
     * @return true if deleted successfully, false otherwise
     */
    public void deletePerson(String personID, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "DELETE From " + table_Name + " where Person = '" + personID + "'";
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("deletePerson failed: could not delete" + personID);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteEvent failed", ex);
        }
    }

    /**
     * Validates whether a given personID belongs to a given userName
     * @param userName
     * @param personID
     * @param conn
     * @return
     * @throws DatabaseException
     */
    public boolean validatePerson(String userName, String personID, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "SELECT * FROM " + table_Name + " WHERE Descendant = '" + userName + "' AND Person = '" + personID + "';";
                stmt = conn.createStatement();

                ResultSet rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty. The userName/personID pair was invalid
                    return false;
                }

                else {
                    //The query was non-empty. The userName/PersonID pair was valid
                    return true;
                }

            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteEvent failed", ex);
        }
    }
}


