package Handler;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import Response.FillResponse;
import Service.FillService;

public class FillHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        boolean success = false;

        try {
            if (exchange.getRequestMethod().toLowerCase().equals("post")) {
                //Get userName and numGenerations from exchange
                String path = exchange.getRequestURI().toString();
                String[] split = path.split("/");
                String userName = null;
                String numGen = null;

                if (split.length == 4) {
                    userName = split[2];
                    numGen = split[3];
                }
                else if (split.length == 3) {
                    userName = split[2];
                    numGen = "4";
                }

                int numGenerations = Integer.parseInt(numGen);
                FillService fs = new FillService();
                FillResponse res = fs.serve(userName, numGenerations);
                Gson gson = new Gson();
                String rdata = gson.toJson(res);

                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                OutputStream respBody = exchange.getResponseBody();
                writeString(rdata, respBody);
                respBody.close();
                success = true;
            }

            if (!success) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                exchange.getResponseBody().close();
            }

        } catch (IOException e) {
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
            exchange.getResponseBody().close();
            e.printStackTrace();
        }

    }

    private void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }
}
