package Generator;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import DataAccess.DataBase;
import DataAccess.DatabaseException;
import Model.AuthToken;
import Model.Event;
import Model.Person;
import Model.User;
import Server.InternalServerException;

public class Fill_Generator {
    private int numGenerations;
    private int curGeneration = 0;

    private int userBirthYear;
    private final int AGE_GAP = 40;
    private String userName;
    private DataBase db;
    private Generator gen;
    private User user;
    private Person u_person;
    private String u_person_ID;

    public Set<Event> getEvents_to_add() {
        return events_to_add;
    }

    public void setEvents_to_add(Set<Event> events_to_add) {
        this.events_to_add = events_to_add;
    }

    public Set<Person> getPeople_to_add() {
        return people_to_add;
    }

    public void setPeople_to_add(Set<Person> people_to_add) {
        this.people_to_add = people_to_add;
    }

    public Set<Event> getEvents_to_delete() {
        return events_to_delete;
    }

    public void setEvents_to_delete(Set<Event> events_to_delete) {
        this.events_to_delete = events_to_delete;
    }

    public Set<Person> getPeople_to_delete() {
        return people_to_delete;
    }

    public void setPeople_to_delete(Set<Person> people_to_delete) {
        this.people_to_delete = people_to_delete;
    }

    private Set<Event> events_to_add = new HashSet<>();
    private Set<Person> people_to_add = new HashSet<>();
    private Set<Event> events_to_delete = new HashSet<>();
    private Set<Person> people_to_delete = new HashSet<>();

    /**
     * Constructor for Fill_Generator. Need to specify the userName whose ancestors will be
     * generated as well as how many generations to generate.
     * @param userName userName of the user
     * @param numGenerations how many generation to generate
     */
    public Fill_Generator(String userName, int numGenerations) {
        db = new DataBase();
        this.numGenerations = numGenerations;
        this.userName = userName;
        try {
            gen = new Generator();
        } catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Main function used to generate family data
     */
    public void generateFamily() {
        //Assumes the userName is in the database (this is checked in server() from which
        //this function is called)

        //Delete all data associated with userName
        deleteData();

        //Create person events for userName
        makeUPersonEvents();

        //generate ancestral person and event objects
        generateAncestors();
    }

    /**
     * Makes the person and event objects for the user.
     */
    private void makeUPersonEvents() {
            //Generate people Id's
            String father_ID = UUID.randomUUID().toString();
            String mother_ID = UUID.randomUUID().toString();

            //Create person object for userName
            u_person = new Person(u_person_ID,
                    userName,
                    user.getFirstName(),
                    user.getLastName(),
                    user.getGender(),
                    father_ID,
                    mother_ID,
                    null);
            people_to_add.add(u_person);

            //createEvents for userName
            //createBirth
            String b_ID = UUID.randomUUID().toString();

            location loc = getRandomLocation();
            userBirthYear = Integer.parseInt(getRandomYear(1975, 2000));
            Event u_birth = new Event(b_ID,
                    userName,
                    u_person_ID,
                    Float.toString(loc.latitude),
                    Float.toString(loc.longitude),
                    loc.country,
                    loc.city,
                    "Birth",
                    Integer.toString(userBirthYear));
            events_to_add.add(u_birth);
    }

    private void deleteData() {
        try {
            //Delete all events associated with userName
            db.openConnection();
            Set<Event> events = db.getEventDao().getAllEvents(userName, db.getConn());
            db.closeConnection(false);
            if (events != null) {
                for (Event e : events) {
                    events_to_delete.add(e);
                }
            }

            //Delete all people associated with userName
            db.openConnection();
            Set<Person> people = db.getPersonDao().getAllPeople(userName, db.getConn());
            db.closeConnection(false);
            if (people != null) {
                for (Person p : people) {
                    people_to_delete.add(p);
                }
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate the people and event objects for numGenerations generations of family.
     * @throws DatabaseException
     */
    private void generateAncestors() {
        ArrayList<Person> people = new ArrayList<>();
        people.add(u_person);

        //Start Recursive function
        _step(people);

    }

    /**
     * recursive helper function for generateAncestors
     * @param people a list of people objects to make parents for.
     * @throws DatabaseException
     */
    private void _step(ArrayList<Person> people) {
        curGeneration+=1;
        //Base case: we've finished
        if (curGeneration >= numGenerations+1) {
            return;
        }

        //Iterate through list of person objects and create parents for each person
        ArrayList<Person> ancestors = new ArrayList<>();
        for (Person p: people) {
            generateParents(p, ancestors);
        }
        _step(ancestors);
    }

    /**
     * Generate the parents of a person object
     * @param child the person object who we want to create parents for.
     * @param ancestors a list of people to add the created parents to.
     * @throws DatabaseException
     */
    private void generateParents(Person child, ArrayList<Person> ancestors) {
        String father_id = child.getFather();
        String mother_id = child.getMother();

        String descendant = child.getDescendant();
        String last_name = getLastName(child);

        String next_father_id;
        String next_mother_id;
        if (curGeneration == numGenerations) {
            next_father_id = null;
            next_mother_id = null;
        } else {
            next_father_id = UUID.randomUUID().toString();
            next_mother_id = UUID.randomUUID().toString();
        }

        //Create mother person object
        Person mother = new Person(mother_id,
                descendant,
                getRandomFName(),
                last_name,
                "f",
                next_father_id,
                next_mother_id,
                father_id);
        people_to_add.add(mother);

        //Create father person object
        Person father = new Person(father_id,
                descendant,
                getRandomMName(),
                last_name,
                "m",
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                mother_id);
        people_to_add.add(father);

        //add parents to ancestors
        ancestors.add(mother);
        ancestors.add(father);

        //Generate events for parents
        generateEvents(mother, father);
    }

    /**
     * Generates the births and marriages of mother and father objects
     * @param mother person object for mother
     * @param father person object for father
     * @throws DatabaseException
     */
    private void generateEvents(Person mother, Person father) {
        //Create birth event for Mother
        location loc = getRandomLocation();
        String birth_year = Integer.toString(userBirthYear-AGE_GAP*curGeneration);
        String event_type = "Birth";
        Event m_birth = new Event(UUID.randomUUID().toString(),
                userName,
                mother.getPersonID(),
                Float.toString(loc.latitude),
                Float.toString(loc.longitude),
                loc.country,
                loc.city,
                "Birth",
                birth_year);
        events_to_add.add(m_birth);

        //Create birth event for Father
        loc = getRandomLocation();
        birth_year = Integer.toString(userBirthYear-AGE_GAP*curGeneration);
        Event f_birth = new Event(UUID.randomUUID().toString(),
                userName,
                father.getPersonID(),
                Float.toString(loc.latitude),
                Float.toString(loc.longitude),
                loc.country,
                loc.city,
                event_type,
                birth_year);
        events_to_add.add(f_birth);

        //Generate marriage events for mother and father
        //Create birth event for Mother
        location marr_loc = getRandomLocation();
        String marriage_year = Integer.toString(Integer.parseInt(m_birth.getYear())+20);
        event_type = "Marriage";
        Event m_marriage = new Event(UUID.randomUUID().toString(),
                userName,
                mother.getPersonID(),
                Float.toString(marr_loc.latitude),
                Float.toString(marr_loc.longitude),
                marr_loc.country,
                marr_loc.city,
                event_type,
                marriage_year);

        events_to_add.add(m_marriage);

        Event f_marriage = new Event(UUID.randomUUID().toString(),
                userName,
                father.getPersonID(),
                Float.toString(marr_loc.latitude),
                Float.toString(marr_loc.longitude),
                marr_loc.country,
                marr_loc.city,
                event_type,
                marriage_year);

        events_to_add.add(f_marriage);
    }

    private String getRandomMName() {
        Random rg = new Random();
        int index = rg.nextInt(gen.mns.data.size());
        return gen.mns.data.get(index);
    }

    private String getRandomFName() {
        Random rg = new Random();
        int index = rg.nextInt(gen.fns.data.size());
        return gen.fns.data.get(index);
    }

    private String getRandomYear(int min, int max) {
        Random rg = new Random();
        return Integer.toString(rg.nextInt(max-min+1) + min);
    }

    private location getRandomLocation() {
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(gen.sns.data.size());
        return gen.locs.data.get(index);
    }

    private String getLastName(Person p) {
        String l_name = p.getLastName();
        String gender = p.getGender();

        if (gender.equals("m") || gender.equals("M")) {
            return l_name;
        }
        else { //Generate random last name
            Random randomGenerator = new Random();
            int index = randomGenerator.nextInt(gen.sns.data.size());
            return gen.sns.data.get(index);
        }
    }

    public String getU_person_ID() {
        return u_person_ID;
    }

    public void setU_person_ID(String u_person_ID) {
        this.u_person_ID = u_person_ID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}