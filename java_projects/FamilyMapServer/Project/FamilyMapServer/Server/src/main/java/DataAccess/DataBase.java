package DataAccess;

import java.sql.*;

import Model.Person;
import Model.User;

public class DataBase {
    //Load Database Driver (see Rhodam's Database.java class)
    static {
        try{
            final String driver = "org.sqlite.JDBC";

            //Register the JDBC Driver
            Class.forName(driver);
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //class members
    private UserDAO userDao = new UserDAO();
    private PersonDAO personDao = new PersonDAO();
    private EventDAO eventDao = new EventDAO();
    private AuthTokenDAO authTokDao = new AuthTokenDAO();
    private Connection conn;
    private String CONNECTION_URL;
    private String db_Name;

    private int userTableSize = 0;
    private int personTableSize = 0;
    private int eventTableSize = 0;
    private int authTableSize = 0;


    //Constructor to call if you want to create a new database
    public DataBase(String db_path) {
        CONNECTION_URL = "jdbc:sqlite:" + db_path;
        db_Name = db_path;
    }

    //Default constructor
    public DataBase() {
        //CONNECTION_URL = "jdbc:sqlite:"+"family_map_database.db";
        CONNECTION_URL = "jdbc:sqlite:"+"C:\\Users\\spenc\\LifeOfSpencer\\School\\CS\\CS 240\\FamilyMapServerProject\\Server\\Project\\FamilyMapServer\\family_map_database.db";
        db_Name = "family_map_database";
    }


    /**
     * opens a connection with the database
     * @throws DatabaseException if the connection fails
     */
    public void openConnection() throws DatabaseException {
        try {
            // Open a database connection
            conn = DriverManager.getConnection(CONNECTION_URL);

            // Start a transaction
            conn.setAutoCommit(false);
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("openConnection failed", e);
        }
    }

    /**
     * Closes the connection with the database
     * @param commit
     * @throws DatabaseException
     */
    public void closeConnection(boolean commit) throws DatabaseException {
        try {
            if (commit) {
                conn.commit();
            }
            else {
                conn.rollback();
            }


        }
        catch (SQLException e) {
            e.printStackTrace();
//            throw new DatabaseException("closeConnection failed", e);
        }
        finally {
            try {
                conn.close();
                conn = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }





    /**
     * clears the database by calling the clear methods on all of the DAO objects
     */
    public void clear() throws DatabaseException {
//        //Fixme: for some reason I have to add something to the database in order to clear it
//        User u = new User("sreschke",
//                "password1",
//                "spencerreschke@gmail.com",
//                "Spencer",
//                "Reschke",
//                "M",
//                "someID");
//        userDao.createUser(u, conn);
        userDao.clear(conn);
        personDao.clear(conn);
        eventDao.clear(conn);
        authTokDao.clear(conn);
    }

    public UserDAO getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    public PersonDAO getPersonDao() {
        return personDao;
    }

    public void setPersonDao(PersonDAO personDao) {
        this.personDao = personDao;
    }

    public EventDAO getEventDao() {
        return eventDao;
    }

    public void setEventDao(EventDAO eventDao) {
        this.eventDao = eventDao;
    }

    public AuthTokenDAO getAuthTokDao() {
        return authTokDao;
    }

    public void setAuthTokDao(AuthTokenDAO authTokDao) {
        this.authTokDao = authTokDao;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public String getConn_URL() {
        return CONNECTION_URL;
    }

    public void setCONNECTION_URL(String CONNECTION_URL) {
        this.CONNECTION_URL = CONNECTION_URL;
    }

    public String getDb_Name() {
        return db_Name;
    }

    public void setDb_Name(String db_Name) {
        this.db_Name = db_Name;
    }
}
