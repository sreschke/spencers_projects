package Response;

public class ClearResponse {
    String message;

    public ClearResponse(String message) {
        this.message = message;
    }

    public ClearResponse() {message = null;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
