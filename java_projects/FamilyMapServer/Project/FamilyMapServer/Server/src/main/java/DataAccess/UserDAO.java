package DataAccess;

import Model.User;
import java.sql.*;

import javax.xml.crypto.Data;

public class UserDAO {
    private String table_Name = "User";

    /**
     * Add a user (row) to the database
     * @param u the User object to add to the database
     * @return true if creation was successful, false otherwise
     */
    public boolean createUser(User u, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "insert into " + table_Name + " VALUES " + u.getColumnValues();
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("createUser failed: could not insert user");
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                    return true;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("createEvent failed", ex);
        }
        return false;
    }

    /**
     * Get a user from the database
     * @param userName
     * @return a user Object
     */
    public User getUser(String userName, Connection conn) throws DatabaseException {
        User u = null;
        try {
            Statement stmt = null;
            ResultSet rs = null;
            try {
                String sql = "SELECT * From User where Username = '" + userName + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty
                    return null;
                }

                //Grab data from ResultSet
                while (rs.next()) {
                    String username = rs.getString("Username");
                    String password = rs.getString("Password");
                    String email = rs.getString("Email");
                    String fn = rs.getString("First");
                    String ln = rs.getString("Last");
                    String gen = rs.getString("Gender");
                    String pi = rs.getString("Person");
                    u = new User(username, password, email, fn, ln, gen, pi);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getEvent failed", ex);
        }
        return u;
    }

    /**
     * Clears all users (rows) from the database
     */
    public void  clear(Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "DELETE FROM " + table_Name + ";";
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);

            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("clear failed", ex);
        }
    }

    /**
     * deletes a user from the database with a given userName
     * @param userName
     */
    public void deleteUser(String userName, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            try {
                String sql = "DELETE From User where Username = '" + userName + "'";
                stmt = conn.createStatement();

                if (stmt.executeUpdate(sql) != 1) {
                    throw new DatabaseException("deleteUser failed: could not delete" + userName);
                }
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("deleteEvent failed", ex);
        }

    }

    /**
     * Check if a given userName is in the database
     * @param userName the userName to check
     * @param conn
     * @return true if the userName is in the database, false otherwise
     * @throws DatabaseException
     */
    public boolean checkForUser(String userName, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            ResultSet rs = null;
            try {
                String sql = "SELECT * From " + table_Name + " where Username = '" + userName + "'";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty
                    return false;
                } else {
                    return true;
                }
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("getEvent failed", ex);
        }
    }

    /**
     * Checks to see if a given userName and password are in the database
     * @param userName the userName to validate
     * @param password the password to validate
     */
    public boolean validateUser(String userName, String password, Connection conn) throws DatabaseException {
        try {
            Statement stmt = null;
            ResultSet rs = null;
            try {
                String sql = "SELECT * From " + table_Name + " where Username = '" + userName + "' AND Password = '" + password + "';";
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (!rs.isBeforeFirst()) {
                    //The query was empty
                    return false;
                } else {
                    return true;
                }
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DatabaseException("validateUser failed", ex);
        }
    }
}

