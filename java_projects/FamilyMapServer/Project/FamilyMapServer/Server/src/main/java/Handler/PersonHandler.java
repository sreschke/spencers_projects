package Handler;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import Response.PersonResponse;
import Service.PersonService;

public class PersonHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        boolean success = false;

        try {
            if (exchange.getRequestMethod().toLowerCase().equals("get")) {
                //Get request headers
                Headers reqHeaders = exchange.getRequestHeaders();

                //Check if an "Authorization" header is present
                if (reqHeaders.containsKey("Authorization")) {
                    //extract the auth token from the "Authorization" header
                    String authToken = reqHeaders.getFirst("Authorization");

                    //get personID
                    String path = exchange.getRequestURI().toString();
                    String[] split = path.split("/");
                    String personID = null;
                    if (split.length == 3) {
                        personID = split[2];
                    }
                    else {
                        //FIXME handle the case if we get here
                        success = false;
                    }

                    PersonService ps = new PersonService();
                    PersonResponse res = new PersonResponse();
                    res = ps.serve(authToken, personID);
                    Gson gson = new Gson();
                    String data = gson.toJson(res);

                    exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                    OutputStream respBody = exchange.getResponseBody();
                    writeString(data, respBody);
                    respBody.close();
                    success = true;
                }

            }
            if (!success) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                exchange.getResponseBody().close();
            }

        } catch (IOException e) {
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
            exchange.getResponseBody().close();
            e.printStackTrace();
        }

    }

    private void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }

}
