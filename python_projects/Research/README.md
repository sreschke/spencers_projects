# Knot_MDP (Spencer's Latest Research Project)

INTRODUCTION

Low-dimensional topology has numerous examples of problems whose solutions require constructing sequences of operations taken
from a fixed set of moves. In knot theory, constructing genus-minimizing slice surfaces of a knot is an example of such a 
problem. To solve this problem, this project seeks to use recent advances in Deep Learning, such as double Q-Learning, Dueling Architextures,
Prioritized Experience Replay, and Asynchronous Methods.

BACKGROUND

The problem can formulated as a Markov Decision Process (MDP), consisting of a state space, a set of actions, a state transition
probability distribution, a reward function, and a discount factor. Once formulated as a MDP, we can apply a variety
of Reinforcement Learning Algorithms in an attempt to solve it. All such algorithms seek to find the optimal action given any state 
that maximizes the expected discounted sum of future rewards.

In this project, we implement a few select Deep Learning algorithms, as described in the following papers:

(1) Playing Atari with Deep Reinforcement Learning (2013) by Google DeepMind authored by Hado van Hasselt et al: https://arxiv.org/pdf/1509.06461.pdf
Summary: Discusses the idea of a Deep Q Network (DQN), a neural network parameterization of the tabular methods traditionally used in Q-learning. The paper also discusses the use of Experience Replay, a buffer storing transitions which are used to train the network during learning.
 
(2) Deep Reinforcement Learning with Double Q-learning (2015) by Google DeepMind authored by Ziyu Wang et. al.: https://arxiv.org/pdf/1511.06581.pdf
A detailed algorithm is given here: http://coach.nervanasys.com/algorithms/value_optimization/double_dqn/index.html
Summary: Introduces the idea of using TWO networks, an online network and a target network. This approach helps stabalize training by separating action selection from action evaluation. The target network's weights are frozen while the online network is updated at every time step. After every 1000 or so iterations, the online weights are copied to the target weights.
 
(3) Dueling Network Architectures for Deep Reinforcement Learning (2016)
by Google DeepMind authored by Ziyu Wang et. al.: https://arxiv.org/pdf/1511.06581.pdf
Summary: Introduces the Dueling Architexture (see figure 1 on page 1) which decouples estimates of the state value and advantage functions.
 
(4) Prioritized Experience Replay (2016)
by Google DeepMind authored by Tom Schaul et. al: https://arxiv.org/pdf/1511.05952.pdf
Summary: Rather than uniformly sampling from the replay buffer, prioritized expereince replay 
samples important (s, a, r, s', t) transitions more frequently which leads to more 
efficient learning.

(5) Asynchronous Methods for Deep Reinforcement Learning (2016)
by Google DeepMind authored by Volodymyr Mnih et al: https://arxiv.org/pdf/1602.01783.pdf
Summary: Introduces asynchronous methods such as Asynchronous 1-step Q-learning (Algorithm 1 in paper).
These approaches use multiple agents in parallel whose learning is agregated in a global network.
Eliminates the need for experience replay and can be trained on CPUs rather than GPUs.
 
Neural Networks allow the algorithm to generalize learning across similar
states and thus allow us to tackle problems with larger state spaces while using
less memory. The extra bells and whistles help to stabilize and speed-up training.
