# regular_expressions.py
"""Volume 3: Regular Expressions.
Spencer Reschke
Math 403
9/3/2019
"""
import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    #Compile a pattern object with patter string 'python'
    pat = re.compile("python")
    return pat

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pat = re.compile(r"\^\{@\}\(\?\)\[%\]\{\.\}\(\*\)\[_\]\{&\}\$")
    return pat

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pat = re.compile(r"^(Book|Mattress|Grocery) (store|supplier)$")
    return pat

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
#    #Python identifier regex
#    py_i_re = r"^[a-zA-Z_][\w]*$"
#    
#    #any number of spaces regex
#    second = r"[ ]*"
#    
#    #(optional) an equals sign followed by any number of spaces and ending
#    #with one of the following: any real number, a single quote followed by any
#    #number of non-single-quote characters followed by a single quote, or any
#    #valid python identifer regex
#    third = r"(^=[ ]*(^[\d]*(\.[\d]*)?$|^'[^']*'$|^[a-zA-Z_][\w]*$){1}){1}?$"
#    
#    final = py_i_re + second + third
#    pat = re.compile(final)
    
    
    p = re.compile(r"^[a-zA-Z_][\w]*[ ]*(=[ ]*(([\d]*(.[\d]*)?)|(\'[^\']*\')|([a-zA-Z_][\w]*)))?$")
    
    return p

# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """
    raise NotImplementedError("Problem 5 Incomplete")

# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """

    raise NotImplementedError("Problem 6 Incomplete")
    
    
def test_prob4():
    matches = ["Mouse", "compile", "_123456789", "__x__", "while", "max=4.2",
               "string= ''", "num_guesses"]
    
    non_matches = ["3", "3rats", "err*r", "sq(x)", "sleep()", " x", "300",
                   "is_4=(value==4)", "pattern = r'^one|two fish$'"]
    
    
    pat = prob4()
    for test in matches:
        #FIXME: should this be search or match? 
        assert bool(pat.search(test)), "{}: failed".format(test)
        
    for test in non_matches:
        assert not bool(pat.search(test)), "{}: failed".format(test)
        
        
def test_prob5():
     pass

        
        
if __name__ == "__main__":
    test_prob4()
        
