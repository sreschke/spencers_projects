# solutions.py
"""Volume 3: Web Technologies. Solutions File."""

import json
import socket
import matplotlib.pyplot as plt
import numpy as np
import random

# Problem 1
def prob1(filename="nyc_traffic.json"):
    """Load the data from the specified JSON file. Look at the first few
    entries of the dataset and decide how to gather information about the
    cause(s) of each accident. Make a readable, sorted bar chart showing the
    total number of times that each of the 7 most common reasons for accidents
    are listed in the data set.
    """
    
    with open(filename, "r") as infile:
        #Load data
        accident_data = json.load(infile)
        
        #Get and count all reasons 
        reasons = {}
        for accident in (accident_data):
            for factor in ["contributing_factor_vehicle_1", "contributing_factor_vehicle_2"]:
                try:
                    reason = accident[factor]
                    if reason not in reasons.keys():
                        reasons[reason] = 1
                    else:
                        reasons[reason] += 1
                        
                except:
                    pass            
    
    #Sort reasons by key value        
    sorted_reasons = sorted(reasons.items(), key=lambda kv: kv[1])
    
    #Make bar chart
    #Get top 7
    top7 = sorted_reasons[-7:]
    labels, counts = zip(*top7)
    x_pos = np.arange(len(labels))
    plt.bar(x_pos, counts, align="center", alpha=0.5)
    plt.xticks(x_pos, labels, rotation="vertical")
    plt.ylabel("Count") 
    plt.title("NYC traffic")
    plt.tight_layout()
    plt.show()   


class TicTacToe:
    def __init__(self):
        """Initialize an empty board. The O's go first."""
        self.board = [[' ']*3 for _ in range(3)]
        self.turn, self.winner = "O", None

    def move(self, i, j):
        """Mark an O or X in the (i,j)th box and check for a winner."""
        if self.winner is not None:
            raise ValueError("the game is over!")
        elif self.board[i][j] != ' ':
            raise ValueError("space ({},{}) already taken".format(i,j))
        self.board[i][j] = self.turn

        # Determine if the game is over.
        b = self.board
        if any(sum(s == self.turn for s in r)==3 for r in b):
            self.winner = self.turn     # 3 in a row.
        elif any(sum(r[i] == self.turn for r in b)==3 for i in range(3)):
            self.winner = self.turn     # 3 in a column.
        elif b[0][0] == b[1][1] == b[2][2] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        elif b[0][2] == b[1][1] == b[2][0] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        else:
            self.turn = "O" if self.turn == "X" else "X"

    def empty_spaces(self):
        """Return the list of coordinates for the empty boxes."""
        return [(i,j) for i in range(3) for j in range(3)
                                        if self.board[i][j] == ' ' ]
    def __str__(self):
        return "\n---------\n".join(" | ".join(r) for r in self.board)


# Problem 2
class TicTacToeEncoder(json.JSONEncoder):
    """A custom JSON Encoder for TicTacToe objects."""
    def default(self, obj):
        #Check whether obj is a TicTacToe instance
        if not isinstance(obj, TicTacToe):
            raise TypeError("expected a TicTacToe object for encoding")
            
        #Store board, turn and winner members
        return {"board": obj.board, "turn": obj.turn, "winner": obj.winner}

# Problem 2
def tic_tac_toe_decoder(obj):
    """A custom JSON decoder for TicTacToe objects."""
    
    if "board" in obj:
        if type(obj["board"]) != list:
            raise ValueError("expected a JSON message from TicTacToeEncoder")
    if "turn" in obj:
        if obj["turn"] not in ["O", "X"]:
            raise ValueError("expected a JSON message from TicTacToeEncoder")
    if "winner" in obj:
        if obj["winner"] not in [None, 'O', 'X']:
            raise ValueError("expected a JSON message from TicTacToeEncoder")
            
        ttt = TicTacToe()
        ttt.board, ttt.turn, ttt.winner = obj["board"], obj["turn"], obj["winner"]
        return ttt
    
    raise ValueError("expected a JSON message from TicTacToeEncoder")
            


def mirror_server(server_address=("0.0.0.0", 33333)):
    """A server for reflecting strings back to clients in reverse order."""
    print("Starting mirror server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:
        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()

        try:
            # Receive data from the client.
            print("Connection accepted from {}.".format(client_address))
            in_data = connection.recv(1024).decode()    # Receive data.
            print("Received '{}' from client".format(in_data))

            # Process the received data and send something back to the client.
            out_data = in_data[::-1]
            print("Sending '{}' back to the client".format(out_data))
            connection.sendall(out_data.encode())       # Send data.

        # Make sure the connection is closed securely.
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))

def mirror_client(server_address=("0.0.0.0", 33333)):
    """A client program for mirror_server()."""
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # Send some data from the client user to the server.
    out_data = input("Type a message to send: ")
    client_sock.sendall(out_data.encode())              # Send data.

    # Wait to receive a response back from the server.
    in_data = client_sock.recv(1024).decode()           # Receive data.
    print("Received '{}' from the server".format(in_data))

    # Close the client socket.
    client_sock.close()


# Problem 3
def tic_tac_toe_server(server_address=("0.0.0.0", 44444)):
    """A server for playing tic-tac-toe with random moves."""
    
    print("Starting Tic Tac Toe server on {}".format(server_address))
    
    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.
    
    while True:
        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()
        try:
            while True:
                #Receive JSON serialized TicTacToe object from client
                print("Connection accepted from {}.".format(client_address))
                sttt = connection.recv(1024).decode()    # Receive data.
                #print("Received '{}' from client".format(sttt))
                
                #Deserialize the TicTacToe object using custom decoder from problem 2
                ttt = json.loads(sttt, object_hook=tic_tac_toe_decoder)
                
                #If the client has won the game, sent "WIN" back to the client
                if ttt.winner:
                    #print("The client has won the game. Sending back 'WIN'...")
                    connection.sendall("WIN".encode())
                    #Close connection
                    break
                
                #If there is no winner but the board is full, send "DRAW" back to the client
                elif len(ttt.empty_spaces()) == 0:
                    #print("The game is drawn. Sending back 'DRAW'...")
                    assert ttt.winner is None, "Error: there should be no winner here"
                    connection.sendall("DRAW".encode())
                    #Close connection
                    break
                
                #Make random move
                else:
                    #Get random move
                    assert len(ttt.empty_spaces()) > 0, "There should be empty spaces here"
                    #print("Making random move...")
                    move = random.choice(ttt.empty_spaces())
                    
                    #Make move
                    ttt.move(move[0], move[1])
                    
                    if ttt.winner:
                        #print("The client has lost. Sending back 'LOSE' and updated TicTacToe object...")
                        #Send LOSE to client
                        connection.sendall("LOSE".encode())
                        
                        #Send serialized tictactoe object to client
                        sttt2 = json.dumps(ttt, cls=TicTacToeEncoder)                  
                        connection.sendall(sttt2.encode())
                        
                        #Close connection
                        break
                    
                    else:
                        #Send updated TicTacToe object to client
                        sttt2 = json.dumps(ttt, cls=TicTacToeEncoder)
                        #print("Sending back updated TicTacToe object".format(sttt2))
                        connection.sendall(sttt2.encode())
            
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))


# Problem 4
def tic_tac_toe_client(server_address=("0.0.0.0", 44444)):
    """A client program for tic_tac_toe_server()."""
    
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")
    
    print("Starting TicTacToe game...")
    #Initialize a new TicTacToe object
    ttt = TicTacToe()
    
    while ttt.winner is None and len(ttt.empty_spaces()) > 0:
        #Show board
        print(ttt)
        
        #Prompt for move
        rows, cols = zip(*ttt.empty_spaces())
        avail_moves = [str(r)+str(c) for r, c in zip(rows, cols)]
        move = input("Specify row/col\nAvailable moves {}: ".format(avail_moves))
        
        #Reprompt if invalid input
        while move not in avail_moves:
            print("Invalid Input")
            move = input("Specify row/col\nAvailable moves {}: ".format(avail_moves))
        print()    
        #Cast moves to integers
        row, col = int(move[0]), int(move[1])
        
        #Make move
        #print("Making move...")
        ttt.move(row, col)
        
        #Serialize ttt and sent to server
        sttt = json.dumps(ttt, cls=TicTacToeEncoder)
        #print("Sending game to server {}...".format(sttt))
        client_sock.sendall(sttt.encode())
        
        #Wait for response from server
        sttt2 = client_sock.recv(1024).decode()
        #print("Recieved {} from server".format(sttt2))
        
        if sttt2 == "WIN":
            print("You Win!")
            print("Final Board")
            print(ttt)
            #Close connection
            client_sock.close()
            return
            
        elif sttt2 == "LOSE":
            print("You Lose!")
            
            #Get final game from server
            sttt3 = client_sock.recv(1024).decode()
            ttt = json.loads(sttt3, object_hook=tic_tac_toe_decoder)
            print("Final Board:")
            print(ttt)
            #Close connection
            client_sock.close()
            return
            
        elif sttt2 == "DRAW":
            break
        else:
            ttt = json.loads(sttt2, object_hook=tic_tac_toe_decoder)
            
            
    #Drawn game
    print("Draw!")
    print("Final Board:")
    print(ttt)
    client_sock.close()
    return
        
    
def test_prob1():
    prob1()
    
#Disable main for pass off    
#if __name__ == "__main__":
#    #test_prob1()
#    #mirror_server()
#    #mirror_client()
    #tic_tac_toe_server(("0.0.0.0", 44459))
    #tic_tac_toe_client(("0.0.0.0", 44459))