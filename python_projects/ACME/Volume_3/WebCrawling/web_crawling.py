# -*- coding: utf-8 -*-
import re
import time
import requests
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import pickle

def scrape_books(start_page = "index.html"):
    """ Crawl through http://books.toscrape.com and extract fiction data"""
    base_url="http://books.toscrape.com/catalogue/category/books/fiction_10/"
    prices = []
    page = base_url + start_page                # Complete page URL.
    next_page_finder = re.compile(r"next")      # We need this button.
    
    current = None

    for i in range(1, 5):
        while current == None:                   # Try downloading until it works.
            # Download the page source and PAUSE before continuing.  
            page_source = requests.get(page).text
            time.sleep(1)           # PAUSE before continuing.
            soup = BeautifulSoup(page_source, "html.parser")
            #current = soup.find_all(class_="product_pod")
            current = soup.find_all(class_="product_price")
            
        # Navigate to the correct tag and extract price.
        for book in current:
            prices.append(float(book.p.text[2:]))
    
        # ind the URL for the page with the next data
        nn = "page-{}".format(i+1)
        if nn not in page:
            # Find the URL for the page with the next data.
            try:
                new_page = soup.find(string=next_page_finder).parent["href"]    
                page = base_url + new_page      # New complete page URL.
                current = None
            except AttributeError:
                break
            
    #Calculate mean price
    mp = np.mean(prices)
    return float(mp)

def bank_data():
    """Crawl through the Federal Reserve site and extract bank data."""
    # Compile regular expressions for finding certain tags.
    link_finder = re.compile(r"December 31, (?!2003)") #Negative look ahead; Don't need to worry about 2002 and 2001 beacuause they're not links
    chase_bank_finder = re.compile(r"^JPMORGAN CHASE BK")
    bank_amer_finder = re.compile(r"^BANK OF AMER")
    wells_far_finder = re.compile(r"^WELLS FARGO BK")
    finders = [chase_bank_finder, bank_amer_finder, wells_far_finder]

    # Get the base page and find the URLs to all other relevant pages.
    base_url="https://www.federalreserve.gov/releases/lbr/"
    base_page_source = requests.get(base_url).text
    base_soup = BeautifulSoup(base_page_source, "html.parser")
    link_tags = base_soup.find_all(name='a', href=True, string=link_finder)
    pages = [base_url + tag.attrs["href"] for tag in link_tags]

    # Crawl through the individual pages and record the data.
    chase_assets = []
    ba_assets = []
    wells_assets = []
    ast_lists = [chase_assets, ba_assets, wells_assets]
    for page in pages:
        time.sleep(1)               # PAUSE, then request the page.
        soup = BeautifulSoup(requests.get(page).text, "html.parser")

        
        # Find the tag corresponding to the banks' consolidated assets.
        for finder, ls in zip(finders, ast_lists):
            temp_tag = soup.find(name="td", string=finder)

            for _ in range(10):
                temp_tag = temp_tag.next_sibling
                
            # Extract the data, removing commas.
            ls.append(int(temp_tag.string.replace(',', '')))

    return ast_lists

def prob3():
    '''The Basketball Reference website at 
    https://www.basketball-reference.com} hosts data on NBA athletes, 
    including which player led different categories.
    For the past ten years, identify which player had the most season points.
    Return a list of triples, ("season year", "player name", points scored).
    '''
    
    #Get page soup
    base_url = "https://www.basketball-reference.com/leaders/pts_top_10.html"
    base_page_source = requests.get(base_url).text
    base_soup = BeautifulSoup(base_page_source, "html.parser")
    
    
    #Get table
    table = base_soup.find_all(name="table", id="leaders")[0]
    rows = list(table.children)
    
    output = []
    
    for i in range(10):
        row = rows[2*i + 3]
        season = str(row.td.string)
        temp = row.td
        for _ in range(4):
            temp = temp.next_sibling
        player_name = str(temp.a.string)
        points = int(temp.span.string[1:-1])
        
        output.append((season, player_name, points))
    
    return output


def prob4():
    """
    Sort the Top 10 movies of the week by Total Grossing, taken from 
    https://www.imdb.com/chart/boxoffice?ref_=nv_ch_cht.

    Returns:
        titles (list): Top 10 movies of the week sorted by total grossing
    """
    #Get page soup
    base_url = "https://www.imdb.com/chart/boxoffice"
    base_page_source = requests.get(base_url).text
    base_soup = BeautifulSoup(base_page_source, "html.parser")
    
    table = base_soup.find_all(name="table")[0].tbody
    
    rows = list(table.children)
    
    #Navigate page and scrape info
    titles = []
    grosses = []
    for i in range(10):
        row = rows[2*i+1]
        #Get title
        title = str(row.a.next_sibling.next_sibling.a.string)
        
        #Get money
        temp = row.a
        for _ in range(4):
            temp = temp.next_sibling
        money = float(str(temp.string).strip()[1:-1])
        
        titles.append(title)
        grosses.append(money)
    
    return titles


def prob5(search_query="linkedin"):
    """Use Selenium to enter the given search query into the search bar of
    https://arxiv.org and press Enter. The resulting page has up to 25 links
    to the PDFs of technical papers that match the query. Gather these URLs,
    then continue to the next page (if there are more results) and continue
    gathering links until obtaining at most 100 URLs. Return the list of URLs.

    Returns:
        (list): Up to 100 URLs that lead directly to PDFs on arXiv.
    """
    #Create webbrowser
    browser = webdriver.Chrome()
    try:
        #Open webpage
        browser.get("https://arxiv.org")
        try:
            #Get search bar and clear it            
            search_bar = browser.find_element_by_name("query")
            search_bar.clear()
            
            #Enter text and hit enter
            search_bar.send_keys(search_query)
            search_bar.send_keys(Keys.RETURN)
            
            
            #Get soup
            soup = BeautifulSoup(browser.page_source, "html.parser")
            
            results = list(soup.find(name="ol").children)
            
            urls = []
            for i in range(len(results)//2):
                #Get URL
                res = results[2*i+1]
                url = res.div.p.a.attrs["href"]
                urls.append(url)
            
            while len(urls) < 150:
                #Check if there are more results
                try:
                    nxt = browser.find_element_by_class_name("pagination-next")
                    nxt.click()
                    soup = BeautifulSoup(browser.page_source, "html.parser")
            
                    results = list(soup.find(name="ol").children)
                    
                    for i in range(len(results)//2):
                        #Get URL
                        res = results[2*i+1]
                        url = res.div.p.a.attrs["href"]
                        urls.append(url)
                    
                except:
                    break
                
                
            
        
            return urls
        except NoSuchElementException:
            print("Could not find the search bar!")
            raise
    finally:
        browser.close()
    

def test_prob1():
    print(scrape_books())
    
def test_prob2():
    print(bank_data())
    
def test_prob3():
    season_years, player_names, points_scored = prob3()
    
def test_prob5():
    urls = prob5("machine learning")
    

def self_check():

    # Problem 1
    with open('ans1', 'rb') as fp:
        res = pickle.load(fp)
    assert type(res) == float

    # Problem2
    """
        extract the assets for 
        JPMORGAN CHASE BK NA
        BANK OF AMER NA
        WELLS FARGO BK NA
    """
    with open('ans2', 'rb') as fp:
        res = pickle.load(fp)
    assert type(res) == list
    assert len(res) == 3
    assert type(res[0]) == list
    assert len(res[0]) == 15
    assert type(res[0][0]) == int

    # Problem 3
    """ 
        make sure your first tuple in the list 
        aka res[0] corresponds to the season that ended in 2019, 
        I won't be checking how you stored the year I'll just be 
        expecting them to be in order with the last entry being 
        the season that ended in 2010 
    """
    with open('ans3', 'rb') as fp:
        res = pickle.load(fp)
    assert type(res) == list
    assert len(res) == 10
    assert type(res[0]) == tuple
    assert len(res[0]) == 3
    assert type(res[0][1]) == str    #player's name
    assert type(res[0][2]) == int    #points scored


    #Problem 4
    """ 
        the first movie in the list should be the movie with 
        the most money or that grossed the most money, 
        not sure how to say it 
    """
    with open('ans4', 'rb') as fp:
        res = pickle.load(fp)
    assert type(res) == list
    assert len(res) == 10
    assert (type(res[0]) == str or type(res[0]) == np.str_)


    #Problem 5
    """ 
        for this problem store the list that you get when 
        the default 'linkedin' search query is passed in. the first 
        element in your list should be 'https://arxiv.org/pdf/1907.12549' 
    """
    with open('ans5', 'rb') as fp:
        res = pickle.load(fp)
    assert type(res) == list
    assert len(res) > 100
    assert len(res) < 150
    assert type(res[0]) == str

    return True    
    
if __name__ == "__main__":
    #test_prob1()
    #test_prob2()
    #test_prob3()
    #test_prob5()
    print(self_check())