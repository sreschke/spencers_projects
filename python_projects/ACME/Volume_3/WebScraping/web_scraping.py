"""Volume 3: Web Scraping.
Spencer Reschke
Math 437
9/17/2019
"""
import re
import requests
from bs4 import BeautifulSoup
from matplotlib import pyplot as plt
import os.path
from datetime import datetime
import numpy as np

# Problem 1
def prob1():
    """Use the requests library to get the HTML source for the website htttp://www.example.com.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    file_name = "example.html"
    if not os.path.exists(file_name):
        #Scrape the website
        response = requests.get("http://www.example.com")
        if response.ok:
            with open(file_name, "x") as f:
                f.write(response.text)
        else:
            #Get request failed
            pass
    
# Problem 2
def prob2():
    """Examine the source code of http://www.example.com. Determine the names
    of the tags in the code and the value of the 'type' attribute associated
    with the 'style' tag.

    Returns:
        (set): A set of strings, each of which is the name of a tag.
        (str): The value of the 'type' attribute in the 'style' tag.
    """
    file_name = "example.html"
    if os.path.exists(file_name):
        tags = set([])
        with open(file_name, "r") as f:
            file_contents = f.read()
            #Find tags and special tags
            tag = re.compile(r"</[^>]*>|<[^>]*/>")
            matches = tag.findall(file_contents)
            
            #Get name of tag
            for match in matches:
                pat = re.compile(r"\w+")
                tag_to_add = pat.findall(match)[0]
                tags.add(tag_to_add)
                
            #Get type of style tag
            pat = re.compile(r"<style[^>]*>")
            match = pat.findall(file_contents)
            pat = re.compile(r"type=[^>]*")
            tpe = pat.findall(match[0])[0][5:]
            
        return tags, tpe


# Problem 3
def prob3(code):
    """Return a list of the names of the tags in the given HTML code."""
    """Return a list of the names of the tags in the given HTML code."""
    #Create soup
    soup = BeautifulSoup(code, "html.parser")
    
    #Find all tag names and return
    return [tag.name for tag in soup.find_all(True)]


# Problem 4
def prob4(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Find the only
    <a> tag with a hyperlink and return its text.
    """
    #Open file
    with open(filename, "r") as f:
        code = f.read()
        soup = BeautifulSoup(code, "html.parser")
        #Find all a tags
        As = soup.find_all(name="a") 
        
        #Find tag with hyperlink
        for tag in As:
            if hasattr(tag, "attrs") and "href" in tag.attrs:
                string = tag.string
    return string


# Problem 5
def prob5(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    #Open file
    with open(filename, "r") as f:
        #Get file contents as string
        code = f.read()
        
        #Make soup
        soup = BeautifulSoup(code, "html.parser")
        
        #Find first tag
        opts = soup.find_all(class_="history-date")
        for tag in opts:
            if tag.string == "Thursday, January 1, 2015":
                first = tag
                break
                
        #Find second tag
        opts = soup.find_all(name="a")
        for tag in opts:
            if "Previous Day" in str(tag.string):
                second = tag
                break
                
        #Find third tag
        for tag in opts:
            if "Next Day" in str(tag.string):
                third = tag
                break
                
        #Find fourth tag
        opts = soup.find_all(class_ ="wx-value")
        for tag in opts:
            if "59" == tag.string:
                fourth = tag
                break
                
    return [first, second, third, fourth]


# Problem 6
def prob6(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    #open file
    with open(filename, "r") as f:
        #Get contents of file as string
        code = f.read()
        
        #Make soup
        soup = BeautifulSoup(code, "html.parser")
        
        #Get all tags with years 2000=2014
        ops = [tag.parent for tag in soup.find_all(string=re.compile("20[01][0-9]$"))][:-1] #Remove last string
        
        #Filter tags
        date1 = datetime.strptime("September 30, 2003", "%B %d, %Y")
        date2 = datetime.strptime("December 31, 2014", "%B %d, %Y")
        tags = [tag for tag in ops if date1 <= datetime.strptime(tag.string, "%B %d, %Y") <= date2]
               
        
    return tags


# Problem 7
def prob7(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    #Open file
    with open(filename, "r") as f:
        #Get contents of file as string
        code = f.read()
        
        #Make soup
        soup = BeautifulSoup(code, "html.parser")
        
        #Get data
        tbody = soup.find_all(name="table")[2].tbody
        names = []
        num_doms = []
        num_fors = []
        
        cur_row = tbody.tr
        for _ in range(1372):
            cur_col = cur_row.td
            name = cur_col.string
            names.append(name)
            
            #Iterate till Domestic Branches col
            i=0
            while i!=9:
                cur_col = cur_col.next_sibling.next_sibling
                i+=1
                
            #Get number of domestic branches
            try:
                n_dom = int(cur_col.string.replace(",", ""))
            except ValueError:
                n_dom = 0
            num_doms.append(n_dom)
            
            #Get num foreing branches
            try:
                n_for = int(cur_col.next_sibling.next_sibling.string)
            except ValueError:
                n_for = 0
            num_fors.append(n_for)
            cur_row = cur_row.next_sibling.next_sibling
    
    names = np.array(names)
    num_doms = np.array(num_doms)
    num_fors = np.array(num_fors)
    #Create bar charts
    plt.subplot(2, 1, 1)
    #Get top domestic branches
    args = np.argsort(num_doms)[::-1][:7]
    top_names = names[args]
    top_doms = num_doms[args]
    y_pos = np.arange(len(top_names))
    plt.barh(y_pos, top_doms, align="center")
    plt.yticks(y_pos, top_names)
    plt.title("Domestic Branches")
    plt.xlabel("count")
    
    #Get top forein branches
    plt.subplot(2, 1, 2)
    #Get top domestic branches
    args = np.argsort(num_fors)[::-1][:7]
    top_names = names[args]
    top_fors = num_fors[args]
    y_pos = np.arange(len(top_names))
    
    #Make plots
    plt.barh(y_pos, top_fors, align="center")
    plt.yticks(y_pos, top_names)
    plt.title("Foreign Branches")
    plt.xlabel("count")
    plt.tight_layout()
    plt.show()
    
def test_prob1():
    prob1()
    
def test_prob2():
    tags, tpe = prob2()
    print(tags)
    print(tpe)
    
def test_prob3():
    with open("example.html", "r") as f:
        code = f.read()
        names = prob3(code)
        print(names)
        
def test_prob4():
    print(prob4())
    
def test_prob5():
    print(prob5())
    
def test_prob6():
    print(prob6())
    
def test_prob7():
    prob7()
    
if __name__ == "__main__":
    #test_prob1()
    test_prob2()
    #test_prob3()
    #test_prob4()
    #test_prob5()
    #test_prob6()
    #test_prob7()
