09/20/19 14:51

Problem 1 (3 points):
Score += 3

Problem 2 (5 points):
Should return a string with the type attribute of the style tag
Score += 3

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
prob4() failed
	Correct response: 'More information...'
	Student response: 'http://www.iana.org/domains/example'
Score += 0

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
missing data, should have 46 elements in list, you have 26
Score += 7

Problem 7 (12 points):
labels aren't visible, maybe try barh plot
Score += 10

Code Quality (5 points):
Score += 5

Total score: 38/50 = 76.0%

-------------------------------------------------------------------------------

09/24/19 16:08

Problem 1 (3 points):
Score += 3

Problem 2 (5 points):
Should return a string with the type attribute of the style tag
Score += 3

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
Score += 10

Problem 7 (12 points):
Score += 12

Code Quality (5 points):
Score += 5

Total score: 48/50 = 96.0%

Great job!

-------------------------------------------------------------------------------

