# solutions.py
"""Volume 2: Polynomial Interpolation.
Spencer Reschke
Math 322
1/16/2019
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import BarycentricInterpolator

# Problem 1
def lagrange(xint, yint, points):
    """Find an interpolating polynomial of lowest degree through the points
    (xint, yint) using the Lagrange method and evaluate that polynomial at
    the specified points.

    Parameters:
        xint ((n,) ndarray): x values to be interpolated.
        yint ((n,) ndarray): y values to be interpolated.
        points((m,) ndarray): x values at which to evaluate the polynomial.

    Returns:
        ((m,) ndarray): The value of the polynomial at the specified points.
    """
    n = len(xint)

    #Steps 1 and 2
    sum = np.zeros_like(points)
    for j in range(n):
        prod = np.ones(len(points))
        for k in range(n):
            if k != j:
                prod *= (points-xint[k])/(xint[j]-xint[k])
        sum += yint[j]*prod

    return sum

# Problems 2 and 3
class Barycentric:
    """Class for performing Barycentric Lagrange interpolation.

    Attributes:
        w ((n,) ndarray): Array of Barycentric weights.
        n (int): Number of interpolation points.
        x ((n,) ndarray): x values of interpolating points.
        y ((n,) ndarray): y values of interpolating points.
    """

    def __init__(self, xint, yint):
        """Calculate the Barycentric weights using initial interpolating points.

        Parameters:
            xint ((n,) ndarray): x values of interpolating points.
            yint ((n,) ndarray): y values of interpolating points.
        """

        #calculate weights
        n = len(xint)
        w = np.ones(n)

        #Calculate capacity of interval
        C = (np.max(xint) - np.min(xint))/4
        self._C = C

        shuffle = np.random.permutation(n-1)
        for j in range(n):
            temp = (xint[j] - np.delete(xint, j))/C
            temp = temp[shuffle]
            w[j] /= np.product(temp)

        #store class attributes
        self._xint = xint
        self._yint = yint
        self._weights = w
        self._map = dict(zip(xint, yint))



    def __call__(self, points):
        """Using the calcuated Barycentric weights, evaluate the interpolating polynomial
        at points.

        Parameters:
            points ((m,) ndarray): Array of points at which to evaluate the polynomial.

        Returns:
            ((m,) ndarray): Array of values where the polynomial has been computed.
        """

        #an array to store the output
        out = np.zeros_like(points)

        #get mask for points that match interpolating values
        mask = np.array([x in self._map for x in points])

        #Get outputs values at interpolation points
        out[mask] = np.array([self._map[x] for x in points[mask]])

        #Calcute outputs using Barycentric Interpolation
        n = len(self._weights)
        num = np.sum(np.array([wj*yj/(points[~mask]-xj) for xj, yj, wj in zip(self._xint, self._yint, self._weights)]), axis=0)
        denom = np.sum(np.array([wj/(points[~mask]-xj) for xj, wj in zip(self._xint, self._weights)]), axis=0)

        out[~mask] = num/denom
        
        return out


    # Problem 3
    def add_weights(self, xint, yint):
        """Update the existing Barycentric weights using newly given interpolating points
        and create new weights equal to the number of new points.

        Parameters:
            xint ((m,) ndarray): x values of new interpolating points.
            yint ((m,) ndarray): y values of new interpolating points.
        """
        #Code in book didn't take into account affect of new points on C
        old_n = len(self._xint)
        self._weights /= self._C**(old_n-1)

        #Update weights
        for x, y in zip(xint, yint):
            nw = 1/np.product(x-self._xint)
            self._weights /= (self._xint-x)
            self._weights = np.append(self._weights, nw)
            self._xint = np.append(self._xint, x)
            self._yint = np.append(self._yint, y)

        self._C = (np.max(self._xint) - np.min(self._xint))/4
        new_n = len(self._xint)
        self._weights *= self._C**(new_n -1)
        self._map = dict(zip(self._xint, self._yint))




# Problem 4
def prob4():
    """For n = 2^2, 2^3, ..., 2^8, calculate the error of intepolating Runge's
    function on [-1,1] with n points using SciPy's BarycentricInterpolator
    class, once with equally spaced points and once with the Chebyshev
    extremal points. Plot the absolute error of the interpolation with each
    method on a log-log plot.
    """
    f = lambda x: 1/(1+25*x**2)

    uni_error = []
    cheb_error = []

    ns = 2**np.arange(2, 9)
    for n in ns:
        domain = np.linspace(-1, 1, 200)

        #create equally spaced points
        u_pts = np.linspace(-1, 1, n)        

        #Construct interpolating function
        poly1 = BarycentricInterpolator(u_pts)
        poly1.set_yi(f(u_pts))

        #Get error for uniformly spaced points
        u_error = np.linalg.norm(f(domain)-poly1(domain), ord=np.inf)
        uni_error.append(u_error)

        #create Chebyshev points
        c_pts = np.array([np.cos(j*np.pi/n) for j in range(n+1)])

        #Construct interpolating function
        poly2 = BarycentricInterpolator(c_pts)
        poly2.set_yi(f(c_pts))

        #Get error for chebyshev points
        c_error = np.linalg.norm(f(domain)-poly2(domain), ord=np.inf)
        cheb_error.append(c_error)

    #make plots
    plt.plot(ns, np.array(uni_error), label="uniform")
    plt.plot(ns, np.array(cheb_error), label="Chebyshev")
    plt.xscale("log", basex=2)
    plt.yscale("log", basey=2)
    plt.title("Interpolation error")
    plt.ylabel("Error")
    plt.xlabel("number of interpolation points")
    plt.legend()
    plt.show()

# Problem 5
def chebyshev_coeffs(f, n):
    """Obtain the Chebyshev coefficients of a polynomial that interpolates
    the function f at n points.

    Parameters:
        f (function): Function to be interpolated.
        n (int): Number of points at which to interpolate.

    Returns:
        coeffs ((n+1,) ndarray): Chebyshev coefficients for the interpolating polynomial.
    """
    #get Chebyshev extremizers

    ces = np.array([np.cos(j*np.pi/n) for j in range(n+1)])
    
    #construct 2n-dimensional input vector
    iv = np.append(f(ces), f(ces)[1:-1][::-1])

    #construct gamma vector
    g = 2*np.ones(2*n)
    g[[0, n]] = 1    

    #take discrete fourier transform and get coefficients
    fft = np.real(np.fft.fft(iv)/(2*n))
    aks = (g*fft)[:n+1]

    assert len(aks) == n+1
    return aks

# Problem 6
def prob6(n):
    """Interpolate the air quality data found in airdata.npy using
    Barycentric Lagrange interpolation. Plot the original data and the
    interpolating polynomial.

    Parameters:
        n (int): Number of interpolating points to use.
    """
    #Load data
    data = np.load("airdata.npy")

    #Get closest matches to Chebyshev extremizers
    fx = lambda a, b, n: .5*(a+b + (b-a) * np.cos(np.arange(n+1) * np.pi/n))
    a, b, = 0, 366 - 1/24
    domain = np.linspace(0, b, 8784)
    points = fx(a, b, n)
    temp = np.abs(points - domain.reshape(8784, 1))
    temp2 = np.argmin(temp, axis=0)

    #Perform interpolation
    poly = Barycentric(domain[temp2], data[temp2])

    #Make subplots
    plt.subplot(1, 2, 1)
    plt.scatter(domain[temp2], data[temp2], c='b', label="Data Points")
    plt.ylabel("PM2.5 concentration")
    plt.xlabel("Time")
    plt.xticks([], [])
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.plot(domain[temp2], poly(domain[temp2]), 'r', label="Interpolation")
    plt.xlabel("Time")
    plt.xticks([], [])

    plt.suptitle("Air data interpolation")
    plt.legend()
    plt.show()


def test_lagrange():
    def f(x):
        return 1/(1+25*x**2)

    num_points = 11
    xint = np.linspace(-1, 1, num_points)
    yint = f(xint)

    points = np.linspace(-1, 1, 100)
    outs = lagrange(xint, yint, points)

    plt.plot(points, f(points))
    plt.plot(points, outs)
    plt.show()


def test_Barycentric():
    def f(x):
        return 1/(1+25*x**2)

    num_points = 11
    xint = np.linspace(-1, 1, num_points)
    yint = f(xint)

    points = np.linspace(-1, 1, 100)

    #test constructor
    B = Barycentric(xint, yint)

    #test call
    outs = B(points)

    #test plots
    plt.plot(points, f(points))
    plt.plot(points, outs)
    plt.show()


    #test add_weights
    xnps = np.array([-np.pi/6, np.pi/6])
    ynps = f(xnps)
    points = np.append(points, xnps)

    B.add_weights(xnps, ynps)

    #test plots
    points.sort()
    plt.plot(points, f(points))
    plt.plot(points, B(points))
    plt.show()


def test_prob4():
    prob4()

 
def test_prob5():
    f = lambda x: -3+2*x**2-x**3+x**4 #degree 4 polynomial
    pcoeffs = [-3, 0, 2, -1, 1]
    ccoeffs = np.polynomial.chebyshev.poly2cheb(pcoeffs)

    cs = chebyshev_coeffs(f, 4)
    
    assert np.allclose(cs, ccoeffs)

    print("All tests passed for problem 5")

def test_prob6():
    prob6(200)


if __name__ == "__main__":
    test_lagrange()
    test_Barycentric()

    test_prob4()
    test_prob5()
    test_prob6()