# linked_lists.py
"""Volume 2: Linked Lists.
<Name>
<Class>
<Date>
"""


# Problem 1
class Node:
    """A basic node class for storing data."""
    def __init__(self, data):
        """Store the data in the value attribute. The data must be a string, integer
        or float."""
        if type(data) not in [str, int, float]:
            raise TypeError("data must be a string, integer or float")
        self.value = data


class LinkedListNode(Node):
    """A node class for doubly linked lists. Inherits from the Node class.
    Contains references to the next and previous nodes in the linked list.
    """
    def __init__(self, data):
        """Store the data in the value attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        self.next = None                # Reference to the next node.
        self.prev = None                # Reference to the previous node.


# Problems 2-5
class LinkedList:
    """Doubly linked list data structure class.

    Attributes:
        head (LinkedListNode): the first node in the list.
        tail (LinkedListNode): the last node in the list.
    """
    def __init__(self):
        """Initialize the head and tail attributes by setting
        them to None, since the list is empty initially.
        Initialize the length of the list to zero.
        """
        self.head = None
        self.tail = None
        self.length = 0

    def append(self, data):
        """Append a new node containing the data to the end of the list."""
        # Create a new node to store the input data.
        new_node = LinkedListNode(data)
        self.length += 1
        if self.head is None:
            # If the list is empty, assign the head and tail attributes to
            # new_node, since it becomes the first and last node in the list.
            self.head = new_node
            self.tail = new_node
        else:
            # If the list is not empty, place new_node after the tail.
            self.tail.next = new_node               # tail --> new_node
            new_node.prev = self.tail               # tail <-- new_node
            # Now the last node in the list is new_node, so reassign the tail.
            self.tail = new_node

    # Problem 2
    def find(self, data):
        """Return the first node in the list containing the data.

        Raises:
            ValueError: if the list does not contain the data.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.find('b')
            >>> node.value
            'b'
            >>> l.find('f')
            ValueError: <message>
        """
        if self.head is None:
            raise ValueError("The linked list is empty")
        if (self.head.value == data):
            return self.head
        current_node = self.head
        while current_node.next is not None:
            current_node = current_node.next
            if (current_node.value == data):
                return current_node
        #if we get here, the data was not in the linked list
        raise ValueError("The data {} was not found".format(data))

    # Problem 2
    def get(self, i):
        """Return the i-th node in the list (assume indexing starts at zero).

        Raises:
            IndexError: if i is negative or greater than or equal to the
                current number of nodes.

        Examples:
            >>> l = LinkedList()
            >>> for x in ['a', 'b', 'c', 'd', 'e']:
            ...     l.append(x)
            ...
            >>> node = l.get(3)
            >>> node.value
            'd'
            >>> l.get(5)
            IndexError: <message>
        """
        #if the linked list is empty
        if (self.length == 0):
            raise IndexError("The linked list is empty")
        if (i < 0) or (i > (self.length-1)):
            raise IndexError("Requested index is out of bounds")
        #if they want the head, return it
        if (i == 0):
            return self.head
        #iterate through linked list and find the ith node
        current_node = self.head
        count = 0
        while(current_node.next is not None):
            current_node = current_node.next
            count+=1
            if count == i:
                return current_node
        #should never get here
        assert True == False, "Bug in get() function"

    # Problem 3
    def __len__(self):
        """Return the number of nodes in the list.

        Examples:
            >>> l = LinkedList()
            >>> for i in (1, 3, 5):
            ...     l.append(i)
            ...
            >>> len(l)
            3
            >>> l.append(7)
            >>> len(l)
            4
        """
        #return the length of the linked list
        return self.length

    # Problem 3
    def __str__(self):
        """String representation: the same as a standard Python list.

        Examples:
            >>> l1 = LinkedList()       |   >>> l2 = LinkedList()
            >>> for i in [1,3,5]:       |   >>> for i in ['a','b',"c"]:
            ...     l1.append(i)        |   ...     l2.append(i)
            ...                         |   ...
            >>> print(l1)               |   >>> print(l2)
            [1, 3, 5]                   |   ['a', 'b', 'c']
        """
        #if the linked list is empty
        if self.length == 0:
            return "[]"
        current_node = self.head
        output = "[" + repr(current_node.value)
        #iterate through linked list and build output string
        while (current_node.next is not None):
            current_node = current_node.next
            output += ", " + repr(current_node.value)
        output += "]"
        return output

    # Problem 4
    def remove(self, data):
        """Remove the first node in the list containing the data.

        Raises:
            ValueError: if the list is empty or does not contain the data.

        Examples:
            >>> print(l1)               |   >>> print(l2)
            ['a', 'e', 'i', 'o', 'u']   |   [2, 4, 6, 8]
            >>> l1.remove('i')          |   >>> l2.remove(10)
            >>> l1.remove('a')          |   ValueError: <message>
            >>> l1.remove('u')          |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.remove(10)
            ['e', 'o']                  |   ValueError: <message>
        """
        if (self.length == 0):
            raise ValueError("The linked list is empty")
        node_to_remove = self.find(data) #raises a value error if data is not found

        #if the linked list has a single node
        if (self.length == 1):
            self.head = None
            self.tail = None
            self.length = 0
            return

        #We know the linked list has at least two nodes at this point
        #if node_to_remove is the head
        if node_to_remove is self.head:           
            self.head = node_to_remove.next
            self.head.prev = None
            self.length-=1
            return

        #if node_to_remove is the tail
        if node_to_remove is self.tail:
            self.tail = node_to_remove.prev
            self.tail.next = None
            node_to_remove.prev = None
            self.length-=1
            return

        #If node_to_remove is not the head nor tail
        temp = node_to_remove.next
        node_to_remove.next = None
        node_to_remove.prev.next = temp
        temp.prev = node_to_remove.prev
        node_to_remove.prev = None
        self.length-=1
        return
        
        raise NotImplementedError("Problem 4 Incomplete")

    #test function 
    def print_traversals(self):
        """prints the values of the tree from front to back and vice versa.
           Used to test insertion and reversal functions"""
        #this is a comment
        if (self.length == 0):
            print("[]")
            return
        #print forward traversal
        print("Forward traversal")
        print(str(self))
        print("Backward traversal")
        
        current_node = self.tail
        output = "[" + repr(current_node.value)
        while (current_node.prev is not None):
            current_node = current_node.prev
            output += ", " + repr(current_node.value)

        output += "]"
        print(output)
        return

    # Problem 5
    def insert(self, index, data):
        """Insert a node containing data into the list immediately before the
        node at the index-th location.

        Raises:
            IndexError: if index is negative or strictly greater than the
                current number of nodes.

        Examples:
            >>> print(l1)               |   >>> len(l2)
            ['b']                       |   5
            >>> l1.insert(0, 'a')       |   >>> l2.insert(6, 'z')
            >>> print(l1)               |   IndexError: <message>
            ['a', 'b']                  |
            >>> l1.insert(2, 'd')       |   >>> l3 = LinkedList()
            >>> print(l1)               |   >>> l3.insert(1, 'a')
            ['a', 'b', 'd']             |   IndexError: <message>
            >>> l1.insert(2, 'c')       |
            >>> print(l1)               |
            ['a', 'b', 'c', 'd']        |
        """
        if (index < 0) or (index > self.length):
            raise IndexError("Requested index is out of bounds")
        #create new node to be added
        new_node = LinkedListNode(data)

        #if the linked list is empty, use append method
        if (self.length == 0):
            self.append(data)
            return
        
        #if we're adding before the head
        if (index == 0):
            new_node.next = self.head
            self.head.prev = new_node
            self.head = new_node
            self.length += 1
            return
     
        #if we're adding after the tail
        if (index == self.length):
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = new_node
            self.length += 1
            return

        #We're adding between two nodes
        node_at_index = self.get(index)
        new_node.next = node_at_index
        node_at_index.prev.next = new_node
        new_node.prev = node_at_index.prev
        node_at_index.prev = new_node
        self.length+=1
        return


# Problem 6: Deque class.

class Deque(LinkedList):
    def __init__(self):
        """constructor for deque class"""
        #use contructor for LinkedList
        LinkedList.__init__(self)

    def pop(self):
        """removes the last node in the list and returns the data

        Raises:
             ValueError is the deque is empty"""
        #if the deque is empty
        if (self.length == 0):
            raise ValueError("The deque is empty.")

        #if the deque has a single node
        if (self.length == 1):
            node_to_remove = self.tail
            self.head = None
            self.tail = None
            node_to_remove.prev = node_to_remove.next = None
            self.length -= 1
            return node_to_remove.value
        
        assert self.length > 1, "length is negative"
        #pop from a deque that has at least two nodes
        node_to_remove = self.tail
        self.tail = node_to_remove.prev
        self.tail.next = None
        node_to_remove.prev = None
        return node_to_remove.value
        

    def popleft(self):
        """removes node at beginning of deque

        Raises:
             ValueError is the deque is empty"""

        #if the deque is empty
        if (self.length == 0):
            raise ValueError("The deque is empty.")
        
        #if the deque has a single node
        if (self.length == 1):
            node_to_remove = self.tail
            self.head = None
            self.tail = None
            node_to_remove.prev = node_to_remove.next = None
            self.length -= 1
            return node_to_remove.value

        assert self.length > 1, "length is negative"
        #pop from a deque that has at least two nodes
        node_to_remove = self.head
        self.head = node_to_remove.next
        node_to_remove.next = None
        self.head.prev = None
        self.length -= 1
        return node_to_remove.value

    def appendleft(self, data):
        """Appends data to the left side of the deque. The data to be added needs
        to be of type str, int, or float."""
        
        #we can use the insert method from LinkedList
        self.insert(0, data)
        return

    #override remove
    def remove(*args, **kwargs):
        raise NotImplementedError("Use pop() or popleft() for removal")

    #override insert
    def insert(*args, **kwargs):
        raise NotImplementedError("Use append() or appendleft() for insertion")       
        
        


# Problem 7
def prob7(infile, outfile):
    """Reverse the contents of a file, infile, by line and write the results to
    another file, outfile.

    Parameters:
        infile (str): the file to read from.
        outfile (str): the file to write to.
    """
    #open infile
    fp = open(infile, 'r')
    line = fp.readline()
    stack = []
    while line:
        stack.append(line)
        line = fp.readline()
    fp.close()

    #open outfile for writing
    op = open(outfile, 'w')
    while len(stack) > 0:
        op.write(stack[-1])
        del stack[-1] #"pop" from stack
    op.close()
              

def test_linked_list():
    """a function used to test the functionality of the linked list class"""
    #here is a comment
    a_list = list(range(10))
    #create instance
    ll = LinkedList()
    assert ll.length == 0, "linked list test failed"

    #test append
    count = 1
    for x in a_list:
        ll.append(x)
        assert ll.length == count, "check append function"
        count += 1

    print("Current list:")
    print(str(ll))
    
    print("Remove head:")
    ll.remove(a_list[0])
    ll.print_traversals()

    print("Remove tail:")
    ll.remove(a_list[-1])
    ll.print_traversals()

    print("Insert at head:")
    ll.insert(0, "a")
    ll.print_traversals()

    print("Insert at tail:")
    ll.insert(ll.length, "b")
    ll.print_traversals()

    print("Insert in middle:")
    ll.insert(ll.length//2, "c")
    ll.print_traversals()
    print(len(ll))

    print("remove everything from head")
    for i in range(len(ll)):
        val = ll.head.value
        ll.remove(val)
    print(str(ll))
    print(len(ll))

    #test append
    count = 1
    for x in a_list:
        ll.append(x)
        assert ll.length == count, "check append function"
        count += 1
    print()
    print("Current list:")
    print(str(ll)) 

    print("remove everything from tail")
    for i in range(len(ll)):
        val = ll.tail.value
        ll.remove(val)
    print(str(ll))
    print(len(ll))

    print("Current list:")
    print(str(ll)) 
    print("Add to empty list")
    ll.insert(0, "a")
    print(str(ll))
    print(len(ll))


if __name__ == "__main__":
    #test_linked_list()
    prob7("english.txt", "reversed_english.txt")



