# standard_library.py
"""Python Essentials: The Standard Library.
Spencer Reschke
<Class>
7/21/2018
"""
import calculator as calc
from math import sqrt
from itertools import combinations
import sys
import random
import time
import box

# Problem 1
def prob1(L):
    """Return the minimum, maximum, and average of the entries of the list L
    (in that order).
    """
    return min(L), max(L), sum(L)/len(L)
    raise NotImplementedError("Problem 1 Incomplete")


# Problem 2
def prob2():
    """Determine which Python objects are mutable and which are immutable.
    Test numbers, strings, lists, tuples, and sets. Print your results.
    """
    mutable={"int": False, "str": False, "list": False, "tuple": False, "set": False}

    my_int=1
    my_int2=my_int
    my_int+=1
    mutable["int"]=my_int==my_int2
    
    my_string="some_string"
    my_string2=my_string
    my_string+="another_string"
    mutable["str"]=my_string==my_string2

    my_list=[1, 2, 3, 4]
    my_list2=my_list
    my_list+=[4, 3, 2, 1]
    mutable["list"]=my_list==my_list2
    
    my_tuple=(1, 2, 3, 4)
    my_tuple2=my_tuple
    my_tuple+=(1,)
    mutable["tuple"]=my_tuple==my_tuple2

    my_set={1, 2, 3, 4}
    my_set2=my_set
    my_set.pop()
    mutable["set"]=my_set==my_set2
    
    print("Conclusions:")
    for object, type in mutable.items():
        print("\t" + object + " objects are ", end="")
        if type:
            print("mutable.")
        else:
            print("immutable.")
    return
    raise NotImplementedError("Problem 2 Incomplete")


# Problem 3
def hypot(a, b):
    """Calculate and return the length of the hypotenuse of a right triangle.
    Do not use any functions other than those that are imported from your
    'calculator' module.

    Parameters:
        a: the length one of the sides of the triangle.
        b: the length the other non-hypotenuse side of the triangle.
    Returns:
        The length of the triangle's hypotenuse.
    """
    return math.sqrt(calc.add_two(calc.prod_two(a, a), calc.prod_two(b, b)))
    raise NotImplementedError("Problem 3 Incomplete")


# Problem 4
def power_set(A):
    """Use itertools to compute the power set of A.

    Parameters:
        A (iterable): a str, list, set, tuple, or other iterable collection.

    Returns:
        (list(sets)): The power set of A as a list of sets.
    """
    if A == set():
        assert set() in power_set, "empty set not in power_set"
        return [set()]
    power_set=[set(), set(A)]
    for i in range(1, len(A)):
        power_set+=list(map(set, list(combinations(A, i))))

    assert set() in power_set, "empty set not in power_set"
    return power_set
    raise NotImplementedError("Problem 4 Incomplete")


# Problem 5: Implement shut the box.
def roll(remaining):
    """simulates the rolling of two six sided dice. Returns the sum of the dice"""
    Sum = sum(remaining)
    if Sum > 6:
        return random.randint(1, 6) + random.randint(1, 6)
    elif Sum <= 6:
        return random.randint(1, 6)
    else:
        assert True==False, "Error in roll(): remaining is invalid: {}".format(remaining)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(prob1([1, 2, 3, 4]))
        prob2()
        print(hypot(3, 4))
        print(power_set([1, 2, 3, 4]))

    elif len(sys.argv) == 3:
        player_name=sys.argv[1]
        tot_time=int(sys.argv[2])
        remaining = list(range(1, 10))

        print("Numbers left: {}".format(remaining))
        dice_roll=roll(remaining)
        print("Roll: {}".format(dice_roll))
        print("Seconds left: {:.2f}".format(round(tot_time)))
        start_time=time.time()
        final_time=start_time+tot_time
        nums_to_remove=[]
        while True:
            user_input=input("Numbers to eliminate: ")
            current_time=time.time()
            nums_to_remove=box.parse_input(user_input, remaining)        
            if final_time - current_time < 0: #time has run out
                break
            if set(nums_to_remove).issubset(set(remaining)) and sum(nums_to_remove) == dice_roll:
                remaining = sorted(set(remaining) - set(nums_to_remove))
                if len(remaining) == 0:
                    break #they've won
                print()
                print("Numbers left: {}".format(remaining))
                dice_roll=roll(remaining)
                print("Roll: {}".format(dice_roll))
                if not box.isvalid(dice_roll, remaining):
                    break
                print("Seconds left: {:.2f}".format(final_time-current_time))
            else:
                print("Invalild input")
                print()
                print("Seconds left: {:.2f}".format(final_time-current_time))

        if len(remaining) > 0:
            print("Game Over!".format(player_name))
        print()
        print("Score for player {}: {} points".format(player_name, sum(remaining)))
        print("Time played: {:.2f} seconds".format(current_time - start_time))
        if len(remaining)==0:
            print("Congratulations!! You shut the box!")
        else:
            print("Better luck next time")
        
    