# breadth_first_search.py
"""Volume 2: Breadth-First Search.
Spencer Reschke
Math 321
10/27/2018
"""

from collections import deque
import networkx as nx
from matplotlib import pyplot as plt
import numpy as np



# Problems 1-3
class Graph:
    """A graph object, stored as an adjacency dictionary. Each node in the
    graph is a key in the dictionary. The value of each key is a set of
    the corresponding node's neighbors.

    Attributes:
        d (dict): the adjacency dictionary of the graph.
    """
    def __init__(self, adjacency={}):
        """Store the adjacency dictionary as a class attribute"""
        self.d = dict(adjacency)

    def __str__(self):
        """String representation: a view of the adjacency dictionary."""
        return str(self.d)

    # Problem 1
    def add_node(self, n):
        """Add n to the graph (with no initial edges) if it is not already
        present.

        Parameters:
            n: the label for the new node.
        """
        if n not in self.d:
            self.d[n] = set()
        return

    # Problem 1
    def add_edge(self, u, v):
        """Add an edge between node u and node v. Also add u and v to the graph
        if they are not already present.

        Parameters:
            u: a node label.
            v: a node label.
        """
        #if u or v are not in d, add them
        if u not in self.d:
            self.add_node(u)
        if v not in self.d:
            self.add_node(v)

        #add edge from u to v
        self.d[u].add(v)

        #FIXME are we implementing directed or undirected graphs?
        #add edge from v to u
        #self.d[v].add(u)
        return

    # Problem 1
    def remove_node(self, n):
        """Remove n from the graph, including all edges adjacent to it.

        Parameters:
            n: the label for the node to remove.

        Raises:
            KeyError: if n is not in the graph.
        """
        #raise a value error if we try to remove a node not in the graph
        if n not in self.d:
            raise KeyError("{} is not in the graph".format(n))

        #remove edges to n
        for key in self.d.keys():
            if n in self.d[key]:
                self.d[key].remove(n)

        #remove n from d.keys()
        self.d.pop(n)
        return

    # Problem 1
    def remove_edge(self, u, v):
        """Remove the edge between nodes u and v.

        Parameters:
            u: a node label.
            v: a node label.

        Raises:
            KeyError: if u or v are not in the graph, or if there is no
                edge between u and v.
        """
        #ensure u and v are in the graph
        if u not in self.d:
            raise KeyError("{} is not in the graph".format(u))
        if v not in self.d:
            raise KeyError("{} is not in the graph".format(v))
        
        #ensure there is an edge to remove between u and v
        if v not in self.d[u]:
            raise KeyError("There is no edge from {} to {}".format(u, v))
        if u not in self.d[v]:
            raise KeyError("There is no edge from {} to {}".format(v, u))

        #remove the edge from u to v
        self.d[v].remove(u)
        #remove the edge from v to u
        self.d[u].remove(v)
        return

    # Problem 2
    def traverse(self, source):
        """Traverse the graph with a breadth-first search until all nodes
        have been visited. Return the list of nodes in the order that they
        were visited.

        Parameters:
            source: the node to start the search at.

        Returns:
            (list): the nodes in order of visitation.

        Raises:
            KeyError: if the source node is not in the graph.
        """
        #Make sure source node is in the graph
        if source not in self.d.keys():
            raise KeyError("{} is not in the graph".format(source))

        #construct V, Q, and M
        V = []
        Q = deque([source])
        M = set([source])

        #print("Initial Set Up:")
        #print("V: {}".format(V))
        #print("Q: {}".format(Q))
        #print("M: {}".format(M))
        #print()

        i=1
        while len(Q) > 0:
            current_node = Q.pop()
            V.append(current_node)
            for neighbor in self.d[current_node]:
                if neighbor not in M:
                    Q.appendleft(neighbor)
                    M.add(neighbor)
            #print("Step {}".format(i))
            #print("V: {}".format(V))
            #print("Q: {}".format(Q))
            #print("M: {}".format(M))
            #print()
            i += 1
        return V

    # Problem 3
    def shortest_path(self, source, target):
        """Begin a BFS at the source node and proceed until the target is
        found. Return a list containing the nodes in the shortest path from
        the source to the target, including endoints.

        Parameters:
            source: the node to start the search at.
            target: the node to search for.

        Returns:
            A list of nodes along the shortest path from source to target,
                including the endpoints.

        Raises:
            KeyError: if the source or target nodes are not in the graph.
        """

        """Assumption: There is a path between all nodes in the Graph"""
        #Make sure source node is in the graph
        if source not in self.d.keys():
            raise KeyError("{} is not in the graph".format(source))

        #Make sure target node is in the graph
        if target not in self.d.keys():
            raise KeyError("{} is not in the graph".format(target))

        #construct V, Q, and M
        V = []
        Q = deque([source])
        M = set([source])

        #initialize paths dict to find shortest path
        paths = dict()

        while len(Q) > 0:
            current_node = Q.pop()
            V.append(current_node)
            if current_node == target:
                break

            for neighbor in self.d[current_node]:
                if neighbor not in M:
                    Q.append(neighbor)
                    M.add(neighbor)

                    #add neighbor to paths
                    paths[neighbor] = current_node

        #get shortest path from paths
        shortest_path = [current_node]
        while current_node in paths:
            next_node = paths[current_node]
            shortest_path.append(next_node)
            current_node = next_node

        return list(reversed(shortest_path))


# Problems 4-6
class MovieGraph:
    """Class for solving the Kevin Bacon problem with movie data from IMDb."""

    # Problem 4
    def __init__(self, filename="movie_data.txt"):
        """Initialize a set for movie titles, a set for actor names, and an
        empty NetworkX Graph, and store them as attributes. Read the speficied
        file line by line, adding the title to the set of movies and the cast
        members to the set of actors. Add an edge to the graph between the
        movie and each cast member.

        Each line of the file represents one movie: the title is listed first,
        then the cast members, with entries separated by a '/' character.
        For example, the line for 'The Dark Knight (2008)' starts with

        The Dark Knight (2008)/Christian Bale/Heath Ledger/Aaron Eckhart/...

        Any '/' characters in movie titles have been replaced with the
        vertical pipe character | (for example, Frost|Nixon (2008)).
        """
        self.movie_titles = set()
        self.actor_names = set()
        self.graph = nx.Graph()

        #Parse File
        with open(filename, encoding="utf8") as file: #the encoding="utf8" is needed for running on windows
            
            while True:
                lines = file.readlines(5000) #read lines in 5000 at a time
                if not lines:
                    break

                #grab lines in file
                for line in lines:
                    title_actors = line.strip().split('/')

                    #get title
                    title = title_actors.pop(0)
                    self.movie_titles.add(title)

                    #add edge from title to each actor
                    for actor in title_actors:
                        self.actor_names.add(actor)
                        self.graph.add_edge(title, actor)

        return

    # Problem 5
    def path_to_actor(self, source, target):
        """Compute the shortest path from source to target and the degrees of
        separation between source and target.

        Returns:
            (list): a shortest path from source to target, including endpoints.
            (int): the number of steps from source to target, excluding movies.
        """
        #get shortest path
        sp = nx.shortest_path(self.graph, source, target)

        #return shortest path with length excluding movies
        return sp, (len(sp)-1)//2

    # Problem 6
    def average_number(self, target):
        """Calculate the shortest path lengths of every actor to the target
        (not including movies). Plot the distribution of path lengths and
        return the average path length.

        Returns:
            (float): the average path length from actor to target.
        """
        #get shortest path lengths from target to all other actors
        spls = []
        dic = nx.shortest_path_length(self.graph, target)
        for node, pl in dic.items():
            if node in self.actor_names:
                spls.append(pl)

        #divide all lengths by two
        spls = np.array(spls)//2

        #plot the lengths
        plt.hist(spls, bins=[i-.5 for i in range(8)])
        plt.title('Path lengths from {}'.format(target))
        plt.xlabel('Path Lengths')
        plt.ylabel('Number of Actors')
        plt.show()
        return np.mean(spls)


def test_Graph():
    #instantiate a Graph object
    G = Graph()
    print("Initial graph:")
    print(G)
    print()

    #add nodes
    for node in ["A", "B", "C"]:
        G.add_node(node)
    print("Added 3 nodes")
    print(G)
    print()

    #add edges
    G.add_edge("A", "B")
    G.add_edge("B", "A") #redundant
    G.add_edge("B", "C")
    G.add_edge("C", "B")
    G.add_edge("D", "C") #D should be added first
    G.add_edge("E", "F") #E and "F" should be added first
    print("Added edges")
    print(G)
    print()

    #remove a node
    G.remove_node("A")
    print("Removed node A")
    print(G)
    print()

    #try to remove a node not in graph
    print("Try to remove A which is not in Graph")
    try:
        G.remove_node("A")
    except KeyError:
        pass
    print("Successfully handled")
    print(G)
    print()

    #remove edge
    print("Removing edge from B to C")
    G.remove_edge("B", "C")
    print(G)
    print()

    #attempt to remove edge not in graph
    print("Try to remove edge from B to C")
    try:
        G.remove_edge("B", "C")
    except KeyError:
        pass
    print("Try to remove edge from C to B")
    try:
        G.remove_edge("C", "B")
    except KeyError:
        pass
    print("Successfully handled")
    print(G)
    print()

    #remove all remaining nodes:
    print("Removing all remaining nodes")
    nodes = list(G.d.keys())
    for node in nodes:
        G.remove_node(node)
    print(G)
    print()

    #Build Graph from lab example
    print("Rebuilding graph")
    G = Graph()
    
    G.add_edge("A", "D")
    G.add_edge("A", "B")
    G.add_edge("B", "D")
    G.add_edge("B", "A")
    G.add_edge("C", "D")  
    G.add_edge("D", "B")
    G.add_edge("D", "C")
    G.add_edge("D", "A")
    print(G)
    print()

    #traverse graph
    print("Traversing Graph from A")
    assert G.traverse("A") == ["A", "D", "B", "C"]

    #Build graph from test case
    print("Rebuilding graph")
    G = Graph()
    G.add_edge("A", "F")
    G.add_edge("A", "B")
    G.add_edge("A", "G")  
    G.add_edge("B", "C")
    G.add_edge("B", "A")
    G.add_edge("C", "D")
    G.add_edge("C", "B")
    G.add_edge("D", "C")
    G.add_edge("D", "E")
    G.add_edge("E", "F")
    G.add_edge("E", "D")
    G.add_edge("F", "G")
    G.add_edge("F", "A")
    G.add_edge("F", "E")
    G.add_edge("G", "F")
    G.add_edge("G", "A")
    print(G)
    assert G.traverse("A") == ["A", "F", "B", "G", "E", "C", "D"]
    print(G)
    print()

    #find shortest path between A and C
    print("Find shortest path between A and C")
    print(G.shortest_path("A", "C"))

def test_MovieGraph():
    #instantiate object
    mg = MovieGraph("movie_data.txt")

    actor = "Al Pacino"
    actor2 = "Frank Whaley"
    actor3 = "Kevin Bacon"
    #test path_to_actor
    path = mg.path_to_actor(actor, actor2)
    print("Path from {} to {}: {}".format(actor, actor2, path))

    #test average_number()
    avg = mg.average_number(actor)
    print(avg)
    assert avg == 2.55, "prob6 failed"
    
    print("Average path length from {} is {}".format(actor, avg))

    #avg = mg.average_number("Kevin Bacon")
    #print("Average path length from {} is {}".format(actor3, avg))


if __name__ == "__main__":
    #probs 1, 2, and 3
    #test_Graph()

    #probs 4
    test_MovieGraph()
