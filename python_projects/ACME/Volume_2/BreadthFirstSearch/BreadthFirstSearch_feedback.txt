10/26/18 17:54

breadth_first_search.py has not been modified yet

-------------------------------------------------------------------------------

10/26/18 18:14

breadth_first_search.py has not been modified yet

-------------------------------------------------------------------------------

10/27/18 13:00

breadth_first_search.py has not been modified yet

-------------------------------------------------------------------------------

10/29/18 17:35

Problem 1 (5 points):
Score += 5

Problem 2 (10 points):
Graph.traverse('A') failed
	Graph: {'A': {'B', 'D'}, 'B': {'A', 'D'}, 'C': {'D'}, 'D': {'C', 'A', 'B'}}
	Correct response: "['A', 'B', 'D', 'C']"
	Student response: "['A', 'D', 'C', 'B']"
Graph.traverse('A') failed
	Graph: {'A': {'G', 'F', 'B'}, 'B': {'C', 'A'}, 'C': {'B', 'D'}, 'D': {'C', 'E'}, 'E': {'F', 'D'}, 'F': {'G', 'A', 'E'}, 'G': {'A', 'F'}}
	Correct response: "['A', 'G', 'F', 'B', 'E', 'C', 'D']"
	Student response: "['A', 'B', 'C', 'D', 'E', 'F', 'G']"
Graph.traverse('A') failed
	Graph: {'A': {'C', 'B'}, 'B': {'A', 'E', 'D'}, 'C': {'G', 'A', 'F'}, 'D': {'I', 'B', 'H'}, 'E': {'J', 'K', 'B'}, 'F': {'M', 'C', 'L'}, 'G': {'C', 'O', 'N'}, 'H': {'D'}, 'I': {'D'}, 'J': {'E'}, 'K': {'E'}, 'L': {'F'}, 'M': {'F'}, 'N': {'G'}, 'O': {'G'}}
	Correct response: "['A', 'C', 'B', 'G', 'F', 'E', 'D', 'O', 'N', 'M', 'L', 'J', 'K', 'I', 'H']"
	Student response: "['A', 'B', 'D', 'H', 'I', 'E', 'K', 'J', 'C', 'F', 'L', 'M', 'G', 'N', 'O']"
Graph.traverse('A') failed
	Graph: {'A': {'B'}, 'B': {'C', 'A', 'D'}, 'C': {'B', 'D'}, 'D': {'C', 'B', 'E'}, 'E': {'G', 'F', 'D'}, 'F': {'H', 'E'}, 'H': {'F'}, 'G': {'I', 'J', 'E'}, 'I': {'G', 'J'}, 'J': {'I', 'G', 'K', 'J'}, 'K': {'J', 'L'}, 'L': {'K'}}
	Correct response: "['A', 'B', 'C', 'D', 'E', 'G', 'F', 'I', 'J', 'H', 'K', 'L']"
	Student response: "['A', 'B', 'D', 'E', 'F', 'H', 'G', 'J', 'K', 'L', 'I', 'C']"
Score += 2

Problem 3 (10 points):
Score += 10

Problem 5 (10 points):
Score += 10

Problem 6 (10 points):
MovieGraph.average_number(Kevin Bacon) failed
Score += 5

Code Quality (5 points):
Score += 5

Total score: 37/50 = 74.0%

-------------------------------------------------------------------------------

10/31/18 15:08

Problem 1 (5 points):
Score += 5

Problem 2 (10 points):
Graph.traverse('A') failed
	Graph: {'A': {'D', 'B'}, 'B': {'D', 'A'}, 'C': {'D'}, 'D': {'B', 'C', 'A'}}
	Correct response: "['A', 'D', 'B', 'C']"
	Student response: "['A', 'B', 'D', 'C']"
Graph.traverse('A') failed
	Graph: {'A': {'F', 'B', 'G'}, 'B': {'C', 'A'}, 'C': {'D', 'B'}, 'D': {'C', 'E'}, 'E': {'F', 'D'}, 'F': {'G', 'A', 'E'}, 'G': {'F', 'A'}}
	Correct response: "['A', 'F', 'B', 'G', 'E', 'C', 'D']"
	Student response: "['A', 'G', 'B', 'C', 'D', 'E', 'F']"
Graph.traverse('A') failed
	Graph: {'A': {'C', 'B'}, 'B': {'D', 'A', 'E'}, 'C': {'F', 'A', 'G'}, 'D': {'H', 'I', 'B'}, 'E': {'K', 'J', 'B'}, 'F': {'M', 'C', 'L'}, 'G': {'N', 'C', 'O'}, 'H': {'D'}, 'I': {'D'}, 'J': {'E'}, 'K': {'E'}, 'L': {'F'}, 'M': {'F'}, 'N': {'G'}, 'O': {'G'}}
	Correct response: "['A', 'C', 'B', 'F', 'G', 'D', 'E', 'M', 'L', 'N', 'O', 'H', 'I', 'K', 'J']"
	Student response: "['A', 'B', 'E', 'J', 'K', 'D', 'I', 'H', 'C', 'G', 'O', 'N', 'F', 'L', 'M']"
Graph.traverse('A') failed
	Graph: {'A': {'B'}, 'B': {'D', 'C', 'A'}, 'C': {'D', 'B'}, 'D': {'C', 'B', 'E'}, 'E': {'F', 'D', 'G'}, 'F': {'H', 'E'}, 'H': {'F'}, 'G': {'J', 'I', 'E'}, 'I': {'J', 'G'}, 'J': {'K', 'J', 'I', 'G'}, 'K': {'L', 'J'}, 'L': {'K'}}
	Correct response: "['A', 'B', 'D', 'C', 'E', 'F', 'G', 'H', 'J', 'I', 'K', 'L']"
	Student response: "['A', 'B', 'C', 'D', 'E', 'G', 'I', 'J', 'K', 'L', 'F', 'H']"
Score += 2

Problem 3 (10 points):
Score += 10

Problem 5 (10 points):
Score += 10

Problem 6 (10 points):
MovieGraph.average_number(Kevin Bacon) failed
Score += 5

Code Quality (5 points):
Score += 5

Total score: 37/50 = 74.0%

-------------------------------------------------------------------------------

11/01/18 10:23

Problem 1 (5 points):
Graph.add_edge() failed
Graph.add_edge() failed
KeyError: 'There is no edge from i to h'

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Score += 10

Problem 5 (10 points):
Score += 10

Problem 6 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 45/50 = 90.0%

Great job!

-------------------------------------------------------------------------------

