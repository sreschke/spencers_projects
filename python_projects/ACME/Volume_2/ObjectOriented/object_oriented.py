# object_oriented.py
"""Python Essentials: Object Oriented Programming.
Spencer Reschke
Math 321
9/11/18
"""
from math import sqrt

class Backpack:
    """A Backpack object class. Has a name, color, max_size, and a list of contents.

    Attributes:
        name (str): the name of the backpack's owner.
        contents (list): the contents of the backpack.
    """

    # Problem 1: Modify __init__() and put(), and write dump().
    def __init__(self, name, color, max_size=5):
        """Set the name, color, max_size and initialize an empty list of contents.

        Parameters:
            name (str): the name of the backpack's owner.
        """
        self.name = name
        self.color = color
        self.max_size = max_size
        self.contents = []

    def put(self, item):
        """Add an item to the backpack's list of contents if there is room"""
        if len(self.contents) >= self.max_size:
            print("No Room!")
        else:
            self.contents.append(item)

    def take(self, item):
        """Remove an item from the backpack's list of contents."""
        self.contents.remove(item)

    def dump(self):
        """resets the backpack's contents to an empty list"""
        self.contents = []

    # Magic Methods -----------------------------------------------------------

    # Problem 3: Write __eq__() and __str__().
    def __add__(self, other):
        """Add the number of contents of each Backpack."""
        return len(self.contents) + len(other.contents)

    def __lt__(self, other):
        """Compare two backpacks. If 'self' has fewer contents
        than 'other', return True. Otherwise, return False.
        """
        return len(self.contents) < len(other.contents)

    def __eq__(self, other):
        if (self.name == other.name and 
            self.color == other.color and
            len(self.contents) == len(other.contents)):
            return True
        else:
           return False

    def __str__(self):
        out_str = "Owner:\t\t{}\n".format(self.name)
        out_str += "Color:\t\t{}\n".format(self.color)
        out_str += "Size:\t\t{}\n".format(len(self.contents))
        out_str += "Max Size:\t{}\n".format(self.max_size)
        out_str += "Contents:\t{}\n".format(self.contents)
        return out_str



# An example of inheritance. You are not required to modify this class.
class Knapsack(Backpack):
    """A Knapsack object class. Inherits from the Backpack class.
    A knapsack is smaller than a backpack and can be tied closed.

    Attributes:
        name (str): the name of the knapsack's owner.
        color (str): the color of the knapsack.
        max_size (int): the maximum number of items that can fit inside.
        contents (list): the contents of the backpack.
        closed (bool): whether or not the knapsack is tied shut.
    """
    def __init__(self, name, color):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A knapsack only holds 3 item by default.

        Parameters:
            name (str): the name of the knapsack's owner.
            color (str): the color of the knapsack.
            max_size (int): the maximum number of items that can fit inside.
        """
        Backpack.__init__(self, name, color, max_size=3)
        self.closed = True

    def put(self, item):
        """If the knapsack is untied, use the Backpack.put() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.put(self, item)

    def take(self, item):
        """If the knapsack is untied, use the Backpack.take() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.take(self, item)

    def weight(self):
        """Calculate the weight of the knapsack by counting the length of the
        string representations of each item in the contents list.
        """
        return sum(len(str(item)) for item in self.contents)

#problem 1 last part
def test_backpack(): 
    testpack = Backpack("Barry", "black", 3) # Instantiate the object.
    testpack2 = Backpack("Barry", "black", 3)
    if testpack.name != "Barry": # Test an attribute. 
        print("Backpack.name assigned incorrectly") 
    for item in ["pencil", "pen", "paper", "computer"]: 
        testpack.put(item) # Test a method. 
        testpack2.put(item)
    testpack2.contents.pop(0)
    print(testpack == testpack2)

test_backpack()
# Problem 2: Write a 'Jetpack' class that inherits from the 'Backpack' class.
class Jetpack(Backpack):
    def __init__(self, name, color, max_size=2, fuel=10):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A knapsack only holds 3 item by default.

        Parameters:
            name (str): the name of the knapsack's owner.
            color (str): the color of the knapsack.
            max_size (int): the maximum number of items that can fit inside.
        """
        Backpack.__init__(self, name, color, max_size)
        self.fuel = fuel

    def fly(self, fuel_to_burn):
        """Uses up fuel_to_burn from JetPack's fuel if possible"""
        if fuel_to_burn > self.fuel:
            print("Not enough fuel!")
        else:
            self.fuel -= fuel_to_burn

    def dump(self):
        """Resets JetPack's contents to an empty list and sets the fuel to zero"""
        self.contents = []
        self.fuel = 0



# Problem 4: Write a 'ComplexNumber' class.
class ComplexNumber(object):
    def __init__(self, real, imag):
        self.real = real
        self.imag = imag

    def conjugate(self):
        return ComplexNumber(self.real, -self.imag)

    def __str__(self):
        if self.imag < 0:
            return "({}-{}j)".format(self.real, -self.imag)
        else:
            return "({}+{}j)".format(self.real, self.imag)

    def __abs__(self):
        return sqrt(self.real**2 + self.imag**2)

    def __eq__(self, other):
        if self.real == other.real and self.imag == other.imag:
            return True
        else:
            return False

    def __add__(self, other):
        return ComplexNumber(self.real+other.real, self.imag+other.imag)

    def __sub__(self, other):
        return ComplexNumber(self.real-other.real, self.imag-other.imag)

    def __mul__(self, other):
        return ComplexNumber(self.real*other.real - self.imag*other.imag,
                             self.imag*other.real + self.real*other.imag)
    
    def __truediv__(self, other):
        real = (self.real*other.real + self.imag*other.imag)/(other.real**2 + other.imag**2)
        imag = (self.imag*other.real - self.real*other.imag)/(other.real**2 + other.imag**2)
        return ComplexNumber(real, imag)

def test_ComplexNumber(a, b): 
    py_cnum, my_cnum = complex(a, b), ComplexNumber(a, b)
    py_cnum2, my_cnum2 = complex(2*a-1, b-2), ComplexNumber(2*a-1, b-2)
    
    #Validate the constructor. 
    if my_cnum.real != a or my_cnum.imag != b: 
        print("__init__() set self.real and self.imag incorrectly") 
        
    # Validate conjugate() by checking the new number's imag attribute.     
    if py_cnum.conjugate().imag != my_cnum.conjugate().imag: 
        print("conjugate() failed for", py_cnum)
    # Validate __str__(). 
    if str(py_cnum) != str(my_cnum): 
        print("__str__() failed for", py_cnum)
    #validate __abs__().
    if abs(py_cnum) != abs(my_cnum):
        print("__abs__() failed for", py_cnum)
    #validate __eq__()
    if not (my_cnum == ComplexNumber(a, b) and not my_cnum == ComplexNumber(-a, b)):
        print("__eq__() failed for", py_cnum)
    #validate __add__()
    if ((py_cnum + py_cnum2).imag != (my_cnum + my_cnum2).imag or
        (py_cnum + py_cnum2).real != (my_cnum + my_cnum2).real):
        print("__add__() failed for", py_cnum, " and ", py_cnum2)
    #validate __sub__()
    if ((py_cnum - py_cnum2).imag != (my_cnum - my_cnum2).imag or
        (py_cnum - py_cnum2).real != (my_cnum - my_cnum2).real):
        print("__sub__() failed for", py_cnum, " and ", py_cnum2)
    #validate __mul__()
    if ((py_cnum * py_cnum2).imag != (my_cnum * my_cnum2).imag or
        (py_cnum * py_cnum2).real != (my_cnum * my_cnum2).real):
        print("__mul__() failed for", py_cnum, " and ", py_cnum2)
    #validate __truediv__()
    if ((py_cnum / py_cnum2).imag != (my_cnum / my_cnum2).imag or
        (py_cnum / py_cnum2).real != (my_cnum / my_cnum2).real):
        print("__add__() failed for", py_cnum, " and ", py_cnum2)
    

test_ComplexNumber(1, -2)