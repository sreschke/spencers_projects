# markov_chains.py
"""Volume II: Markov Chains.
Spencer Reschke
Math 321
11/6/2018
"""

import numpy as np
from numpy.linalg import norm


# Problem 1
def random_chain(n):
    """Create and return a transition matrix for a random Markov chain with
    'n' states. This should be stored as an nxn NumPy array.
    """
    #make sure n is a postive integer
    assert type(n) == int and n > 0, "n must be a positive integer"

    matrix = np.random.rand(n, n) #generate a random nxn matrix
    matrix = matrix/matrix.sum(axis=0) #divide each column by the column sum
    assert np.allclose(matrix.sum(axis=0), np.array([1]*n )), "Column sums aren't 1"
    return matrix


# Problem 2
def forecast(days):
    """Forecast weather for the next days days given that today is hot.
    
    Parameters:
        days (int): an integer specifying the number of days to forecast weather for
    Returns:
        a list of weather predictions. 0 for hot, 1 for cold. For example the list
        [0, 1, 1, 0] predicts the first and fourth days to be cold and predicts
        the second and third days to be cold.
        
        
    Example:
        >>> forecast(4) 
        [0, 1, 1, 0] #a cold day, two hot days and a cold day
        >>> forecast(3)
        [0, 0, 1] # two cold days and a hot day"""

    
    assert type(days) == int and days > 0, "days must be a positive integer"

    #markov transition matrix
    transition = np.array([[0.7, 0.6], [0.3, 0.4]])

    predictions = []
    temp = 1 #1 for hot, 0 for cold; the original day starts as hot
    for day in range(days):
        temp = np.random.binomial(1, transition[1, temp])
        predictions.append(temp)
    return predictions


# Problem 3
def four_state_forecast(days):
    """Run a simulation for the weather over the specified number of days,
    with mild as the starting state, using the four-state Markov chain.
    Return a list containing the day-by-day results, not including the
    starting day.

    Paramters:
        days (int): the number of days to forecast whether for

    Examples:
        >>> four_state_forecast(3)
        [0, 1, 3]
        >>> four_state_forecast(5)
        [2, 1, 2, 1, 1]
    """
    assert type(days) == int and days > 0, "days must be a postive integer"
    #Construct the transition matrix
    #The columns and rows correspond to hot, mild, cold, freezing
    #Given that today is hot (first column) the probability that it is cold (3rd row)
    #is 0.2
    transitions = np.array([[0.5, 0.3, 0.1, 0],
                             [0.3, 0.3, 0.3, 0.3],
                             [0.2, 0.3, 0.4, 0.5],
                             [0, 0.1, 0.2, 0.2]])
    temp = 0 #0 for hot, 1 for mild, 2 for cold, 3 for freezing; the temperature is initially hot
    predictions = []
    for day in range(days):
        temp = np.argmax(np.random.multinomial(1, transitions[:, temp]))
        predictions.append(temp)

    assert len(predictions) == days, "predictions has wrong length"
    return predictions


# Problem 4
def steady_state(A, tol=1e-12, N=40):
    """Compute the steady state of the transition matrix A.

    Inputs:
        A ((n,n) ndarray): A column-stochastic transition matrix.
        tol (float): The convergence tolerance.
        N (int): The maximum number of iterations to compute.

    Raises:
        ValueError: if the iteration does not converge within N steps.

    Returns:
        x ((n,) ndarray): The steady state distribution vector of A.
    """
    assert A.shape[0] == A.shape[1], "A must be square"

    #generate a random initial state distribution
    x = np.random.rand(A.shape[0])
    x = x/x.sum() #normalize x

    for step in range(N):
        x_next = A @ x
        error = norm(x - x_next)
        x = x_next
        if error < tol:
            return x
    raise ValueError("The iteration did not converge within N steps")


# Problems 5 and 6
class SentenceGenerator(object):
    """Markov chain creator for simulating bad English.

    Attributes:
        num_words (int): the number of unique words in input file
        transition (num_words x num_words ndarray): the transition matrix

    Example:
        >>> yoda = SentenceGenerator("Yoda.txt")
        >>> print(yoda.babble())
        The dark side of loss is a path as one with you.
    """
    def __init__(self, filename):
        """Read the specified file and build a transition matrix from its
        contents. You may assume that the file has one complete sentence
        written on each line.
        """
        ##################################################
        #Algorithm 5.1
        ##################################################
        #open the file and read the words into a set
        with open(filename) as file:
            words_set = set(file.read().replace('\n', ' ').split(' '))
        if "" in words_set:
            words_set.remove("")

        #get the number of words
        num_words = len(words_set)

        #initialize an array of zeros for the transition matrix
        transition = np.zeros((num_words+2, num_words+2)) #+2 to include the $tart and $top tokens

        states = ["$tart"]
        states_set = set(states) #searching a set is faster than searching a list; memory trade-off for speed

        with open(filename) as file:
            lines = file.readlines()
            for sentence in lines:
                if sentence == "\n":
                    continue
                words = sentence.split(' ')
                indices = []
                for word in words:
                    word = word.rstrip()
                    if word == "":
                        continue
                    if word not in states_set:
                        states.append(word)
                        states_set.add(word)
                    indices.append(states.index(word))
                if indices == []:
                    print("Sentence: " + sentence)
                #$tart to first word
                transition[indices[0], 0] += 1
                for ind in range(len(indices)-1):
                    transition[indices[ind+1], indices[ind]] += 1
                transition[-1, indices[-1]] += 1
            #make sure stop state transitions to itself
            transition[-1, -1] = 1
            states.append("$top")

            #normalize columns
            transition = transition/transition.sum(axis=0)

        #print(words_set - set(states))
        #print(set(states) - words_set)
        assert len(states) == (num_words+2), "not counting words the same"
        self.num_words = num_words
        self.states = states
        self.transition = transition



    def babble(self):
        """Begin at the start sate and use the strategy from
        four_state_forecast() to transition through the Markov chain.
        Keep track of the path through the chain and the corresponding words.
        When the stop state is reached, stop transitioning and terminate the
        sentence. Return the resulting sentence as a single string.
        """
        state = "$tart"
        sentence = []
        while state != "$top":
            column = self.transition[:, self.states.index(state)]
            index = np.argmax(np.random.multinomial(1, column))
            state = self.states[index]
            sentence.append(state)
        return " ".join(sentence[:-1])


def test_1():
    """Unit test for problem 1"""
    assert np.allclose(random_chain(1).sum(axis=0), np.array([1])), "Problem 1 failed"
    assert np.allclose(random_chain(3).sum(axis=0), np.array([1]*3)), "Problem 1 failed"
    assert np.allclose(random_chain(10).sum(axis=0), np.array([1]*10)), "Problem 1 failed"
    print("All tests passed for problem 1")

def test_2():
    forc = forecast(10)
    assert len(forc) == 10, "length of forecast is wrong"
    print("All tests passed for problem 2")
    
def test_3():
    forc = four_state_forecast(1)
    assert forc[0] in [0, 1, 2, 3], "test_3 failed"
    assert len(forc) == 1, "test_3 failed"

    forc = four_state_forecast(10)
    for temp in forc:
        assert temp in [0, 1, 2, 3], "test_3 failed"
    assert len(forc) == 10, "test_3 failed"
    print("All tests passed for problem 3")

def test_4():
    #Test transition matrix from problem 2
    A = np.array([[0.7, 0.6], [0.3, 0.4]])
    x = steady_state(A)
    assert np.allclose(x, np.array([2/3, 1/3])), "test_4 failed"  
    #Test that x is an eigenvector of A with eigenvalue 1
    assert np.allclose(x, A@x), "test_4 failed"

    #Test transition matrix from problem 3
    A = transitions = np.array([[0.5, 0.3, 0.1, 0],
                                [0.3, 0.3, 0.3, 0.3],
                                [0.2, 0.3, 0.4, 0.5],
                                [0, 0.1, 0.2, 0.2]])

    x = steady_state(A)
    #Test that x is an eigenvector of A with eigenvalue 1
    assert np.allclose(x, A@x), "test_4 failed"
    print("All tests passed for problem 4")

def test_SentenceGenerator():
    sg = SentenceGenerator("test.txt")
    assert sg.num_words == 15, "words counted incorrectly in test.txt"
    assert sg.transition[-1, -1] == 1, "$top token does not transition to itself"
    assert np.allclose(sg.transition.sum(axis=0), np.array([1]*(sg.num_words+2))), "The columns of the transition matrix are not normalized"
    
    print()
    print("Test:")
    for _ in range(5):
        print(sg.babble())
    print()

    yoda = SentenceGenerator("yoda.txt")
    assert len(yoda.states) == len(yoda.transition), "states and transition are misaligned"
    
    print("Yoda:")
    for _ in range(5):
        print(yoda.babble())
    print()

    trump = SentenceGenerator("trump.txt")
    assert len(trump.states) == len(trump.transition), "states and transition are misaligned"
    print("Trump")
    for _ in range(5):
        print(trump.babble())
    print()
    
    print("All tests passed for Sentence Generator")

if __name__ == "__main__":
    test_1()
    test_2()
    test_3()
    test_4()
    test_SentenceGenerator()