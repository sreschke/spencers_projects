# binary_trees.py
"""Volume 2: Binary Trees.
Spencer
Reschke
10/17/18
"""
import random
import copy
import time
from matplotlib import pyplot as plt

# These imports are used in BST.draw().
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout


class SinglyLinkedListNode:
    """A node with a value and a reference to the next node."""
    def __init__(self, data):
        self.value, self.next = data, None

class SinglyLinkedList:
    """A singly linked list with a head and a tail."""
    def __init__(self):
        self.head, self.tail = None, None

    def append(self, data):
        """Add a node containing the data to the end of the list."""
        n = SinglyLinkedListNode(data)
        if self.head is None:
            self.head, self.tail = n, n
        else:
            self.tail.next = n
            self.tail = n

    def iterative_find(self, data):
        """Search iteratively for a node containing the data.
        If there is no such node in the list, including if the list is empty,
        raise a ValueError.

        Returns:
            (SinglyLinkedListNode): the node containing the data.
        """
        current = self.head
        while current is not None:
            if current.value == data:
                return current
            current = current.next
        raise ValueError(str(data) + " is not in the list")

    # Problem 1
    def recursive_find(self, data):
        """Search recursively for the node containing the data.
        If there is no such node in the list, including if the list is empty,
        raise a ValueError.

        Returns:
            (SinglyLinkedListNode): the node containing the data.
        """

        #recursive find function
        def _step(cur_node, data):
            """recursive function for recursive_find()
            Parameters:
                cur_node - the current node
                data - the data we're looking for in the linked list
        
            Returns:
                The node in the linked list if present. 
            Raises:
                a ValueError if the data is not in the list"""

            if cur_node is None:
                raise ValueError(str(data) + " is not in the list")
            elif (data == cur_node.value): #if we've found the node, return it
                return cur_node
            else: #call function on next node
                return _step(cur_node.next, data)


        #begin with the head of the linked list
        current = self.head
        return _step(current, data)  


class BSTNode:
    """A node class for binary search trees. Contains a value, a
    reference to the parent node, and references to two child nodes.
    """
    def __init__(self, data):
        """Construct a new node and set the value attribute. The other
        attributes will be set when the node is added to a tree.
        """
        self.value = data
        self.prev = None        # A reference to this node's parent node.
        self.left = None        # self.left.value < self.value
        self.right = None       # self.value < self.right.value


class BST:
    """Binary search tree data structure class.
    The root attribute references the first node in the tree.
    """
    def __init__(self):
        """Initialize the root attribute."""
        self.root = None

    def find(self, data):
        """Return the node containing the data. If there is no such node
        in the tree, including if the tree is empty, raise a ValueError.
        """

        # Define a recursive function to traverse the tree.
        def _step(current):
            """Recursively step through the tree until the node containing
            the data is found. If there is no such node, raise a Value Error.
            """
            if current is None:                     # Base case 1: dead end.
                raise ValueError(str(data) + " is not in the tree.")
            if data == current.value:               # Base case 2: data found!
                return current
            if data < current.value:                # Recursively search left.
                return _step(current.left)
            else:                                   # Recursively search right.
                return _step(current.right)

        # Start the recursion on the root of the tree.
        return _step(self.root)

    # Problem 2
    def insert(self, data):
        """Insert a new node containing the specified data.

        Raises:
            ValueError: if the data is already in the tree.

        Example:
            >>> tree = BST()                    |
            >>> for i in [4, 3, 6, 5, 7, 8, 1]: |            (4)
            ...     tree.insert(i)              |            / \
            ...                                 |          (3) (6)
            >>> print(tree)                     |          /   / \
            [4]                                 |        (1) (5) (7)
            [3, 6]                              |                  \
            [1, 5, 7]                           |                  (8)
            [8]                                 |
        """
        # Define a recursive function to traverse the tree.
        def _step(cur_node, data):
            """recursive function for insert()
            parameters:
                cur_node - the current node in the tree
                data - the data to be inserted

            Raises:
                A ValueError if the data is already in the tree                
            """
            val = cur_node.value
            if val == data:
                raise ValueError("{} is already in the tree.".format(data))
            elif data < val:
                if cur_node.left is None:
                    #create a new node and link it to its parent
                    cur_node.left = BSTNode(data)
                    cur_node.left.prev = cur_node
                    return
                else:
                    return _step(cur_node.left, data)
            else:
                if cur_node.right is None:
                    #create a new node and link it to its parent
                    cur_node.right = BSTNode(data)
                    cur_node.right.prev = cur_node
                    return
                else:
                    return _step(cur_node.right, data)
            
        if self.root is None:
            self.root = BSTNode(data)
            return
        else:
            return _step(self.root, data)
            

    # Problem 3
    def remove(self, data):
        """Remove the node containing the specified data.

        Raises:
            ValueError: if there is no node containing the data, including if
                the tree is empty.

        Examples:
            >>> print(12)                       | >>> print(t3)
            [6]                                 | [5]
            [4, 8]                              | [3, 6]
            [1, 5, 7, 10]                       | [1, 4, 7]
            [3, 9]                              | [8]
            >>> for x in [7, 10, 1, 4, 3]:      | >>> for x in [8, 6, 3, 5]:
            ...     t1.remove(x)                | ...     t3.remove(x)
            ...                                 | ...
            >>> print(t1)                       | >>> print(t3)
            [6]                                 | [4]
            [5, 8]                              | [1, 7]
            [9]                                 |
                                                | >>> print(t4)
            >>> print(t2)                       | [5]
            [2]                                 | >>> t4.remove(1)
            [1, 3]                              | ValueError: <message>
            >>> for x in [2, 1, 3]:             | >>> t4.remove(5)
            ...     t2.remove(x)                | >>> print(t4)
            ...                                 | []
            >>> print(t2)                       | >>> t4.remove(5)
            []                                  | ValueError: <message>
        """
        #helper functions
        def remove_leaf(cur_node):
            """helper function for remove(). Called to remove a leaf node"""
            #ensure cur_node is a leaf
            assert cur_node.left is None and cur_node.right is None, "cur_node must be a leaf node"
            #ensure cur_node is not the root
            assert cur_node.prev is not None, "cur_node can't be the root"

            parent = cur_node.prev
            if (parent.left == cur_node): #if cur_node is a left child
                parent.left = None
                return
            elif (parent.right == cur_node): #cur_node is a right child
                parent.right = None
                return
            else:
                assert True==False, "We should never get here"

        def remove_one(cur_node):
            """Helper function for remove. Called if cur_node has only one
            child and the cur_node is not the root"""

            #ensure cur_node has a parent
            assert cur_node.prev is not None, "Can't call remove_one with root node"
            #ensure cur_node has only one child
            assert (cur_node.left is None and cur_node.right is not None) or (cur_node.left is not None and cur_node.right is None), "cur_node must have exactly one child"

            parent = cur_node.prev #get parent node
            if cur_node.left is not None: #if cur_node has a left child
                if parent.left == cur_node: #if cur_node is a left child
                    parent.left = cur_node.left 
                    cur_node.left.prev = parent
                    return
                else: #cur_node is a right child
                    parent.right = cur_node.left
                    cur_node.left.prev = parent
                    return
            else: #cur_node has a right child
                if parent.left == cur_node: #if cur_node is a left child
                    parent.left = cur_node.right 
                    cur_node.right.prev = parent
                    return
                else: #cur_node is a right child
                    parent.right = cur_node.right
                    cur_node.right.prev = parent
                    return
        def remove_two(cur_node):
            """Helper function for remove(). Called if the target node has
            two children."""
            #get predessesor node and its value
            pred = get_predecessor(cur_node)
            val = pred.value
            
            #pred should have at most one child. We remove it using remove_leaf() or remove_one()
            if (pred.left is None and pred.right is None): #if pred is a leaf
                remove_leaf(pred)
            else:
                remove_one(pred)
            cur_node.value = val
            return

        def get_predecessor(cur_node):
            """Gets the predecessor node of the cur_node. Since this
            function is only called when we are removing a node with
            two children, we'll assume cur_node has two children
            
            parameters:
                cur_node - the node to get the predecessor of
            returns:
                the predessor node of cur_node"""

            #make sure cur_node has two children
            assert cur_node.left is not None and cur_node.right is not None, "cur_node must have two children"

            predecessor = cur_node.left
            if predecessor.right is None: #if the left child of cur_node has no right child, it is the predecessor
                return predecessor
            else: #iterate down to the right to get predecessor
                while predecessor.right is not None:
                    predecessor = predecessor.right
                return predecessor



        cur_node = self.find(data) #get the node to remove
        if cur_node.prev is None: #if the target is the root node
            if cur_node.left is None and cur_node.right is None: #if both children are None, set the root to None
                self.root = None
                return
            elif (cur_node.left is not None and cur_node.right is None) or (cur_node.left is None and cur_node.right is not None): #if the root has only one child
                if (cur_node.left is not None): #if the root has a left child
                    self.root = cur_node.left
                    self.root.prev = None #remove reference to old root
                    return
                else: #the root has a right child
                    self.root = cur_node.right
                    self.root.prev = None #remove reference to old root
                    return

        if (cur_node.left is None and cur_node.right is None): #if cur_node is a leaf node
            remove_leaf(cur_node)
        elif (cur_node.left is not None and cur_node.right is None) or (cur_node.left is None and cur_node.right is not None): #if cur_node has only one child
            remove_one(cur_node)
            return
        else: #cur_node has two children
            remove_two(cur_node)
            return





    def __str__(self):
        """String representation: a hierarchical view of the BST.

        Example:  (3)
                  / \     '[3]          The nodes of the BST are printed
                (2) (5)    [2, 5]       by depth levels. Edges and empty
                /   / \    [1, 4, 6]'   nodes are not printed.
              (1) (4) (6)
        """
        if self.root is None:                       # Empty tree
            return "[]"
        out, current_level = [], [self.root]        # Nonempty tree
        while current_level:
            next_level, values = [], []
            for node in current_level:
                values.append(node.value)
                for child in [node.left, node.right]:
                    if child is not None:
                        next_level.append(child)
            out.append(values)
            current_level = next_level
        return "\n".join([str(x) for x in out])

    def draw(self):
        """Use NetworkX and Matplotlib to visualize the tree."""
        if self.root is None:
            return

        # Build the directed graph.
        G = nx.DiGraph()
        G.add_node(self.root.value)
        nodes = [self.root]
        while nodes:
            current = nodes.pop(0)
            for child in [current.left, current.right]:
                if child is not None:
                    G.add_edge(current.value, child.value)
                    nodes.append(child)

        # Plot the graph. This requires graphviz_layout (pygraphviz).
        nx.draw(G, pos=graphviz_layout(G, prog="dot"), arrows=True,
                with_labels=True, node_color="C1", font_size=8)
        plt.show()


class AVL(BST):
    """Adelson-Velsky Landis binary search tree data structure class.
    Rebalances after insertion when needed.
    """
    def insert(self, data):
        """Insert a node containing the data into the tree, then rebalance."""
        BST.insert(self, data)      # Insert the data like usual.
        n = self.find(data)
        while n:                    # Rebalance from the bottom up.
            n = self._rebalance(n).prev

    def remove(*args, **kwargs):
        """Disable remove() to keep the tree in balance."""
        raise NotImplementedError("remove() is disabled for this class")

    def _rebalance(self,n):
        """Rebalance the subtree starting at the specified node."""
        balance = AVL._balance_factor(n)
        if balance == -2:                                   # Left heavy
            if AVL._height(n.left.left) > AVL._height(n.left.right):
                n = self._rotate_left_left(n)                   # Left Left
            else:
                n = self._rotate_left_right(n)                  # Left Right
        elif balance == 2:                                  # Right heavy
            if AVL._height(n.right.right) > AVL._height(n.right.left):
                n = self._rotate_right_right(n)                 # Right Right
            else:
                n = self._rotate_right_left(n)                  # Right Left
        return n

    @staticmethod
    def _height(current):
        """Calculate the height of a given node by descending recursively until
        there are no further child nodes. Return the number of children in the
        longest chain down.
                                    node | height
        Example:  (c)                  a | 0
                  / \                  b | 1
                (b) (f)                c | 3
                /   / \                d | 1
              (a) (d) (g)              e | 0
                    \                  f | 2
                    (e)                g | 0
        """
        if current is None:     # Base case: the end of a branch.
            return -1           # Otherwise, descend down both branches.
        return 1 + max(AVL._height(current.right), AVL._height(current.left))

    @staticmethod
    def _balance_factor(n):
        return AVL._height(n.right) - AVL._height(n.left)

    def _rotate_left_left(self, n):
        temp = n.left
        n.left = temp.right
        if temp.right:
            temp.right.prev = n
        temp.right = n
        temp.prev = n.prev
        n.prev = temp
        if temp.prev:
            if temp.prev.value > temp.value:
                temp.prev.left = temp
            else:
                temp.prev.right = temp
        if n is self.root:
            self.root = temp
        return temp

    def _rotate_right_right(self, n):
        temp = n.right
        n.right = temp.left
        if temp.left:
            temp.left.prev = n
        temp.left = n
        temp.prev = n.prev
        n.prev = temp
        if temp.prev:
            if temp.prev.value > temp.value:
                temp.prev.left = temp
            else:
                temp.prev.right = temp
        if n is self.root:
            self.root = temp
        return temp

    def _rotate_left_right(self, n):
        temp1 = n.left
        temp2 = temp1.right
        temp1.right = temp2.left
        if temp2.left:
            temp2.left.prev = temp1
        temp2.prev = n
        temp2.left = temp1
        temp1.prev = temp2
        n.left = temp2
        return self._rotate_left_left(n)

    def _rotate_right_left(self, n):
        temp1 = n.right
        temp2 = temp1.left
        temp1.left = temp2.right
        if temp2.right:
            temp2.right.prev = temp1
        temp2.prev = n
        temp2.right = temp1
        temp1.prev = temp2
        n.right = temp2
        return self._rotate_right_right(n)


# Problem 4
def prob4():
    """Compare the build and search times of the SinglyLinkedList, BST, and
    AVL classes. For search times, use SinglyLinkedList.iterative_find(),
    BST.find(), and AVL.find() to search for 5 random elements in each
    structure. Plot the number of elements in the structure versus the build
    and search times. Use log scales where appropriate.
    """

    #read english.txt file into a list
    file = open("english.txt", "r")
    words = []
    for line in file:
        words.append(line)

    #lists for matplotlib
    ll_build_times = []
    bst_build_times = []
    avl_build_times = []
    ll_find_times = []
    bst_find_times = []
    avl_find_times = []
    ns = []

    for i in range(3, 11):
        n = 2**i
        ns.append(n)
        sub_set = random.sample(set(words), n)

        #time LinkedList insertion
        ll = SinglyLinkedList()
        tick = time.clock()
        for item in sub_set:
            ll.append(item)
        tock = time.clock()
        ll_build_times.append(tock-tick)

        #time BST insertion
        bst = BST()
        tick = time.clock()
        for item in sub_set:
            bst.insert(item)
        tock = time.clock()
        bst_build_times.append(tock-tick)

        #time AVL insertion
        avl = AVL()
        tick = time.clock()
        for item in sub_set:
            avl.insert(item)
        tock = time.clock()
        avl_build_times.append(tock-tick)

        #get 5 random items from subset
        items_to_find = random.sample(set(sub_set), 5)

        #time linked list find
        tick = time.clock()
        for item in items_to_find:
            ll.iterative_find(item)
        tock = time.clock()
        ll_find_times.append(tock-tick)

        #time bst find
        tick = time.clock()
        for item in items_to_find:
            bst.find(item)
        tock = time.clock()
        bst_find_times.append(tock-tick)

        #time avl find
        tick = time.clock()
        for item in items_to_find:
            avl.find(item)
        tock = time.clock()
        avl_find_times.append(tock-tick)

    #create graphs
    ax1 = plt.subplot(121)
    ax1.plot(ns, ll_build_times, "b", label="LL insert times")
    ax1.plot(ns, bst_build_times, "g", label="BST insert times")
    ax1.plot(ns, avl_build_times, "r", label="AVL insert times")
    plt.xscale("log", basex=2)
    plt.yscale("log", basey=10)
    plt.title("Insert times")
    plt.xlabel("input size n")
    plt.ylabel("time")
    plt.legend(loc="upper left")

    ax2 = plt.subplot(122)
    ax2.plot(ns, ll_find_times, "b", label="LL find times")
    ax2.plot(ns, bst_find_times, "g", label="BST find times")
    ax2.plot(ns, avl_find_times, "r", label="AVL find times")
    plt.xscale("log",  basex=2)
    plt.yscale("log",  basey=10)
    plt.title("Find times ")
    plt.xlabel("input size n")
    plt.ylabel("time")
    plt.legend(loc="upper left")
    plt.tight_layout()
    plt.show()
    return

def test_ll():
    """Function to test linked-list class functionality"""
    ll = SinglyLinkedList()

    try:
        ll.recursive_find(11)
    except ValueError:
        pass

    size = 10
    print("Adding values from 0 to {}".format(size-1))
    for i in range(size):
        ll.append(i)

    print("Current list: ", end=" ")
    cur_node = ll.head
    while cur_node is not None:
        print(cur_node.value, end=" ")
        cur_node = cur_node.next

    print("\nFinding all nodes in the list...")
    for i in range(size):
        ll.recursive_find(i)
    print("Successfully found all nodes")

    try:
        ll.recursive_find(11)
    except ValueError:
        pass

def test_BST():
    """Function to test BST class functionality"""
    #create BST object
    bst = BST()

    #insert elements
    print("Inserting elements")
    for i in [4, 3, 6, 5, 7, 8, 1]:
        bst.insert(i)
    print(str(bst) + "\n")

    #remove a leaf
    print("Removing the leaf 8")
    bst.remove(8)
    print(str(bst) + "\n")

    #remove a node with one child
    print("Removing the node 3")
    bst.remove(3)
    print(str(bst) + "\n")

    #remove the root node
    print("Removing the root")
    bst.remove(4)
    print(str(bst) + "\n")

    #remove a value not in the tree
    try:
        print("Removing the value 2")
        bst.remove(2)
    except ValueError:
        pass
    print(str(bst) + "\n")

    #remove all remaining nodes
    print("Removing all remaining nodes")
    while(bst.root is not None):
        val = bst.root.value
        bst.remove(val)
        print(str(bst) + "\n")

    #Try to remove from an empty tree
    print("Remove from an empty tree")
    try:
        bst.remove(1)
    except ValueError:
        pass
    print(str(bst) + "\n")

    #more extensive insertions finds and removals
    #create sets
    test_size = 1000
    temp_set = set(range(test_size))
    temp_set_copy1 = copy.deepcopy(temp_set)
    temp_set_copy2 = copy.deepcopy(temp_set)

    #insert set into tree
    print("Randomly inserting all values between 0 and {}".format(test_size))
    for i in range(test_size):
        val = random.choice(list(temp_set))
        bst.insert(val)
        temp_set.remove(val)  
    print(str(bst) + "\n")

    #find all values
    print("Randomly find all nodes in the tree...")
    for i in range(test_size):
        val = random.choice(list(temp_set_copy1))
        bst.find(val)
        temp_set_copy1.remove(val)
    print("Successfully found all nodes\n")

    #remove all remaining nodes
    print("Randomly removing all remaining nodes")
    for i in range(test_size):
        val = random.choice(list(temp_set_copy2))
        bst.remove(val)
        temp_set_copy2.remove(val)
    print(str(bst) + "\n")
    return



if __name__ == "__main__":
    #test_BST()
    test_ll()
    #prob4()