# exceptions_fileIO.py
"""Python Essentials: Exceptions and File Input/Output.
Spencer Reschke
Math 321
9/19/2018
"""

from random import choice
import traceback


# Problem 1
def arithmagic():
    """Performs the arithmatic "magic" trick described on pg. 86 of Execptions lab.
    Throws exceptions for invalid user input"""

    #Get user input
    step_1 = input("Enter a 3-digit number where the first and last "
                                           "digits differ by 2 or more: ")
    if len(step_1) != 3:
        raise ValueError("step_1 requires a 3-digit number. Input was {}".format(step_1))
    if abs(int(step_1[0]) - int(step_1[-1])) < 2:
        raise ValueError("step_1 requires the first and last digit to differ by at least 2. Input was {}".format(step_1))
           
    step_2 = input("Enter the reverse of the first number, obtained "
                                              "by reading it backwards: ")
    if step_2[::-1] != step_1:
        raise ValueError("step_2 must be the reverse of step_1. Input was {}".format(step_2))

    step_3 = input("Enter the positive difference of these numbers: ")
    
    if (int(step_3) != abs(int(step_1)-int(step_2))):
        raise ValueError("step_3 must be the positive difference between step_1 and step_2. Input was {}".format(step_3))

    step_4 = input("Enter the reverse of the previous result: ")

    if step_4[::-1] != step_3:
        raise ValueError("step_4 must be the reverse of step_3. Input was {}".format(step_4))
    
    print(str(step_3), "+", str(step_4), "= 1089 (ta-da!)")


# Problem 2
def random_walk(max_iters=1e12):
    """Performs a random walk for max_iters iterations unless stopped by a keyboard
    interrupt exception"""
    walk = 0 #an accumulator for the random walk
    directions = [1, -1] #forward or backward
    try:
        for i in range(int(max_iters)):
            walk += choice(directions)
    except KeyboardInterrupt:
        print("Process interrupted at iteration {}".format(i))
    else:
        print("Process completed")
    finally:
        return walk


# Problems 3 and 4: Write a 'ContentFilter' class.
class ContentFilter:
    def __init__(self, file_name=None):
        """ContentFilter constructor
        @param file_name : a string specifying the file to open"""

        self.contents = "" #keeps track of contents of file
        self.name = "" #name of input file
        try:
            if type(file_name) != str:
                raise TypeError("file_name must be a valid file name")
            my_file = open(file_name)
        except (FileNotFoundError, TypeError, OSError):
            while True:
                try:
                    file_name = input("Please enter a valid file name: ")
                    my_file = open(file_name)
                except FileNotFoundError or TypeError or OSError:
                    continue
                else:
                    break
        self.contents = my_file.read()
        self.name = file_name

        if len(self.contents) != 0:
            while self.contents[-1].isspace():
                self.contents = self.contents[:-1]

    def uniform(self, file_name=None, mode='w', case='upper'):
        """Writes contents of ContentFilter to the file file_name in mode mode with
       style described by case.
        @param file_name : the name of file to write to.
        @param mode : a single letter string. Must be either 'w' (write), 'x' (write new), or 'a' (append).
        @param case : a string. Must be either 'upper' or lower"""
        if type(file_name) != str: #if file-name isn't a string, raise a TypeError
            raise TypeError("file_name must be a valid file name")
        if mode not in ['w', 'x', 'a']:
            raise ValueError("mode must be 'w', 'x', or 'a'. Value of mode was {}.".format(mode))
        if case not in ['upper', 'lower']:
            raise ValueError("case must be 'upper' or 'lower'. Value of case was {}.".format(case))
        my_file = open(file_name, mode)
        
        lines = self.contents.split("\n")
        for line in lines:
            if case == "upper":
                my_file.write(line.upper() + "\n")
            else:
                my_file.write(line.lower() + "\n")
        my_file.close

    def reverse(self, file_name=None, mode='w', unit='line'):
        """Writes contents of ContentFilter to the file file_name in mode mode with
       style described by unit. If unit='line' reverses the file by lines. If
       unit='word', reverses the file by word.
        @param file_name : the name of file to write to.
        @param mode : a single letter string. Must be either 'w' (write), 'x' (write new), or 'a' (append).
        @param unit : a string. Must be either 'line' or 'word'."""
        
        if type(file_name) != str: #if file-name isn't a string, raise a TypeError
            raise TypeError("file_name must be a valid file name")
        if mode not in ['w', 'x', 'a']:
            raise ValueError("mode must be 'w', 'x', or 'a'. Value of mode was {}.".format(mode))
        if unit not in ['line', 'word']:
            raise ValueError("unit must be 'line' or 'word'. Value of case was {}.".format(unit))
        my_file = open(file_name, mode)
        
        lines = self.contents.split("\n")
        if unit == "word":
            for line in lines:                
                my_file.write(line[::-1] + "\n")
        elif unit == "line":
            for line in reversed(lines):
                my_file.write(line + "\n")
        else:
            try:
                raise ValueError("unit wasn't 'word' or 'line'. Check exception handling in reverse().")
            except ValueError as e:
                traceback.print_stack()
                print(e)

    def transpose(self, file_name, mode='w'):
        """Writes contents of ContentFilter to the file file_name but with the
        contents transposed (e.g. the second word in the first row becomes the 
        first word in the second row and so forth).
        @param file_name : the name of file to write to.
        @param mode : a single letter string. Must be either 'w' (write), 'x' (write new), or 'a' (append)."""
        
        if type(file_name) != str: #if file-name isn't a string, raise a TypeError
            raise TypeError("file_name must be a valid file name")
        if mode not in ['w', 'x', 'a']:
            raise ValueError("mode must be 'w', 'x', or 'a'. Value of mode was {}.".format(mode))
        my_file = open(file_name, mode)
        
        lines = self.contents.split("\n")
        num_columns = len(lines)
        num_rows = len(lines[-1].split(" "))
        for i in range(num_rows):
            for j in range(num_columns):
                my_file.write(lines[j].split(" ")[i] + " ")
            my_file.write("\n")
        my_file.close()   
          
    def __str__(self):
        """Overrides string method for ContentFilter"""
        #this is a comment
        num_chars = len(self.contents)
        num_alpha_chars = sum([c.isalpha() for c in self.contents])
        num_num_chars = sum([c.isdigit() for c in self.contents])
        num_ws_chars = sum([c.isspace() for c in self.contents])
        num_lines = sum(map(lambda c: c == "\n", self.contents)) + 1

        #accumulate results
        results = "Source file:\t\t{}\n".format(self.name)
        results += "Total characters:\t{}\n".format(num_chars)
        results += "Alphabetic characters:\t{}\n".format(num_alpha_chars)
        results += "Numerical characters:\t{}\n".format(num_num_chars)
        results += "Whitespace characters:\t{}\n".format(num_ws_chars)
        results += "Number of lines:\t{}\n".format(num_lines)

        return results

if __name__ == "__main__":
    #arithmagic()
    #print(random_walk(max_iters=1e7))
    in_file_name = "test.txt"
    #out_file_name = in_file_name.split(".")[0] + "_out.txt"
    out_file_name = "_OUT_"
    cf = ContentFilter(in_file_name)
    cf.uniform(out_file_name, mode="z")
    #cf.reverse(out_file_name, "z")
    #cf.transpose(out_file_name, "z")
    #cf.uniform()
    #cf.reverse()
    #print(str(cf))
