# nearest_neighbor.py
"""Volume 2: Nearest Neighbor Search.
Spencer Reschke
Math 321
10/24/2018
"""

import numpy as np
from scipy import linalg as la
from scipy.spatial import KDTree
from scipy.stats import mode


# Problem 1
def exhaustive_search(X, z):
    """Solve the nearest neighbor search problem with an exhaustive search.

    Parameters:
        X ((m,k) ndarray): a training set of m k-dimensional points.
        z ((k, ) ndarray): a k-dimensional target point.

    Returns:
        ((k,) ndarray) the element (row) of X that is nearest to z.
        (float) The Euclidean distance from the nearest neighbor to z.
    """
    #calucluate distances between all x in X and z
    norms = la.norm(X-z, axis=1)
    #get index of nearest neighbor
    best_index = np.argmin(norms)
    #get nearest neighbor
    nn = X[best_index]
    return nn, norms[best_index]


# Problem 2: Write a KDTNode class.
class KDTNode:
    def __init__(self, x):
        """Constructor for the KDTNode class
        
        Parameters:
            x ((k, ) ndarray): a k-dimensional vector."""

        if type(x) != np.ndarray:
            raise TypeError("x must be a numpy array")

        self.value = np.copy(x)
        self.left = None
        self.right = None
        self.pivot = None

# Problems 3 and 4
class KDT:
    """A k-dimensional binary tree for solving the nearest neighbor problem.

    Attributes:
        root (KDTNode): the root node of the tree. Like all other nodes in
            the tree, the root has a NumPy array of shape (k,) as its value.
        k (int): the dimension of the data in the tree.
    """
    def __init__(self):
        """Initialize the root and k attributes."""
        self.root = None
        self.k = None

    def find(self, data):
        """Return the node containing the data. If there is no such node in
        the tree, or if the tree is empty, raise a ValueError.
        """
        def _step(current):
            """Recursively step through the tree until finding the node
            containing the data. If there is no such node, raise a ValueError.
            """
            if current is None:                     # Base case 1: dead end.
                raise ValueError(str(data) + " is not in the tree")
            elif np.allclose(data, current.value):
                return current                      # Base case 2: data found!
            elif data[current.pivot] < current.value[current.pivot]:
                return _step(current.left)          # Recursively search left.
            else:
                return _step(current.right)         # Recursively search right.

        # Start the recursive search at the root of the tree.
        return _step(self.root)

    # Problem 3
    def insert(self, data):
        """Insert a new node containing the specified data.

        Parameters:
            data ((k,) ndarray): a k-dimensional point to insert into the tree.

        Raises:
            ValueError: if data does not have the same dimensions as other
                values in the tree.
        """

        def _step(self, current):
            """Recursively step through the tree until finding the node
            containing the data. If there is no such node, raise a ValueError.
            """
            if data[current.pivot] < current.value[current.pivot]: #check left subtree
                if current.left is None: #found place to insert
                    current.left = KDTNode(data)
                    current.left.pivot = (current.pivot + 1) % self.k
                    return
                elif np.allclose(data, current.left.value): #data already in tree
                    raise ValueError("{} already in the tree".format(data))
                else: #call recursive function on left subtree
                    _step(self, current.left)
            else:
                if current.right is None: #found place to insert
                    current.right = KDTNode(data)
                    current.right.pivot = (current.pivot + 1) % self.k
                    return
                elif np.allclose(data, current.right.value): #data already in tree
                    raise ValueError("{} already in the tree".format(data))
                else: #call rescursive function on right subtree
                    _step(self, current.right)

        #make sure data is a numpy array
        if type(data) != np.ndarray:
            raise TypeError("data must be a numpy array")

        #get dimension of data
        dim = data.shape[0]

        #if tree is empty, create a new root
        if self.root is None:
            self.root = KDTNode(data)
            self.root.pivot = 0
            self.k = dim
            return

        #ensure dimension of data is the same as other values in the tree
        if dim != self.k:
            raise ValueError("data must have dimension of {}".self.k)

        #call recursive function on root)
        _step(self, self.root)
        return

    # Problem 4
    def query(self, z):
        """Find the value in the tree that is nearest to z.

        Parameters:
            z ((k,) ndarray): a k-dimensional target point.

        Returns:
            ((k,) ndarray) the value in the tree that is nearest to z.
            (float) The Euclidean distance from the nearest neighbor to z.
        """
        #recursive function 
        def KDSearch(current, nearest, d_star):
            """Helper function used to find node of nearest neighbor and 
            smallest distance"""
            if current is None:
                return nearest, d_star

            #get current value, pivot and distance to target
            x = current.value
            i = current.pivot
            d = la.norm(x-z)
            if d < d_star:
                nearest = current
                d_star = d
            if z[i] < x[i]: #search to the left
                nearest, d_star = KDSearch(current.left, nearest, d_star)
                if (z[i] + d_star) >= x[i]: #search to the right if needed
                    nearest, d_star = KDSearch(current.right, nearest, d_star)
            else: #search to the right
                nearest, d_star = KDSearch(current.right, nearest, d_star)
                if (z[i] - d_star) <= x[i]: #search to the left if needed
                    nearest, d_star = KDSearch(current.left, nearest, d_star)
            return nearest, d_star
        
        #get distance from root to target
        d_star = la.norm(self.root.value-z)

        #begin recursion with root node
        node, d_star = KDSearch(self.root, self.root, d_star)
        return node.value, d_star

    def __str__(self):
        """String representation: a hierarchical list of nodes and their axes.

        Example:                           'KDT(k=2)
                    [5,5]                   [5 5]   pivot = 0
                    /   \                   [3 2]   pivot = 1
                [3,2]   [8,4]               [8 4]   pivot = 1
                    \       \               [2 6]   pivot = 0
                    [2,6]   [7,5]           [7 5]   pivot = 0'
        """
        if self.root is None:
            return "Empty KDT"
        nodes, strs = [self.root], []
        while nodes:
            current = nodes.pop(0)
            strs.append("{}\tpivot = {}".format(current.value, current.pivot))
            for child in [current.left, current.right]:
                if child:
                    nodes.append(child)
        return "KDT(k={})\n".format(self.k) + "\n".join(strs)


# Problem 5: Write a KNeighborsClassifier class.
class KNeighborsClassifier():
    def __init__(self, n_neighbors):
        """Constructor
        
        Parameters:
            n_neighbors int: the number of neighbors to include while classifying
            """

        #assign attributes
        self.n_neighbors = n_neighbors
        self.tree = None
        self.labels = None
    
    def fit(self, X, y):
        """Load X into a SciPy KDTree and save the labels in y
        
        Parameters:
            X ((m,k) ndarray): An mxk numpy array containing the training set
            y ((m, ) ndarray): The labels for the dataset"""


        #Check parameter types are correct
        if type(X) != np.ndarray:
            raise TypeError("X must be a numpy array")
        if type(y) != np.ndarray:
            raise TypeError("y must be a numpy array")

        #assign attributes
        self.tree = KDTree(X)
        self.labels = y
        return

    def predict(self, z):
        """Finds the label for z using the mode of the it's n_neighbors 
        nerarest neighbors and assigning. If there is a tie, it chooses
        the alphanumerically smallest label.
        
        Parameters:
            z ((k, ) ndarray: A k-dimensional target array
            
        Returns:
            The predicted label of the target z.
            """
        if type(z) != np.ndarray:
            raise TypeError("z must be a numpy array")

        #get indices of nearest neighbors
        _, indices = self.tree.query(z, self.n_neighbors)

        #get labels of nearest neightbors
        labels = self.labels[indices]

        #return the mode
        return mode(labels)[0][0]
    


# Problem 6
def prob6(n_neighbors, filename="mnist_subset.npz"):
    """Extract the data from the given file. Load a KNeighborsClassifier with
    the training data and the corresponding labels. Use the classifier to
    predict labels for the test data. Return the classification accuracy, the
    percentage of predictions that match the test labels.

    Parameters:
        n_neighbors (int): the number of neighbors to use for classification.
        filename (str): the name of the data file. Should be an npz file with
            keys 'X_train', 'y_train', 'X_test', and 'y_test'.

    Returns:
        (float): the classification accuracy.
    """

    #load the train and test data
    data = np.load(filename)
    X_train = data["X_train"].astype(np.float)
    y_train = data["y_train"]
    X_test = data["X_test"].astype(np.float)
    y_test = data["y_test"]

    #construct classifier
    KNC = KNeighborsClassifier(n_neighbors)

    #fit the classifier
    KNC.fit(X_train, y_train)

    #get predictions
    predictions = []
    for x in X_test: #If have time, figure out how to eliminate this for loop
        predictions.append(KNC.predict(x))

    #cast predictions to numpy array
    predictions = np.array(predictions)

    error = np.mean(predictions != y_test)
    return 1 - error

def test_KDT():
    #create instance
    kdt = KDT()
    
    #insert root
    print("Insertint root")
    kdt.insert(np.array([5, 5]))
    print(kdt)
    print()

    #fill next two levels
    print("Fill next two levels")
    kdt.insert(np.array([3, 2]))
    kdt.insert(np.array([8, 4]))
    kdt.insert(np.array([2, 6]))
    kdt.insert(np.array([7, 5]))
    kdt.insert(np.array([1, 1]))
    kdt.insert(np.array([6, 3]))
    print(kdt)
    print()

    #insert value already in tree
    print("Try to insert [6, 3]")
    try:
        kdt.insert(np.array([6, 3]))
    except ValueError:
        pass
    print("Successfully handled")

    #Insert with wrong data type
    print("Try to insert [1, 1] as a list")
    try:
        kdt.insert([1, 1])
    except TypeError:
        pass
    print("Successfully handled")
    print()

    #reconstruct tree in lab example (pg. 35)
    print("Clear and rebuild tree")
    kdt = KDT()
    kdt.insert(np.array([5, 5]))
    kdt.insert(np.array([3, 2]))
    kdt.insert(np.array([8, 4]))
    kdt.insert(np.array([2, 6]))
    kdt.insert(np.array([7, 7]))
    print(kdt)
    print()

    #find nearest neighbor of [3, 2.75]
    print("Query [3, 2.75]")
    target = np.array([3, 2.75])
    nearest, d_star = kdt.query(target)
    assert (np.allclose(nearest, np.array([3, 2])) and d_star == 0.75)
    print("Successfully found nearest neighbor")


def test_KNeighborsClassifier():
    #create instance
    KNC = KNeighborsClassifier(20)

    #test fit
    m = 100
    k = 5
    num_classes = 2
    X = np.random.random((m, k))
    y = np.random.randint(0, num_classes, m)
    KNC.fit(X, y)

    #test predict
    z = np.random.random(k)
    print("Prediction for {}: {}".format(z, KNC.predict(z)))

if __name__ == "__main__":
    ##problem 1
    #X = np.array([[1, 2, 3], [10, 1, 1], [10, 1, 1]])
    #z = np.array([1, 1, 1])
    #print(exhaustive_search(X, z))

    #problems 2, 3 and 4
    #test_KDT()

    ##problem 5
    #test_KNeighborsClassifier()

    #problem 6
    print(prob6(4))

