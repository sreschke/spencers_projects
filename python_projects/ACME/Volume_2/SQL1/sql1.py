    # sql1.py
"""Volume 3: SQL 1 (Introduction).
Spencer Reschke
Math 321
11/26/18
"""

import sqlite3 as sql
import csv
import numpy as np
import matplotlib.pyplot as plt


# Problems 1, 2, and 4
def student_db(db_file="students.db", student_info="student_info.csv",
                                      student_grades="student_grades.csv"):
    """Connect to the database db_file (or create it if it doesn’t exist).
    Drop the tables MajorInfo, CourseInfo, StudentInfo, and StudentGrades from
    the database (if they exist). Recreate the following (empty) tables in the
    database with the specified columns.

        - MajorInfo: MajorID (integers) and MajorName (strings).
        - CourseInfo: CourseID (integers) and CourseName (strings).
        - StudentInfo: StudentID (integers), StudentName (strings), and
            MajorID (integers).
        - StudentGrades: StudentID (integers), CourseID (integers), and
            Grade (strings).

    Next, populate the new tables with the following data and the data in
    the specified 'student_info' 'student_grades' files.

                MajorInfo                         CourseInfo
            MajorID | MajorName               CourseID | CourseName
            -------------------               ---------------------
                1   | Math                        1    | Calculus
                2   | Science                     2    | English
                3   | Writing                     3    | Pottery
                4   | Art                         4    | History

    Finally, in the StudentInfo table, replace values of −1 in the MajorID
    column with NULL values.

    Parameters:
        db_file (str): The name of the database file.
        student_info (str): The name of a csv file containing data for the
            StudentInfo table.
        student_grades (str): The name of a csv file containing data for the
            StudentGrades table.
    """
    try:
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            #Drop MajorInfo, CourseInfo, StudentInfo and StudentGrades tables if they exist
            cur.execute("DROP TABLE IF EXISTS MajorInfo")
            cur.execute("DROP TABLE IF EXISTS CourseInfo")
            cur.execute("DROP TABLE IF EXISTS StudentInfo")
            cur.execute("DROP TABLE IF EXISTS StudentGrades")
            cur.execute("DROP TABLE IF EXISTS StudentID")
            
            #Create Tables
            cur.execute("CREATE TABLE MajorInfo (MajorID INTEGER, MajorName TEXT)")
            cur.execute("CREATE TABLE CourseInfo (CourseID INTEGER, CourseName TEXT)")
            cur.execute("CREATE TABLE StudentInfo (StudentID INTEGER, StudentName TEXT, MajorID INTEGER)")
            cur.execute("CREATE TABLE StudentGrades (StudentID INTEGER, CourseID INTEGER, Grade TEXT)")
            
            #Row insertions
            mi_rows = [(1, "Math"), (2, "Science"), (3, "Writing"), (4, "Art")]
            ci_rows = [(1, "Calculus"), (2, "English"), (3, "Pottery"), (4, "History")]
            
            cur.executemany("INSERT INTO MajorInfo VALUES(?, ?);", mi_rows)
            cur.executemany("INSERT INTO CourseInfo VALUES(?, ?);", ci_rows)
            
            with open("student_info.csv", 'r') as infile:
                si_rows = list(csv.reader(infile))
            cur.executemany("INSERT INTO StudentInfo VALUES(?, ?, ?);", si_rows)
            
            with open("student_grades.csv", 'r') as infile:
                sg_rows = list(csv.reader(infile))
            cur.executemany("INSERT INTO StudentGrades VALUES(?, ?, ?);", sg_rows)
            
            #Problem 4: Replace -1s in StudentInfo with NULLs
            cur.execute("UPDATE StudentInfo SET MajorID=NULL WHERE MajorID==-1;")
            
            
            
    finally:
        conn.close()


# Problems 3 and 4
def earthquakes_db(db_file="earthquakes.db", data_file="us_earthquakes.csv"):
    """Connect to the database db_file (or create it if it doesn’t exist).
    Drop the USEarthquakes table if it already exists, then create a new
    USEarthquakes table with schema
    (Year, Month, Day, Hour, Minute, Second, Latitude, Longitude, Magnitude).
    Populate the table with the data from 'data_file'.

    For the Minute, Hour, Second, and Day columns in the USEarthquakes table,
    change all zero values to NULL. These are values where the data originally
    was not provided.

    Parameters:
        db_file (str): The name of the database file.
        data_file (str): The name of a csv file containing data for the
            USEarthquakes table.
    """
    try:
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            #Drop USEarthquakes if it already exists
            cur.execute("DROP TABLE IF EXISTS USEarthquakes")
            
            #Create Tables
            cur.execute("CREATE TABLE USEarthquakes (Year INTEGER, Month INTEGER, Day INTEGER, Hour INTEGER, Minute INTEGER, Second INTEGER, Latitude REAL, Longitude REAL, Magnitude REAL)")
            
           	#Insert Rows from csv file            
            with open("us_earthquakes.csv", 'r') as infile:
                rows = list(csv.reader(infile))
            cur.executemany("INSERT INTO USEarthquakes VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", rows)
            
            #Remove rows
            cur.execute("DELETE FROM USEarthquakes WHERE Magnitude==0")
            
            #Replace 0s with NULL values
            cur.execute("UPDATE USEarthquakes SET Day=NULL WHERE Day==0")
            cur.execute("UPDATE USEarthquakes SET Hour=NULL WHERE Hour==0")
            cur.execute("UPDATE USEarthquakes SET Minute=NULL WHERE Minute==0")
            cur.execute("UPDATE USEarthquakes SET Second=NULL WHERE Second==0")
            
            
    finally:
        conn.close()



# Problem 5
def prob5(db_file="students.db"):
    """Query the database for all tuples of the form (StudentName, CourseName)
    where that student has an 'A' or 'A+'' grade in that course. Return the
    list of tuples.

    Parameters:
        db_file (str): the name of the database to connect to.

    Returns:
        (list): the complete result set for the query.
    """

    #Get all students with an A or A+ with their corresponding coursesg
    try:
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            
            cur.execute("SELECT SI.StudentName, CI.CourseName "
                        "FROM StudentInfo AS SI, CourseInfo as CI, StudentGrades AS SG "
                        "WHERE (SG.Grade == 'A' OR SG.Grade=='A+') AND CI.CourseID == SG.CourseID AND SG.StudentID == SI.StudentID")
            results = cur.fetchall()
            return results
    finally:
        conn.close()

# Problem 6
def prob6(db_file="earthquakes.db"):
    """Create a single figure with two subplots: a histogram of the magnitudes
    of the earthquakes from 1800-1900, and a histogram of the magnitudes of the
    earthquakes from 1900-2000. Also calculate and return the average magnitude
    of all of the earthquakes in the database.

    Parameters:
        db_file (str): the name of the database to connect to.

    Returns:
        (float): The average magnitude of all earthquakes in the database.
    """

    try:
        with sql.connect(db_file) as conn:
            cur = conn.cursor()
            
            #Select magnitudes for 19th century
            cur.execute("SELECT Magnitude FROM USEarthquakes WHERE Year >= 1800 AND Year <= 1899")
            magnitudes19 = np.array([t[0] for t in cur.fetchall()])
            
            #Select magnitudes for 20th century
            cur.execute("SELECT Magnitude FROM USEarthquakes WHERE Year >= 1900 AND Year <= 1999")
            magnitudes20 = np.array([t[0] for t in cur.fetchall()])

            #Get average magnitude of all earthquakes in the database
            cur.execute("SELECT AVG(Magnitude) FROM USEarthquakes")
            ave_magnitude = cur.fetchall()[0][0]
            
            plt.subplot(1, 2, 1)
            plt.hist(magnitudes19)
            plt.xlabel("Magnitudes in 19th century")

            plt.subplot(1, 2, 2)
            plt.hist(magnitudes20)
            plt.xlabel("Magnitudes in 20th century")

            plt.suptitle("Earthquakes")
            plt.show()

            return ave_magnitude
            
    finally:
        conn.close()
    
    
def test_student_db():
    student_db()
    with sql.connect("students.db") as conn:
    	cur = conn.cursor()
    	cur.execute("SELECT * FROM StudentInfo;")
    	print([d[0] for d in cur.description])
    	
    	
    with sql.connect("students.db") as conn:
        cur = conn.cursor()
        for row in cur.execute("SELECT * FROM MajorInfo;"):
            print(row)
    	
   
def test_earthquakes_db():
    earthquakes_db()
    with sql.connect("earthquakes.db") as conn:
        cur = conn.cursor()
        i = 0
        for row in cur.execute("SELECT * FROM USEarthquakes;"):
            if i < 10:
                print(row)
                i += 1
            else:
            	 break	


def test_prob5():
    results = prob5()

    assert len(results) == 7
    print("All tess passed for prob5")

def test_prob6():
    print(prob6())

    print("All tests passed for prob6")

if __name__ == "__main__":
    test_student_db()
    test_earthquakes_db()
    test_prob5()
    test_prob6()