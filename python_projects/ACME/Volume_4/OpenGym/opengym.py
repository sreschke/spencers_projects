# opengym.py
"""Volume 2: Open Gym
Spencer Reschke
Math 347
9/5/2019
"""

import gym
import numpy as np
from IPython.display import clear_output
import random
import matplotlib.pyplot as plt
import time

def find_qvalues(env,alpha=.1,gamma=.6,epsilon=.1):
    """
    Use the Q-learning algorithm to find qvalues.

    Parameters:
        env (str): environment name
        alpha (float): learning rate
        gamma (float): discount factor
        epsilon (float): maximum value

    Returns:
        q_table (ndarray nxm)
    """
    # Make environment
    env = gym.make(env)
    # Make Q-table
    q_table = np.zeros((env.observation_space.n,env.action_space.n))

    # Train
    for i in range(1,100001):
        # Reset state
        state = env.reset()

        epochs, penalties, reward, = 0,0,0
        done = False

        while not done:
            # Accept based on alpha
            if random.uniform(0,1) < epsilon:
                action = env.action_space.sample()
            else:
                action = np.argmax(q_table[state])

            # Take action
            next_state, reward, done, info = env.step(action)

            # Calculate new qvalue
            old_value = q_table[state,action]
            next_max = np.max(q_table[next_state])

            new_value = (1-alpha) * old_value + alpha * (reward + gamma * next_max)
            q_table[state, action] = new_value

            # Check if penalty is made
            if reward == -10:
                penalties += 1

            # Get next observation
            state = next_state
            epochs += 1

        # Print episode number    
        if i % 100 == 0:
            clear_output(wait=True)
            print("Episode: {}".format(i))

    print("Training finished.")
    return q_table

# Problem 1
def random_blackjack(n):
    """
    Play a random game of Blackjack. Determine the
    percentage the player wins out of n times.

    Parameters:
        n (int): number of iterations

    Returns:
        percent (float): percentage that the player
                         wins
    """
    #Make Black Jack environment
    b_jack = gym.make("Blackjack-v0")
    
    #Play game n times
    wins = 0
    for i in range(n):        
        #take inital action
        tup = b_jack.step(b_jack.action_space.sample())
        done = tup[2]
        
        #take random actions until game is over
        while not done:
            tup = b_jack.step(b_jack.action_space.sample())
            done = tup[2]
            
        #Check for win
        if tup[1] == 1.0:
            wins += 1
        
        #close environment
        b_jack.reset()
     
    b_jack.close()
    ave_wins = wins/n
    return ave_wins
            

# Problem 2
def blackjack(n=11):
    """
    Play blackjack with naive algorithm.
    Run 10000 times
    
    Parameters:
        n (int): maximum accepted player hand

    Return:
         average reward (float): average total reward over the 10000 runs
    """
    
    #Make black jack environment (i.e. get first hand)
    b_jack = gym.make("Blackjack-v0")
    
    #Play 10000 times
    summ = 0
    for i in range(10000):        
        #Check sum of first two cards
        smm = b_jack._get_obs()[0]
    
        #Take first action
        if smm <= n:
            #Hit
            tup = b_jack.step(1)
            
        else:
            #Stick
            tup = b_jack.step(0)
        
        smm = tup[0][0]
        while smm <= n:
            #keep playing
            tup = b_jack.step(1)
            smm = tup[0][0]
           
        #Get reward
        reward = tup[1]
        summ += reward
        
        #Reset environment
        b_jack.reset()
    
    #Close environment
    b_jack.close()
    
    #Calculate and return average
    return summ/10000   
    

# Problem 3
def cartpole():
    """
    Solve CartPole-v0 by checking the velocity
    of the tip of the pole

    Return:
        time (float): time cartpole is held vertical
    """
    #Get CartPole environment
    env = gym.make("CartPole-v0")
    
    try:
        state = env.reset()
        t_vel = state[-1]
        
        #Get initial action
        if t_vel >= 0:
            #push right
            action = 1
        else:
            #push left
            action = 0
        done = False
        #Start timer
        tick=time.time()
        while not done:
            env.render()
            state, reward, done, info = env.step(action)
            t_vel = state[-1]
            
            #Get next state
            if t_vel >= 0:
                #push right
                action = 1
            else:
                #push left
                action = 0
            
            if done:
                break
        #end timer    
        tock = time.time()
        return tock-tick
    finally:
        env.close()

# Problem 4
def car():
    """
    Solve MountainCar-v0 by checking the position
    of the car.

    Return:
        time (float): time to solve environment
    """
    #Get MountainCar environment
    env = gym.make("MountainCar-v0")
        
    #Initialize environmnet
    state = env.reset()
    pos=state[0]
    try:
        done = False
        i = 0
        #Start timer
        tick = time.time()
        #left
        while pos > -0.7 and not done:
            env.render()     
            action = 0
            state, reward, done, info = env.step(action)
            pos = state[0]
            i+=1
        
        #right
        while pos < -0.1  and not done:            
            env.render()     
            action = 2
            state, reward, done, info = env.step(action)
            pos = state[0]
            i+=1
        
        #left
        while pos > -1 and not done:            
            env.render()     
            action = 0
            state, reward, done, info = env.step(action)
            pos = state[0]
            i+=1
        
        #right
        while not done:            
            env.render()     
            action = 2
            state, reward, done, info = env.step(action)
            pos = state[0]
            i+=1
            
        #End timer
        tock = time.time()
        return tock-tick
            
    finally:
        env.close()
    

# Problem 5
def taxi(q_table):
    """
    Compare naive and q-learning algorithms.

    Parameters:
        q_table (ndarray nxm): table of qvalues

    Returns:
        naive (int): reward of naive algorithm
        q_reward (int): reward of Q-learning algorithm
    """
    NUM_TIMES = 10000
    
    #Get Taxi environment
    env = gym.make("Taxi-v2")
    
    #Act randomly
    sum1 = 0
    for i in range(NUM_TIMES):
        #reset environment
        env.reset()
        episode_rewards = 0
        
        #initial action
        action = env.action_space.sample()
        done = False
        while not done:
            state, reward, done, _ = env.step(action)
            episode_rewards += reward
            action = env.action_space.sample()
        sum1 += episode_rewards
        
    #Calculate average rewards for random actions
    ave_random_r = sum1/NUM_TIMES
    
    #Now use q_table
    sum2 = 0
    for i in range(NUM_TIMES):
        #reset environment
        state = env.reset()
        episode_rewards = 0
        
        #initial action
        action = np.argmax(q_table[state])
        done=False
        while not done:
            state, reward, done, _ = env.step(action)
            episode_rewards += reward
            action = np.argmax(q_table[state])
        sum2 += episode_rewards
            
    #Calculate average rewards for q_table
    q_table_r = sum2/NUM_TIMES
    
    return ave_random_r, q_table_r
                
    
    
def test_prob1():
    #Test output type
    ave_winnings = random_blackjack(10)
    assert type(ave_winnings) in [int, float]
    
    wins = []
    for n in range(1, 1000):
        wins.append(random_blackjack(n))
    
    plt.plot(wins)
    plt.ylabel("Average wins")
    plt.xlabel("n")
    plt.show()
    print("All tests passed for problem 1")


def test_prob2():
    ns = np.arange(1, 21)
    average_rewards = []
    for n in ns:
        average_rewards.append(blackjack(n))
        
    plt.plot(ns, average_rewards)
    plt.xticks(ticks=ns)
    plt.xlabel("n")
    plt.ylabel("Average Reward")
    plt.title("Black Jack Results")
    plt.show()
    
def test_prob3():
    time_up = cartpole()
    print("The pole was balanced for {:.2f} seconds".format(time_up))
    
    
def test_prob4():
    time_up = car()
    print("The problem was solve in {:.2f} seconds".format(time_up))
    
    
def test_prob5():
    q_table = find_qvalues("Taxi-v2")
    
    naive_score, q_score = taxi(q_table)
    print(naive_score)
    print(q_score)
     
    
if __name__ == "__main__":
    #test_prob1()
    test_prob2()
    test_prob3()
    test_prob4()
    test_prob5()
