# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton


def EmbeddingAlg(param_list, guess, F):
    X = []
    for param in param_list:
        try:
            # Solve for x_value making F(x_value, param) = 0.
            x_value = newton(F, guess, fprime=None, args=(param,), tol=1E-7, maxiter=50)
            # Record the solution and update guess for the next iteration.
            X.append(x_value)
            guess = x_value
        except RuntimeError:
        # If Newton's method fails, return a truncated list of parameters
        # with the corresponding x values.
            return param_list[:len(X)], X
    # Return the list of parameters and the corresponding x values.
    return param_list, X

#def F(x, lmbda):
#    return x**2 + lmbda


## Top curve shown in the bifurcation diagram
#C1, X1 = EmbeddingAlg(np.linspace(-5, 0, 200), np.sqrt(5), F)
## The bottom curve
#C2, X2 = EmbeddingAlg(np.linspace(-5, 0, 200), -np.sqrt(5), F)

#Problem 1
def prob1():
    F = lambda x, lmbda: lmbda*x - x**3
    C1, X1 = EmbeddingAlg(np.linspace(-5, 5, 200), 0, F) #Middle line
    C2, X2 = EmbeddingAlg(np.linspace(5, 0, 100), 1.5, F) #Top curve
    C3, X3 = EmbeddingAlg(np.linspace(5, 0, 100), -1.5, F) #Bottome curve
    
    plt.plot(C1, X1, color="Green")
    plt.plot(C2, X2, color="Blue")
    plt.plot(C3, X3, color="Orange")
    plt.title(r"$\dotx = \lambda x + x^3$")
    plt.xlabel(r"$\lambda$")
    plt.show()
    
    
def prob2():
    n=-1
    F = lambda x, lmbda: n + lmbda*x - x**3
    C1, X1 = EmbeddingAlg(np.linspace(-5, 5, 200), -1.5, F)
    C2, X2 = EmbeddingAlg(np.linspace(1.9, 5, 200), 0, F)
    C3, X3 = EmbeddingAlg(np.linspace(1.9, 5, 200), 2, F)
    
    plt.plot(C1, X1, color="Green")
    plt.plot(C2, X2, color="Blue")
    plt.plot(C3, X3, color="Orange")
    plt.title(r"$\dotx = {:.1f} + \lambda x + x^3$".format(n))
    plt.xlabel(r"$\lambda$")
    plt.show()
    
    n=-.2
    F = lambda x, lmbda: n + lmbda*x - x**3
    C1, X1 = EmbeddingAlg(np.linspace(-5, 5, 200), -1, F)
    C2, X2 = EmbeddingAlg(np.linspace(0.65, 5, 200), 0.2, F)
    C3, X3 = EmbeddingAlg(np.linspace(0.65, 5, 200), 1, F)
    
    plt.plot(C1, X1, color="Green")
    plt.plot(C2, X2, color="Blue")
    plt.plot(C3, X3, color="Orange")
    plt.title(r"$\dotx = {:.1f} + \lambda x + x^3$".format(n))
    plt.xlabel(r"$\lambda$")
    plt.show()
    
    n=.2
    F = lambda x, lmbda: n + lmbda*x - x**3
    C1, X1 = EmbeddingAlg(np.linspace(-5, 5, 200), 2.5, F)
    C2, X2 = EmbeddingAlg(np.linspace(0.65, 5, 200), 0, F)
    C3, X3 = EmbeddingAlg(np.linspace(0.65, 5, 200), -3, F)
    
    plt.plot(C1, X1, color="Green")
    plt.plot(C2, X2, color="Blue")
    plt.plot(C3, X3, color="Orange")
    plt.title(r"$\dotx = {:.1f} + \lambda x + x^3$".format(n))
    plt.xlabel(r"$\lambda$")
    plt.show()
    
    n=1
    F = lambda x, lmbda: n + lmbda*x - x**3
    C1, X1 = EmbeddingAlg(np.linspace(-5, 5, 200), 2.5, F)
    C2, X2 = EmbeddingAlg(np.linspace(1.9, 5, 200), 0, F)
    C3, X3 = EmbeddingAlg(np.linspace(1.9, 5, 200), -2.5, F)
    
    plt.plot(C1, X1, color="Green")
    plt.plot(C2, X2, color="Blue")
    plt.plot(C3, X3, color="Orange")
    plt.title(r"$\dotx = {:.1f} + \lambda x + x^3$".format(n))
    plt.xlabel(r"$\lambda$")
    plt.show()
    
    
    
if __name__ == "__main__":
    #prob1()
    prob2()


