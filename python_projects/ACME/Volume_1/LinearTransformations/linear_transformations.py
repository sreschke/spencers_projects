# linear_transformations.py
"""Volume 1: Linear Transformations.
Spencer Reschke
Math 345
September 28, 2019
"""

import numpy as np
from matplotlib import pyplot as plt
from random import random
import math
import time


# Problem 1
def stretch(A, a, b):
    """Scale the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    #construct matrix of transformation
    H = np.array([[a, 0], [0, b]])
    return np.matmul(H, A)
    raise NotImplementedError("Problem 1 Incomplete")

def shear(A, a, b):
    """Slant the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    #construct matrix of transformation
    H = np.array([[1, a], [b, 1]])
    return np.matmul(H, A)

def reflect(A, a, b):
    """Reflect the points in A about the line that passes through the origin
    and the point (a,b).

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): x-coordinate of a point on the reflecting line.
        b (float): y-coordinate of the same point on the reflecting line.
    """
    #construct matrix of transformation
    H = np.array([[a**2 - b**2, 2*a*b], [2*a*b, b**2 - a**2]])/(a**2 + b**2)
    return np.matmul(H, A)

def rotate(A, theta):
    """Rotate the points in A about the origin by theta radians.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        theta (float): The rotation angle in radians.
    """
    #construct matrix of transformation
    H = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])
    return np.matmul(H, A)
    
    
def test_transforms():
	"""Function used to test the 4 transforms defined above."""
	data = np.load("horse.npy")
	
	#set up subplots
	ax1 = plt.subplot(231)
	
	#plot original picture
	ax1.plot(data[0], data[1], 'k,')
	plt.title("Original")
	
	#plot stretch
	tdata = stretch(data, 1/2, 6/5)	
	ax2 = plt.subplot(232, sharey = ax1, sharex = ax1)
	ax2.plot(tdata[0], tdata[1], 'k,')
	plt.title("Stretch")
	
	#plot shear
	tdata = shear(data, 1/2, 0)
	ax3 = plt.subplot(233, sharey = ax1, sharex = ax1)
	ax3.plot(tdata[0], tdata[1], 'k,')
	plt.title("Shear")
	
	#plot reflection
	tdata = reflect(data, 0, 1)
	ax4 = plt.subplot(234, sharey = ax1, sharex = ax1)
	ax4.plot(tdata[0], tdata[1], 'k,')
	plt.title("Reflect")
	
	#plot rotation
	tdata = rotate(data, math.pi/2)
	ax5 = plt.subplot(235, sharey = ax1, sharex = ax1)
	ax5.plot(tdata[0], tdata[1], 'k,')
	plt.title("Rotation")
	
	#plot composition
	tdata = rotate(reflect(shear(stretch(data, 1/2, 6/5), 1/2, 0), 0, 1), math.pi/2)
	ax6 = plt.subplot(236, sharex = ax1, sharey = ax1)
	ax6.plot(tdata[0], tdata[1], 'k,')
	plt.title("Composition")
	
	fig=plt.gcf()
	fig.tight_layout()
	plt.show()


# Problem 2
def solar_system(T, x_e, x_m, omega_e, omega_m):
    """Plot the trajectories of the earth and moon over the time interval [0,T]
    assuming the initial position of the earth is (x_e,0) and the initial
    position of the moon is (x_m,0).

    Parameters:
        T (int): The final time.
        x_e (float): The earth's initial x coordinate.
        x_m (float): The moon's initial x coordinate.
        omega_e (float): The earth's angular velocity.
        omega_m (float): The moon's angular velocity.
    """
    pei = np.array([[x_e], [0]]) # initial earth vector
    pmi = np.array([[x_m - x_e], [0]]) # initial moon vector relative to earth
    pei_t = pei #earth vector at time t
    pmi_t = pmi #moon vector at time t
    total_t = T/omega_e #how much time the system is in motion
    theta_E = T #total angular displacement of Earth
    theta_M = total_t * omega_m #total angular displacement of Moon

    earth_x_coords = []
    earth_y_coords = []
    moon_x_coords = []
    moon_y_coords = []
    
    #plt.plot(pei_t[0], pei_t[1], "b.", label='Earth')
    #plt.plot(pmi_t[0] + pei_t[0], pmi_t[1] + pei_t[1], color="orange", marker=".", label='Moon')
    earth_x_coords.append(pei_t[0])
    earth_y_coords.append(pei_t[1])
    moon_x_coords.append(pmi_t[0] + pei_t[0])
    moon_y_coords.append(pmi_t[1] + pei_t[1])
    
    plot_points = 1000 #how many points to plot in system
    for i in range(plot_points):
    	  #rotate earth and moon vectors
        pei_t = rotate(pei_t, theta_E/plot_points)
        pmi_t = rotate(pmi_t, theta_M/plot_points)
        
        ##plot earth and moon vectors
        #plt.plot(pei_t[0], pei_t[1], "b.")
        #plt.plot(pmi_t[0] + pei_t[0], pmi_t[1] + pei_t[1], color="orange", marker=".")

        earth_x_coords.append(pei_t[0])
        earth_y_coords.append(pei_t[1])
        moon_x_coords.append(pmi_t[0] + pei_t[0])
        moon_y_coords.append(pmi_t[1] + pei_t[1])
    
    #cast lists to numpy arrays 
    earth_x_coords = np.array(earth_x_coords)
    earth_y_coords = np.array(earth_y_coords)
    moon_x_coords = np.array(moon_x_coords)
    moon_y_coords = np.array(moon_y_coords)
    
    #plot earth trajectory
    plt.plot(earth_x_coords, earth_y_coords, color="blue", marker=".", label='Earth')

    #plot moon trajectory
    plt.plot(moon_x_coords, moon_y_coords, color="orange", marker=".", label='Moon')
    
    ax = plt.gca()
    ax.set_aspect("equal")
    ax.legend()
    plt.show()


def random_vector(n):
    """Generate a random vector of length n as a list."""
    return [random() for i in range(n)]

def random_matrix(n):
    """Generate a random nxn matrix as a list of lists."""
    return [[random() for j in range(n)] for i in range(n)]

def matrix_vector_product(A, x):
    """Compute the matrix-vector product Ax as a list."""
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]

def matrix_matrix_product(A, B):
    """Compute the matrix-matrix product AB as a list of lists."""
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]

# Problem 3
def prob3():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    
    ns = []
    v_times = []
    m_times = []
    for i in range(9):
    	  n = 2**i
    	  ns.append(n)
    	  
    	  #generate vector and matrices
    	  x = random_vector(n)
    	  A = random_matrix(n)
    	  B = random_matrix(n)
    	  
    	  #time matrix-vector prod.
    	  tick = time.time()
    	  matrix_vector_product(A, x)
    	  tock = time.time()
    	  v_times.append(tock-tick)
    	  
    	  #time matrix-matrix-vector prod
    	  tick = time.time()
    	  matrix_matrix_product(A, B)
    	  tock = time.time()
    	  m_times.append(tock-tick)
        
    #cast everything to numpy arrays
    ns = np.array(ns)
    v_times = np.array(v_times)
    m_times = np.array(m_times)
    
    ax1 = plt.subplot(121)
    ax1.plot(ns, v_times, 'b', marker="o")
    plt.title("Matrix-Vector Multiplication")
    ax1.set_xlabel("n")
    ax1.set_ylabel("Seconds")
    
    ax2 = plt.subplot(122)
    ax2.plot(ns, m_times, color="orange", marker="o")
    plt.title("Matrix-Matrix Multiplication")
    ax2.set_xlabel("n")
    ax2.set_ylabel("Seconds")
    
    fig = plt.gcf()
    fig.tight_layout()
    plt.show()


# Problem 4
def prob4():
    """Time matrix_vector_product(), matrix_matrix_product(), and np.dot().

    Report your findings in a single figure with two subplots: one with all
    four sets of execution times on a regular linear scale, and one with all
    four sets of exections times on a log-log scale.
    """
    ns = []
    v_times = []
    m_times = []
    npv_times = []
    npm_times = []
    for i in range(10):
    	  n = 2**i
    	  ns.append(n)
    	  
    	  #generate vector and matrices
    	  x = random_vector(n)
    	  A = random_matrix(n)
    	  B = random_matrix(n)
    	  
    	  #time matrix-vector prod.
    	  tick = time.time()
    	  matrix_vector_product(A, x)
    	  tock = time.time()
    	  v_times.append(tock-tick)
    	  
    	  #time matrix-matrix-vector prod
    	  tick = time.time()
    	  matrix_matrix_product(A, B)
    	  tock = time.time()
    	  m_times.append(tock-tick)
    	  
    	  #cast vector and matrices to numpy arrays
    	  x = np.array(x)
    	  A = np.array(A)
    	  B = np.array(B)
    	  
    	  #time matrix-matrix-vector prod with numpy
    	  tick = time.time()
    	  A @ x
    	  tock = time.time()
    	  npv_times.append(tock-tick)
    	  
    	  #time matrix-matrix-vector prod with numpy
    	  tick = time.time()
    	  A @ B
    	  tock = time.time()
    	  npm_times.append(tock-tick)
    	  
        
    #cast everything to numpy arrays
    ns = np.array(ns)
    v_times = np.array(v_times)
    m_times = np.array(m_times)
    npv_times = np.array(npv_times)
    npm_times = np.array(npm_times)
    
    ax1 = plt.subplot(121)
    ax1.plot(ns, v_times, 'b', marker="o", label = "Normal MV")
    ax1.plot(ns, m_times, 'r', marker="o", label = "Normal MM")
    ax1.plot(ns, npv_times, 'k', marker="o", label = "NP MV")
    ax1.plot(ns, npm_times, 'y', marker="o", label = "NP MM")
    ax1.set_xlabel("n")
    ax1.set_ylabel("Seconds")
    plt.legend(loc="upper left", fontsize = "small")
    
    ax2 = plt.subplot(122)
    ax2.loglog(ns, v_times, 'b', marker="o", label = "Normal MV")
    ax2.loglog(ns, m_times, 'r', marker="o", label = "Normal MM")
    ax2.loglog(ns, npv_times, 'k', marker="o", label = "NP MV")
    ax2.loglog(ns, npm_times, 'y', marker="o", label = "NP MM")
    ax2.set_xlabel("n")
    ax2.set_ylabel("Seconds")
    plt.legend(loc="upper left", fontsize = "small")
    
    fig = plt.gcf()
    fig.tight_layout()
    
    plt.show()
    
    
if __name__ == "__main__":
	#problem 1
	#test_transforms()
	
	#problem 2
	solar_system(3*math.pi/2, 10, 11, 1, 13)
	
	#problem 3
	#prob3()
	
	#problem 4
	#prob4()
	
	
	
