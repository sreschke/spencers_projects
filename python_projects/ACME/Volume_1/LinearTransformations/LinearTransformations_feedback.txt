09/25/18 13:18

Problem 1 (10 points):
NotImplementedError: Problem 1 Incomplete

Problem 2 (15 points):
TypeError: solar_system() got an unexpected keyword argument 'x_e'

Problem 3 (10 points):
NotImplementedError: Problem 3 Incomplete

Problem 4 (10 points):
NotImplementedError: Problem 4 Incomplete

Code Quality (5 points):
Code can be improved for solar_system()
Score += 0

Total score: 0/50 = 0.0%

-------------------------------------------------------------------------------

09/28/18 14:13

Problem 1 (10 points):
NotImplementedError: Problem 1 Incomplete

Problem 2 (15 points):
TypeError: solar_system() got an unexpected keyword argument 'x_e'

Problem 3 (10 points):
NotImplementedError: Problem 3 Incomplete

Problem 4 (10 points):
NotImplementedError: Problem 4 Incomplete

Code Quality (5 points):
Code can be improved for prob4()
Score += 0

Total score: 0/50 = 0.0%


Comments:
	Let us know if you have any questions!

-------------------------------------------------------------------------------

10/01/18 11:07

Problem 1 (10 points):
Score += 10

Problem 2 (15 points):
NotImplementedError: Problem 2 Incomplete

Problem 3 (10 points):
NotImplementedError: Problem 3 Incomplete

Problem 4 (10 points):
NotImplementedError: Problem 4 Incomplete

Code Quality (5 points):
Score += 5

Total score: 15/50 = 30.0%

-------------------------------------------------------------------------------

10/01/18 13:20

Problem 1 (10 points):
Score += 10

Problem 2 (15 points):
Graph does not look correct
Score += 0

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 35/50 = 70.0%

-------------------------------------------------------------------------------

10/01/18 17:18

Problem 1 (10 points):
Score += 10

Problem 2 (15 points):
Not the correct graph
Score += 0

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 35/50 = 70.0%

-------------------------------------------------------------------------------

10/01/18 22:49

Problem 1 (10 points):
Score += 10

Problem 2 (15 points):
Score += 15

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
TimeoutError: Timeout after 60 seconds

Code Quality (5 points):
Score += 5

Total score: 40/50 = 80.0%


Comments:
	Make sure you are not doing too many iterations for problem 4

-------------------------------------------------------------------------------

10/02/18 10:00

Problem 1 (10 points):
Score += 10

Problem 2 (15 points):
Score += 15

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 50/50 = 100.0%

Excellent!

-------------------------------------------------------------------------------

