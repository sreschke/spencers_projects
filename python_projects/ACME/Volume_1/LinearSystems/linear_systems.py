# linear_systems.py
"""Volume 1: Linear Systems.
Spencer Reschke
Math 345
10/7/18
"""
import numpy as np
import copy
import time
from scipy import linalg as la
from matplotlib import pyplot as plt
from scipy import sparse
from scipy.sparse import linalg as spla


# Problem 1
def ref(A):
    """Reduce the square matrix A to REF. You may assume that A is invertible
    and that a 0 will never appear on the main diagonal. Avoid operating on
    entries that you know will be 0 before and after a row operation.

    Parameters:
        A ((n,n) ndarray): The square invertible matrix to be reduced.

    Returns:
        ((n,n) ndarray): The REF of A.
    """
    #cast A nto type 'float64'
    A = A.astype('float64')
    n = A.shape[0]
    for k in range(1, n): #iterate over columns
        for j in range(k, n): #iterate over rows
            m = (A[j, (k-1)]/A[(k-1), (k-1)]) #get multiplier
            A[j, (k-1):] -= m*A[(k-1), (k-1):] 
    return A


# Problem 2
def lu(A):
    """Compute the LU decomposition of the square matrix A. You may
    assume that the decomposition exists and requires no row swaps.

    Parameters:
        A ((n,n) ndarray): The matrix to decompose.

    Returns:
        L ((n,n) ndarray): The lower-triangular part of the decomposition.
        U ((n,n) ndarray): The upper-triangular part of the decomposition.
    """
    #get shapes of A
    m = A.shape[0]
    n = A.shape[1]
    U = copy.deepcopy(A)
    L = np.eye(m)
    for j in range(n):
        for i in range(j+1, m):
            L[i, j] = U[i, j]/U[j, j]
            U[i, j:] = U[i, j:] - L[i, j]*U[j, j:]
    return L, U


# Problem 3
def solve(A, b):
    """Use the LU decomposition and back substitution to solve the linear
    system Ax = b. You may again assume that no row swaps are required.

    Parameters:
        A ((n,n) ndarray)
        b ((n,) ndarray)

    Returns:
        x ((m,) ndarray): The solution to the linear system.
    """
    #get LU decomposition of A
    L, U = lu(A)

    n = L.shape[0]
    #solve system Ly = b
    y = np.zeros(n,)
    for k in range(n):
        sum = np.dot(L[k, 0:k], y[0:k])
        y[k] = b[k] - sum

    #solve system Ax = b
    x = np.zeros(n,)
    x[n-1] = (1/U[k, k])*y[n-1]
    for k in reversed(list(range(n-1))):
        sum = np.dot(U[k, k+1:], x[k+1:])
        x[k] = 1/U[k, k]*(y[k] - sum)
    return x



# Problem 4
def prob4():
    """Time different scipy.linalg functions for solving square linear systems.

    For various values of n, generate a random nxn matrix A and a random
    n-vector b using np.random.random(). Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Invert A with la.inv() and left-multiply the inverse to b.
        2. Use la.solve().
        3. Use la.lu_factor() and la.lu_solve() to solve the system with the
            LU decomposition.
        4. Use la.lu_factor() and la.lu_solve(), but only time la.lu_solve()
            (not the time it takes to do the factorization).

    Plot the system size n versus the execution times. Use log scales if
    needed.
    """

    method_1_times = []
    method_2_times = []
    method_3_times = []
    method_4_times = []
    ns = []

    for i in range(1, 12):
        #Generate A and b
        n = 2**i
        A = np.random.random((n, n))
        b = np.random.random((n,))

        ns.append(n)

        #time method 1
        tick = time.time()
        inverse = la.inv(A)
        solution = inverse@b
        tock = time.time()
        method_1_times.append(tock - tick)

        #time method 2
        tick = time.time()
        solution = la.solve(A, b)
        tock = time.time()
        method_2_times.append(tock - tick)

        #time method 3
        tick = time.time()
        L, P = la.lu_factor(A)
        solution = la.lu_solve((L,P), b)
        tock = time.time()
        method_3_times.append(tock - tick)

        #time method 4
        L, P = la.lu_factor(A)
        tick = time.time()
        solution = la.lu_solve((L,P), b)
        tock = time.time()
        method_4_times.append(tock - tick)

    #cast all list to numpy arrays
    method_1_times = np.array(method_1_times)
    method_2_times = np.array(method_2_times)
    method_3_times = np.array(method_3_times)
    method_4_times = np.array(method_4_times)

    plt.plot(ns, method_1_times, 'b', marker="o", label = "la.inv()")
    plt.plot(ns, method_2_times, 'r', marker="o", label = "la.solve()")
    plt.plot(ns, method_3_times, 'g', marker="o", label = "LU decomposition with decomposition")
    plt.plot(ns, method_4_times, 'y', marker="o", label = "LU decomposition without decomposition")
    plt.xscale('log')
    plt.xlabel("input size n")
    plt.ylabel("time in seconds")
    plt.legend(loc="upper left", fontsize = "small")
    plt.show()
    return


# Problem 5
def prob5(n):
    """Let I be the n x n identity matrix, and define
                    [B I        ]        [-4  1            ]
                    [I B I      ]        [ 1 -4  1         ]
                A = [  I . .    ]    B = [    1  .  .      ],
                    [      . . I]        [          .  .  1]
                    [        I B]        [             1 -4]
    where A is (n**2,n**2) and each block B is (n,n).
    Construct and returns A as a sparse matrix.

    Parameters:
        n (int): Dimensions of the sparse matrix B.

    Returns:
        A ((n**2,n**2) SciPy sparse matrix)
    """

    #throw exception if n < 1
    if n < 1:
        raise ValueError('n must satisfy n >= 1')
    #create identity matrix
    I = np.eye(n)

    #create B
    B = sparse.diags([1, -4, 1], offsets = [-1, 0, 1], shape=(n, n))

    #create A
    #create first row of A
    A = [[B, I] + [None]*(n-2)]
    #create rows 1 through n-2
    temp = [I, B, I]
    for i in range(1, n-1):
        row = [] + [None]*(i-1) + temp + [None]*(n-2-i)
        A.append(row)
    #create last row of A
    A.append([None]*(n-2) + [I, B])
    A = sparse.bmat(A, format='bsr')

    ##visualize matrix using plt.spy. Used to verify matrix A is correct
    #plt.spy(A, markersize=1)
    #plt.show()
    return A

# Problem 6
def prob6():
    """Time regular and sparse linear system solvers.

    For various values of n, generate the (n**2,n**2) matrix A described of
    prob5() and vector b of length n**2. Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Convert A to CSR format and use scipy.sparse.linalg.spsolve()
        2. Convert A to a NumPy array and use scipy.linalg.solve().

    In each experiment, only time how long it takes to solve the system (not
    how long it takes to convert A to the appropriate format). Plot the system
    size n**2 versus the execution times. As always, use log scales where
    appropriate and use a legend to label each line.
    """
    method_1_times = []
    method_2_times = []
    ns = []
    for i in range(1, 7):
        n = 2**i
        ns.append(n)
        A_orig = prob5(n)
        b = np.random.random((n**2,))
        #print(A_orig)

        #time method 1
        #cast to csr
        A = A_orig.tocsr()
        tick = time.time()
        spla.spsolve(A, b)
        tock = time.time()
        method_1_times.append(tock-tick)

        #time method 2
        #cast to numpy array
        A = np.array(A_orig.toarray())
        tick = time.time()
        la.solve(A, b)
        tock = time.time()
        method_2_times.append(tock-tick)

    plt.loglog(ns, method_1_times, 'b', marker="o", label = "spla.spsolve()")
    plt.loglog(ns, method_2_times, 'r', marker="o", label = "la.solve()")
    plt.legend(loc="upper left")
    plt.xlabel("input size n")
    plt.ylabel("time in seconds")
    #plt.xscale('log')
    plt.show()
    return


if __name__ == "__main__":
    #A = np.random.rand(3, 3)
    #print(ref(A))

    ##problem 2
    #A = np.random.rand(3, 3)
    #A = np.array([[4, 3], [6, 5]])
    #L, U = lu(A)
    #print(np.allclose(A, L@U))

    ##problem 3
    #A = np.random.rand(3, 3)
    #b = np.random.rand(3,)
    #x = solve(A, b)

    #print(np.allclose(A@x, b))

    #problem 4
    prob4()

    ##problem 5
    #print(prob5(4).toarray())

    #problem 6
    #prob6()
    print()