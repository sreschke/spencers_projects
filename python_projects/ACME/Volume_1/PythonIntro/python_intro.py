# python_intro.py
"""Python Essentials: Introduction to Python.
Spencer Reschke
<Class>
7/20/2018
"""

def sphere_volume(r):
    return 4/3 * 3.14159 * r**3

def isolate(a, b, c, d, e):
    print(a, end="     ")
    print(b, end="     ")
    print(c, end=" ")
    print(d, end=" ")
    print(e)

def first_half(some_string):
    return some_string[:len(some_string)//2]

def backward(some_string):
    return some_string[::-1]

def list_ops():
    a=["bear", "ant", "cat", "dog"]
    a.append("eagle")
    a[2]="fox"
    a.pop(1)
    a.sort(reverse=True)
    a[a.index("eagle")]="hawk"
    a[-1]=a[-1]+"hunter"
    return a

def pig_latin(word):
    if word[0] in ["a", "e", "i", "o", "u"]:
        return word + "hay"
    else:
        return word[1:] + word[0] + "ay"

def palindrome():
    nums=[]
    for i in range(100, 1000, 1):
        for j in range(100, 1000, 1):
            num=str(i*j)
            if num == num[::-1]:
                nums.append(i*j)
    return sorted(nums)[-1]

def alt_harmonic(n):
    return sum([((-1)**(x+1))/x for x in range(1, n+1)])


if __name__ == "__main__":
    print("Hello, world!")
    print(sphere_volume(10))
    isolate(1, 2, 3, 4, 5)
    print(first_half("hello"))
    print(backward("Hello world!"))
    list_ops()
    print(pig_latin("apple"))
    print(palindrome())
    print(alt_harmonic(10000))   