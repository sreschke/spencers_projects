# matplotlib_intro.py
"""Python Essentials: Intro to Matplotlib.
Spencer Reschke
345
9/17/18
"""
import numpy as np
from matplotlib import pyplot as plt


# Problem 1
def var_of_means(n):
    """Construct a random matrix A with values drawn from the standard normal
    distribution. Calculate the mean value of each row, then calculate the
    variance of these means. Return the variance.

    Parameters:
        n (int): The number of rows and columns in the matrix A.

    Returns:
        (float) The variance of the means of each row.
    """
    return np.var(np.mean(np.random.normal(size=(n, n)), axis=1))
    raise NotImplementedError("Problem 1 Incomplete")

def prob1():
    """Create an array of the results of var_of_means() with inputs
    n = 100, 200, ..., 1000. Plot and show the resulting array.
    """
    arr = []
    for i in range(1, 11):
        arr.append(var_of_means(i*100))
    arr = np.array(arr)
    plt.plot(arr)
    plt.show()
    return
    raise NotImplementedError("Problem 1 Incomplete")


# Problem 2
def prob2():
    """Plot the functions sin(x), cos(x), and arctan(x) on the domain
    [-2pi, 2pi]. Make sure the domain is refined enough to produce a figure
    with good resolution.
    """
    x = np.linspace(-2*np.pi, 2*np.pi, 100)
    plt.plot(x, np.sin(x))
    plt.plot(x, np.cos(x))
    plt.plot(x, np.arctan(x))
    plt.show()
    return
    raise NotImplementedError("Problem 2 Incomplete")


# Problem 3
def prob3():
    """Plot the curve f(x) = 1/(x-1) on the domain [-2,6].
        1. Split the domain so that the curve looks discontinuous.
        2. Plot both curves with a thick, dashed magenta line.
        3. Set the range of the x-axis to [-2,6] and the range of the
           y-axis to [-6,6].
    """
    x1 = np.linspace(-2, 0.99, 30)
    x2 = np.linspace(1.01, 6, 40)
    y1 = np.array([1/(x-1) for x in x1])
    y2 = np.array([1/(x-1) for x in x2])
    plt.plot(x1, y1, 'm--', linewidth=4)
    plt.plot(x2, y2, 'm--', linewidth=4)
    plt.xlim(-2, 6)
    plt.ylim(-6, 6)
    plt.show()
    return
    raise NotImplementedError("Problem 3 Incomplete")


# Problem 4
def prob4():
    """Plot the functions sin(x), sin(2x), 2sin(x), and 2sin(2x) on the
    domain [0, 2pi].
        1. Arrange the plots in a square grid of four subplots.
        2. Set the limits of each subplot to [0, 2pi]x[-2, 2].
        3. Give each subplot an appropriate title.
        4. Give the overall figure a title.
        5. Use the following line colors and styles.
              sin(x): green solid line.
             sin(2x): red dashed line.
             2sin(x): blue dashed line.
            2sin(2x): magenta dotted line.
    """
    x = np.linspace(0, 2*np.pi, 50)
    plt.subplot(2, 2, 1)
    plt.axis([0, 2*np.pi, -2, 2])
    plt.plot(x, np.sin(x), "g")
    plt.title("sin(x)")
    plt.xlim(0, 2*np.pi)
    plt.ylim(-2, 2)

    plt.subplot(2, 2, 2)
    plt.plot(x, np.sin(2*x), "r--")
    plt.title("sin(2x)")
    plt.xlim(0, 2*np.pi)
    plt.ylim(-2, 2)

    plt.subplot(2, 2, 3)
    plt.plot(x, 2*np.sin(x), "b--")
    plt.title("2sin(x)")
    plt.xlim(0, 2*np.pi)
    plt.ylim(-2, 2)

    plt.subplot(2, 2, 4)
    plt.plot(x, 2*np.sin(2*x), "m:")
    plt.title("2sin(2x)")
    plt.xlim(0, 2*np.pi)
    plt.ylim(-2, 2)

    plt.suptitle("sine transformations", fontsize=12, y=1)
    fig = plt.gcf()
    fig.tight_layout()
    plt.show()
    return
    raise NotImplementedError("Problem 4 Incomplete")


# Problem 5
def prob5():
    """Visualize the data in FARS.npy. Use np.load() to load the data, then
    create a single figure with two subplots:
        1. A scatter plot of longitudes against latitudes. Because of the
            large number of data points, use black pixel markers (use "k,"
            as the third argument to plt.plot()). Label both axes.
        2. A histogram of the hours of the day, with one bin per hour.
            Label and set the limits of the x-axis.
    """
    fars = np.load("FARS.npy")
    hours = fars[:, 0]
    longs = fars[:, 1]
    lats = fars[:, 2]
    ax1 = plt.subplot(1, 2, 1)
    ax1.plot(longs, lats, "k,", markersize=0.1)
    ax1.set_xlabel("Longitude")
    ax1.set_ylabel("Latitude")
    ax1.set_aspect("equal")
    ax2 = plt.subplot(1, 2, 2)
    ax2.hist(hours, bins=24, range=[0, 24])
    ax2.set_xlabel("hours in military time")
    ax2.set_ylabel("Frequency")
    fig = plt.gcf()
    fig.tight_layout()
    plt.show()
    return
    raise NotImplementedError("Problem 5 Incomplete")


# Problem 6
def prob6():
    """Plot the function f(x,y) = sin(x)sin(y)/xy on the domain
    [-2pi, 2pi]x[-2pi, 2pi].
        1. Create 2 subplots: one with a heat map of f, and one with a contour
            map of f. Choose an appropriate number of level curves, or specify
            the curves yourself.
        2. Set the limits of each subplot to [-2pi, 2pi]x[-2pi, 2pi].
        3. Choose a non-default color scheme.
        4. Add a colorbar to each subplot.
    """
    x = np.linspace(-2*np.pi, 2*np.pi, 100)
    y = x.copy()
    X, Y = np.meshgrid(x, y)
    Z = np.sin(X)*np.sin(Y)/(np.multiply(X, Y))

    #heat map
    plt.subplot(1, 2, 1)
    plt.pcolormesh(X, Y, Z, cmap="magma")
    plt.colorbar()
    plt.xlim(-2*np.pi, 2*np.pi)
    plt.ylim(-2*np.pi, 2*np.pi)

    #contour map
    plt.subplot(1, 2, 2)
    plt.contourf(X, Y, Z, 10, cmap="coolwarm")
    plt.colorbar()

    plt.suptitle("sin(x)sin(y)/xy", fontsize=12, y=1)
    fig=plt.gcf()
    fig.tight_layout()
    plt.show()
    return
    raise NotImplementedError("Problem 6 Incomplete")

if __name__ == "__main__":
    #prob1()
    #prob2()
    #prob3()
    #prob4()
    prob5()
    #prob6()
    None
