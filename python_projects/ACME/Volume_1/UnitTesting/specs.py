# specs.py
"""Python Essentials: Unit Testing.
Spencer Reschke
Math 345
10/22/2018
"""
import math
from itertools import combinations 

def add(a, b):
    """Add two numbers."""
    return a + b

def divide(a, b):
    """Divide two numbers, raising an error if the second number is zero."""
    if b == 0:
        raise ZeroDivisionError("second input cannot be zero")
    return a / b


# Problem 1
def smallest_factor(n):
    """Return the smallest prime factor of the positive integer n."""
    if n == 1: return 1
    for i in range(2, int(n**.5)+1): #error was fixed by adding a one to second argument of range function
        if n % i == 0: return i
    return n


# Problem 2
def month_length(month, leap_year=False):
    """Return the number of days in the given month."""
    if month in {"September", "April", "June", "November"}:
        return 30
    elif month in {"January", "March", "May", "July",
                        "August", "October", "December"}:
        return 31
    if month == "February":
        #take into account leap year
        if not leap_year:
            return 28
        else:
            return 29
    else: #return none if month was mis-spelled or not a string
        return None


# Problem 3
def operate(a, b, oper):
    """Apply an arithmetic operation to a and b."""
    if type(oper) is not str:
        raise TypeError("oper must be a string")
    elif oper == '+':
        return a + b
    elif oper == '-':
        return a - b
    elif oper == '*':
        return a * b
    elif oper == '/':
        if b == 0:
            raise ZeroDivisionError("division by zero is undefined")
        return a / b
    raise ValueError("oper must be one of '+', '/', '-', or '*'")


# Problem 4
class Fraction(object):
    """Reduced fraction class with integer numerator and denominator."""
    def __init__(self, numerator, denominator):
        #this is the constructor
        if denominator == 0:
            raise ZeroDivisionError("denominator cannot be zero")
        elif type(numerator) is not int or type(denominator) is not int:
            raise TypeError("numerator and denominator must be integers")

        
        def gcd(a,b):
            """Find greatest common divisor"""
            while b != 0:
                a, b = b, a % b
            return a
        common_factor = gcd(numerator, denominator)
        #reduce fraction
        self.numer = numerator // common_factor
        self.denom = denominator // common_factor

    def __str__(self):
        """Define str(). Allows Fraction objects to be cast to strings"""
        if self.denom != 1:
            return "{}/{}".format(self.numer, self.denom)
        else:
            return str(self.numer)

    def __float__(self):
        """allows Fraction objects to be cast to floats"""
        return self.numer / self.denom

    def __eq__(self, other):
        #check to see if other is equal to self
        if type(other) is Fraction:
            return self.numer==other.numer and self.denom==other.denom
        else:
            return float(self) == other

    def __add__(self, other):
        #bug found and fixed in this function
        return Fraction(self.numer*other.denom + self.denom*other.numer,
                                                        self.denom*other.denom)
    def __sub__(self, other):
        #bug found and fixed in this function
        return Fraction(self.numer*other.denom - self.denom*other.numer,
                                                        self.denom*other.denom)
    def __mul__(self, other):
        #this function looks good
        return Fraction(self.numer*other.numer, self.denom*other.denom)

    def __truediv__(self, other):
        #this function looks good
        if self.denom*other.numer == 0:
            raise ZeroDivisionError("cannot divide by zero")
        return Fraction(self.numer*other.denom, self.denom*other.numer)


# Problem 6
def count_sets(cards):
    """Return the number of sets in the provided Set hand.

    Parameters:
        cards (list(str)) a list of twelve cards as 4-bit integers in
        base 3 as strings, such as ["1022", "1122", ..., "1020"].
    Returns:
        (int) The number of sets in the hand.
    Raises:
        ValueError: if the list does not contain a valid Set hand, meaning
            - there are not exactly 12 cards,
            - the cards are not all unique,
            - one or more cards does not have exactly 4 digits, or
            - one or more cards has a character other than 0, 1, or 2.
    """
    #Check for Value Errors
    if len(cards) != 12:
        raise ValueError("There are not exactly 12 cards.")
    if len(set(cards)) != 12:
        raise ValueError("The cards are not all unique.")
    for card in cards:
        if len(card) != 4:
            raise ValueError("One or more cards does not have exactly 4 digits.")
    for card in cards:
        for digit in card:
            if digit not in ["0", "1", "2"]:
                raise ValueError("One or more cards has a character other than 0, 1, or 2.")

    #get combinations of length 3
    combs = combinations(cards, 3) 
    count = 0
    #check if each combination is a set
    for comb in combs:
        if is_set(comb[0], comb[1], comb[2]):
            count += 1 #increment count
    return count

def is_set(a, b, c):
    """Determine if the cards a, b, and c constitute a set.

    Parameters:
        a, b, c (str): string representations of 4-bit integers in base 3.
            For example, "1022", "1122", and "1020" (which is not a set).
    Returns:
        True if a, b, and c form a set, meaning the ith digit of a, b,
            and c are either the same or all different for i=1,2,3,4.
        False if a, b, and c do not form a set.
    """
    #get sum of a, b, c
    sum = str(int(a) + int(b) + int(c))
    #iterate through the digits to make sure they are divisible by 3
    #if not, then we don't have a set
    for digit in sum:
        if (int(digit)%3 != 0):
            return False
    #if we get here we should return True
    return True
