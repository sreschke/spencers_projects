# test_specs.py
"""Python Essentials: Unit Testing.
Spencer Reschke
Math 345
10/22/18
"""

import specs
import pytest


def test_add():
    """Tests add funtion from specs.py"""
    assert specs.add(1, 3) == 4, "failed on positive integers"
    assert specs.add(-5, -7) == -12, "failed on negative integers"
    assert specs.add(-6, 14) == 8

def test_divide():
    """Test divide function from specs.py"""
    assert specs.divide(4,2) == 2, "integer division"
    assert specs.divide(5,4) == 1.25, "float division"
    with pytest.raises(ZeroDivisionError) as excinfo:
        specs.divide(4, 0)
    assert excinfo.value.args[0] == "second input cannot be zero"


# Problem 1: write a unit test for specs.smallest_factor(), then correct it.
def test_smallest_factor():
    """Unit test for smallest_factor() in specs"""
    #bug was fixed by adding a one to second argument of range function
    assert specs.smallest_factor(1) == 1, "1 failed"
    assert specs.smallest_factor(2) == 2, "2 failed"
    assert specs.smallest_factor(3) == 3, "3 failed"
    assert specs.smallest_factor(4) == 2, "4 failed"
    assert specs.smallest_factor(5) == 5, "5 failed"
    assert specs.smallest_factor(6) == 2, "3 failed"
    assert specs.smallest_factor(7) == 7, "7 failed"
    assert specs.smallest_factor(8) == 2, "8 failed"
    assert specs.smallest_factor(9) == 3, "9 failed"
    assert specs.smallest_factor(11) == 11, "11 failed"

# Problem 2: write a unit test for specs.month_length().
def test_month_length():
    """Unit test for month_length in specs.py"""
    #Manually checks each month to ensure the correct length is returned
    for month in {"September", "April", "June", "November"}:
        assert specs.month_length(month) == 30, "{} failed".format(month)

    for month in {"January", "March", "May", "July",
                        "August", "October", "December"}:
        assert specs.month_length(month) == 31, "{} failed".format(month)
    
    #Test February with and without leap year
    assert specs.month_length("February") == 28, "February (no leap year) failed"
    assert specs.month_length("February", True) == 29, "February (leap year) failed"
    
    #Test misspelling
    assert specs.month_length("Mrch") == None, "Mis-spelled Mrch failed"
    #Test None
    assert specs.month_length(None) == None, "None failed"

# Problem 3: write a unit test for specs.operate().
def test_operate():
    """Unit test for operate function in specs.py"""
    #test addition
    assert specs.operate(1, 2, "+") == 3, "Addition on positive integers failed"
    assert specs.operate(-1, -2, "+") == -3, "Addition on negative integers failed"
    assert specs.operate(-1, 2, "+") == 1, "Addition on mixed integers failed"

    #test subtraction
    assert specs.operate(1, 2, "-") == -1, "Subtraction on positive integers failed"
    assert specs.operate(-1, -2, "-") == 1, "Subtraction on negative integers failed"
    assert specs.operate(-1, 2, "-") == -3, "Subtraction on mixed integers failed"
    
    #test multiplication
    assert specs.operate(3, 2, "*") == 6, "Multiplication on positive integers failed"
    assert specs.operate(-3, -2, "*") == 6, "Multiplication on negative integers failed"
    assert specs.operate(-3, 2, "*") == -6, "Multiplication on mixed integers failed"
    
    #test division
    assert specs.operate(6, 2, "/") == 3, "Division on positive integers failed"
    assert specs.operate(-6, -2, "/") == 3, "Division on negative integers failed"
    assert specs.operate(-6, 2, "/") == -3, "Division on mixed integers failed"

    #test exceptions
    with pytest.raises(TypeError) as excinfo: 
        specs.operate(4, 0, 3)
    assert excinfo.value.args[0] == "oper must be a string"

    with pytest.raises(ZeroDivisionError) as excinfo: 
        specs.operate(4, 0, "/") 
    assert excinfo.value.args[0] == "division by zero is undefined"

    with pytest.raises(ValueError) as excinfo: 
        specs.operate(4, 0, "a")
    assert excinfo.value.args[0] == "oper must be one of '+', '/', '-', or '*'"

    

# Problem 4: write unit tests for specs.Fraction, then correct it.
@pytest.fixture
def set_up_fractions():
    """Creates fractions to be used in unit tests"""
    #create fractions 1/3, 1/2, and -2/3
    frac_1_3 = specs.Fraction(1, 3)
    frac_1_2 = specs.Fraction(1, 2)
    frac_n2_3 = specs.Fraction(-2, 3)
    return frac_1_3, frac_1_2, frac_n2_3

def test_fraction_init(set_up_fractions):
    """Test Fraction constuctor"""
    #get fractions to test
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    #check members
    assert frac_1_3.numer == 1
    assert frac_1_2.denom == 2
    assert frac_n2_3.numer == -2
    #ensure fractions are properly reduced
    frac = specs.Fraction(30, 42)
    assert frac.numer == 5
    assert frac.denom == 7

    #test exceptions
    with pytest.raises(ZeroDivisionError) as excinfo: 
        specs.Fraction(4, 0)
    assert excinfo.value.args[0] == "denominator cannot be zero"

    with pytest.raises(TypeError) as excinfo: 
        specs.Fraction(4.1, 1)
    assert excinfo.value.args[0] == "numerator and denominator must be integers"

    with pytest.raises(TypeError) as excinfo: 
        specs.Fraction(4, 1.1)
    assert excinfo.value.args[0] == "numerator and denominator must be integers"



def test_fraction_str(set_up_fractions):
    """Test str() function"""
    #get example fractions to test
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert str(frac_1_3) == "1/3"
    assert str(frac_1_2) == "1/2"
    assert str(frac_n2_3) == "-2/3"

    #test if denominator is 1
    frac = specs.Fraction(3, 1)
    assert str(frac) == "3"

def test_fraction_float(set_up_fractions):
    """Test to float casting function"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert float(frac_1_3) == 1 / 3.
    assert float(frac_1_2) == .5
    assert float(frac_n2_3) == -2 / 3.

def test_fraction_eq(set_up_fractions):
    """Test equality function"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_2 == specs.Fraction(1, 2)
    assert frac_1_3 == specs.Fraction(2, 6)
    assert frac_n2_3 == specs.Fraction(8, -12)

    #test if other is not type Fraction
    assert frac_n2_3 == -2/3

def test_fraction_add(set_up_fractions):
    """test addition function from Fraction class"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert (frac_1_3 + frac_1_2) == 5/6
    assert frac_1_3 + frac_n2_3 == -1/3
    assert frac_1_2 + frac_n2_3 == specs.Fraction(-1, 6)


def test_fraction_sub(set_up_fractions):
    """test subtraction function from Fraction class"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_3 - frac_1_2 == -1/6
    assert frac_1_3 - frac_n2_3 == 1
    assert frac_1_2 - frac_n2_3 == specs.Fraction(7, 6)

def test_fraction_mul(set_up_fractions):
    """test multiplication function from Fraction class"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_3 * frac_1_2 == 1/6
    assert frac_1_3 * frac_n2_3 == -2/9
    assert frac_1_2 * frac_n2_3 == specs.Fraction(-2, 6)

def test_fraction_div(set_up_fractions):
    """test division function from Fraction class"""
    #get example fractions to test on
    frac_1_3, frac_1_2, frac_n2_3 = set_up_fractions
    assert frac_1_3 / frac_1_2 == 2/3
    assert frac_1_3 / frac_n2_3 == 1/-2
    assert frac_1_2 / frac_n2_3 == specs.Fraction(3, -4)
    
    #test exception
    with pytest.raises(ZeroDivisionError) as excinfo: 
        specs.Fraction(4, 1) / specs.Fraction(0, 2)
    assert excinfo.value.args[0] == "cannot divide by zero"



# Problem 5: Write test cases for Set.
@pytest.fixture
def make_cards():
    """Used to generate an example hand for unit tests"""
    #create card examples in lab specs
    cards = ["1022", "1122", "0100", "2021",
             "0010", "2201", "2111", "0020", 
             "1102", "0210", "2110", "1020"]
    return cards
    


def test_set_count_sets(make_cards):
    """Unit test for count_sets in specs.py"""
    #test exceptions
    #should have 12 cards
    with pytest.raises(ValueError) as excinfo: 
        specs.count_sets(["1111"])
    assert excinfo.value.args[0] == "There are not exactly 12 cards."

    #cards must be unique
    with pytest.raises(ValueError) as excinfo: 
        specs.count_sets(["1022", "1122", "0100", "2021",
                          "0010", "2201", "2111", "0020",
                          "1102", "0210", "2110", "1022"]) #first and last cards are the same
    assert excinfo.value.args[0] == "The cards are not all unique."

    #Test all cards have 4 digits
    with pytest.raises(ValueError) as excinfo: 
        specs.count_sets(["1022", "1122", "0100", "2021",
                          "0010", "2201", "2111", "0020",
                          "1102", "0210", "2110", "102"]) #last card has only 3 digits
    assert excinfo.value.args[0] == "One or more cards does not have exactly 4 digits."

    #All cards need to have 4 ditits
    with pytest.raises(ValueError) as excinfo: 
        specs.count_sets(["1022", "1122", "0100", "2021",
                          "0010", "2201", "2111", "0020", 
                          "1102", "0210", "2110", "102a"]) #last card has an 'a' in fourth digit
    assert excinfo.value.args[0] == "One or more cards has a character other than 0, 1, or 2."
    
    #test counts
    cards = make_cards
    assert specs.count_sets(cards) == 6, "Case in lab specs failed"

def test_set_is_set():
    """Unit test for is_set from specs.py"""
    #True cases
    assert specs.is_set("0000", "1111", "2222") == True
    assert specs.is_set("0120", "1201", "2012") == True
    assert specs.is_set("0120", "1201", "2012") == True

    #False cases
    assert specs.is_set("0000", "0111", "2222") == False
    assert specs.is_set("1201", "1202", "2200") == False
    assert specs.is_set("2222", "1111", "0002") == False
