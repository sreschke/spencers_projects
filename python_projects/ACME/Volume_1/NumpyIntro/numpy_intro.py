# numpy_intro.py
"""Python Essentials: Intro to NumPy.
Spencer
Math 345
9/10/18
"""
import numpy as np

def prob1():
    """Define the matrices A and B as arrays. Return the matrix product AB."""
    A = np.array([[3, -1, 4], 
                  [1, 5, -9]])
    B = np.array([[2, 6, -5, 3], 
                  [5, -8, 9, 7], 
                  [9, -3, -2, -3]])
    return A @ B
    raise NotImplementedError("Problem 1 Incomplete")


def prob2():
    """Define the matrix A as an array. Return the matrix -A^3 + 9A^2 - 15A."""
    A = np.array([[3, 1, 4],
                  [1, 5, 9],
                  [-5, 3, 1]])
    return -1*(A @ A @ A) + 9*(A @ A) - 15*(A)
    raise NotImplementedError("Problem 2 Incomplete")


def prob3():
    """Define the matrices A and B as arrays. Calculate the matrix product ABA,
    change its data type to np.int64, and return it.
    """
    A = np.triu(np.ones((7, 7)))
    B = np.tril(-1*np.ones((7, 7))) + np.triu(5*np.ones((7,7))) - 5*np.eye(7)
    return (A @ B @ A).astype(np.int64)
    raise NotImplementedError("Problem 3 Incomplete")


def prob4(A):
    """Make a copy of 'A' and set all negative entries of the copy to 0.
    Return the copy.

    Example:
        >>> A = np.array([-3,-1,3])
        >>> prob4(A)
        array([0, 0, 3])
    """
    copy = A.copy()
    mask = copy < 0
    copy[mask] = 0
    return copy
    raise NotImplementedError("Problem 4 Incomplete")


def prob5():
    """Define the matrices A, B, and C as arrays. Return the block matrix
                                | 0 A^T I |
                                | A  0  0 |,
                                | B  0  C |
    where I is the 3x3 identity matrix and each 0 is a matrix of all zeros
    of the appropriate size.
    """
    A = np.arange(6).reshape((3, 2)).T
    B = np.tril(3*np.ones((3, 3)))
    C = -2*np.eye(3)
    top = np.hstack((np.zeros((3, 3)), A.T, np.eye(3)))
    middle = np.hstack((A, np.zeros((2, 2)), np.zeros((2, 3))))
    bottom = np.hstack((B, np.zeros((3, 2)), C))
    return np.vstack((top, middle, bottom))
    raise NotImplementedError("Problem 5 Incomplete")


def prob6(A):
    """Divide each row of 'A' by the row sum and return the resulting array.

    Example:
        >>> A = np.array([[1,1,0],[0,1,0],[1,1,1]])
        >>> prob6(A)
        array([[ 0.5       ,  0.5       ,  0.        ],
               [ 0.        ,  1.        ,  0.        ],
               [ 0.33333333,  0.33333333,  0.33333333]])
    """
    return np.apply_along_axis(lambda row: row/sum(row), 1, A)
    raise NotImplementedError("Problem 6 Incomplete")


def prob7():
    """Given the array stored in grid.npy, return the greatest product of four
    adjacent numbers in the same direction (up, down, left, right, or
    diagonally) in the grid.
    """
    def square_checker(A):
        """given an nxn np array, returns the greatest product of n
        adjacent numbers in the same direction (up, down, left, right, 
        or diagonal"""
        assert A.shape[0] == A.shape[1], "A must be nxn"
        return max(max(np.prod(A, 0)), max(np.prod(A, 1)), np.prod(np.diag(A)), np.prod(np.diag(np.flip(A, 1)))) 
    grid = np.load("grid.npy")
    num_rows = grid.shape[0]
    num_cols = grid.shape[1]
    prod_len = 4

    max_prod = float("-inf")
    for i in range(num_rows - prod_len + 1):
        for j in range(num_cols - prod_len + 1):
            B = grid[i:(i+prod_len), j:(j+prod_len)]
            max_prod = max(max_prod, square_checker(B))
    return max_prod
    raise NotImplementedError("Problem 7 Incomplete")

#print(prob1())
#print(prob2())
#print(prob3())
#print(prob4(np.array([-3, -2, 0, 4, -10, 3])))
#print(prob5())
#print(prob6(np.array([[1,1,0],[0,1,0],[1,1,1]])))
#print(prob7())
