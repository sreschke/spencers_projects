01/18/19 15:58

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Incorrect expression
Score += 1

Problem 3 (5 points):
NotImplementedError: Problem 3 Incomplete

Problem 4 (5 points):
NotImplementedError: Problem 4 Incomplete

Problem 5 (5 points):
NotImplementedError: Problem 5 Incomplete

Problem 6 (10 points):
NotImplementedError: Problem 6 Incomplete

Problem 7 (10 points):
NotImplementedError: Problem 7 Incomplete

Code Quality (5 points):
add some comments
Score += 0

Total score: 6/50 = 12.0%

-------------------------------------------------------------------------------

01/21/19 16:18

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
Score += 5

Problem 6 (10 points):
nothing plotted
Score += 5

Problem 7 (10 points):
Score += 10

Code Quality (5 points):
Score += 5

Total score: 45/50 = 90.0%

Great job!

-------------------------------------------------------------------------------

