# sympy_intro.py
"""Python Essentials: Introduction to SymPy.
Spencer Reschke
Math 347
1/14/19
"""
import sympy as sy
import numpy as np
import matplotlib.pyplot as plt


# Problem 1
def prob1():
    """Return an expression for

        (2/5)e^(x^2 - y)cosh(x+y) + (3/7)log(xy + 1).

    Make sure that the fractions remain symbolic.
    """
    x, y = sy.symbols('x, y')
    return sy.Rational(2, 5)*sy.exp(x**2-y)*sy.cosh(x+y) + sy.Rational(3, 7)*sy.log(x*y+1)


# Problem 2
def prob2():
    """Compute and simplify the following expression.

        product_(i=1 to 5)[ sum_(j=i to 5)[j(sin(x) + cos(x))] ]
    """
    x, i, j = sy.symbols('x, i, j')
    return sy.simplify(sy.product(sy.summation(j*(sy.sin(x)+sy.cos(x)), (j, i, 5)), (i, 1, 5)))


# Problem 3
def prob3(N):
    """Define an expression for the Maclaurin series of e^x up to order N.
    Substitute in -y^2 for x to get a truncated Maclaurin series of e^(-y^2).
    Lambdify the resulting expression and plot the series on the domain
    y in [-3,3]. Plot e^(-y^2) over the same domain for comparison.
    """
    #Make lambda function of Maclarin series
    x, y, n = sy.symbols('x, y, n')
    expr = sy.summation(x**n/sy.factorial(n), (n, 0, N))
    y_expr = expr.subs(x, -y**2)
    f = sy.lambdify(y, y_expr, "numpy")

    #Make lambda function for e^(-y^2)
    g = sy.lambdify(y, sy.E**(-y**2))

    #Plot graph
    domain = np.linspace(-3, 3, 1000)
    plt.plot(domain, f(domain), "b", label="Maclarin Series", linewidth=2.0)
    plt.plot(domain, g(domain), "r", label="Original")
    plt.ylim((-3, 3))
    plt.title("N = {}".format(N))
    plt.legend(loc="lower left")
    plt.show()



# Problem 4
def prob4():
    """The following equation represents a rose curve in cartesian coordinates.

    0 = 1 - [(x^2 + y^2)^(7/2) + 18x^5 y - 60x^3 y^3 + 18x y^5] / (x^2 + y^2)^3

    Construct an expression for the nonzero side of the equation and convert
    it to polar coordinates. Simplify the result, then solve it for r.
    Lambdify a solution and use it to plot x against y for theta in [0, 2pi].
    """
    #create original expression
    x, y = sy.symbols("x, y")
    expr = 1 - ((x**2+y**2)**sy.Rational(7, 2) + 18*x**5*y-60*x**3*y**3 + 18*x*y**5)/(x**2+y**2)**3
    
    #Convert to polar coordinates
    r, theta = sy.symbols("r, theta")
    n_expr = sy.simplify(expr.subs({x: r*sy.cos(theta), y: r*sy.sin(theta)}))

    #Solve for r and create the function r(theta)
    r = sy.lambdify(theta, sy.solve(n_expr, r)[0], "numpy")
    
    #plot function
    domain = np.linspace(0, 2*np.pi, 1000)
    plt.plot(r(domain)*np.cos(domain), r(domain)*np.sin(domain), "b")
    plt.title("Rose Curve")
    plt.show()



# Problem 5
def prob5():
    """Calculate the eigenvalues and eigenvectors of the following matrix.

            [x-y,   x,   0]
        A = [  x, x-y,   x]
            [  0,   x, x-y]

    Returns:
        (dict): a dictionary mapping eigenvalues (as expressions) to the
            corresponding eigenvectors (as SymPy matrices).
    """

    #Find eigenvalues
    x, y, l = sy.symbols("x, y, l")
    A = sy.Matrix([[x-y, x, 0], [x, x-y, x], [0, x, x-y]])
    L = sy.Matrix([[l, 0, 0], [0, l, 0], [0, 0, l]])
    eig_vals = sy.solve(sy.det(A-L), l)

    #Solve for eigenvectors
    e_map = {}
    for eig_val in eig_vals:
        eig_vec = (A - eig_val*sy.eye(3)).nullspace()[0]
        e_map[eig_val] = eig_vec

    return e_map

# Problem 6
def prob6():
    """Consider the following polynomial.

        p(x) = 2*x^6 - 51*x^4 + 48*x^3 + 312*x^2 - 576*x - 100

    Plot the polynomial and its critical points. Determine which points are
    maxima and which are minima.

    Returns:
        (set): the local minima.
        (set): the local maxima.
    """

    #define the polynomial p
    x = sy.symbols("x")
    p = 2*x**6-51*x**4+48*x**3+312*x**2-576*x-100

    #calculate the critical points
    pp = sy.simplify(sy.diff(p, x))
    cps = sy.solve(pp, x)

    #calcuate second derivative
    ppp = sy.simplify(sy.diff(pp, x))

    #apply second derivative test
    mins = set()
    maxs = set()
    for cp in cps:
        sign = ppp.subs({x: cp}).evalf()
        if sign < 0: #found a local maximum
            maxs.add(cp)
        elif sign > 0: #found local minimum
            mins.add(cp)
        else: #test has failed
            print("Test failed for critical point {}".format(cp))

        
    #Plot polynomial
    px = sy.lambdify(x, p, "numpy")
    domain = np.linspace(-5, 5, 1000)
    plt.plot(domain, px(domain), 'k')
    plt.scatter(np.array(list(mins)), px(np.array(list(mins))), c='r', label="Mins")
    plt.scatter(np.array(list(maxs)), px(np.array(list(maxs))), c='b', label="Maxs")
    plt.title("p(x)")
    plt.legend()
    plt.show()
    return mins, maxs



# Problem 7
def prob7():
    """Calculate the integral of f(x,y,z) = (x^2 + y^2 + z^2)^2 over the
    sphere of radius r. Lambdify the resulting expression and plot the integral
    value for r in [0,3]. Return the value of the integral when r = 2.

    Returns:
        (float): the integral of f over the sphere of radius 2.
    """
    x, y, z, p, theta, phi, r = sy.symbols("x, y, z, r, theta, phi, r")

    #define f
    f = (x**2 + y**2 + z**2)**2

    #Calculate Jacobian matrix
    h = sy.Matrix([[p*sy.sin(phi)*sy.cos(theta)],
                   [p*sy.sin(phi)*sy.sin(theta)],
                   [p*sy.cos(phi)]])

    J = h.jacobian([p, theta, phi])

    #Change to spherical coordinates
    frtp = sy.simplify(f.subs({x: p*sy.sin(phi)*sy.cos(theta),
                               y: p*sy.sin(phi)*sy.sin(theta),
                               z: p*sy.cos(phi)}))

    #Calculate integral
    integrand = frtp*(-J.det())
    integral = sy.integrate(sy.integrate(sy.integrate(integrand,
                            (p, 0, r)),
                            (theta, 0, 2*sy.pi)),
                            (phi, 0, sy.pi))

    #lambdify integral expression
    IE = sy.lambdify(r, integral, "numpy")

    #make plot
    domain = np.linspace(0, 3, 1000)
    plt.plot(domain, IE(domain), "b")
    plt.xlabel("radius r")
    plt.ylabel("Value")
    plt.title("Integral of f over ball of radius r")
    plt.show()

    return IE(2)


def test_prob1():
    print(prob1())

    print("All tests passed for problem 1")

def test_prob2():
    print(prob2())

    print("All tests passed for problem 2")

def test_prob3():
    for i in range(6):
        N = 2**i
        prob3(N)

    print("All tests passed for problem 3")

def test_prob4():
    prob4()

def test_prob5():
    x, y, l = sy.symbols("x, y, l")
    A = sy.Matrix([[x-y, x, 0], [x, x-y, x], [0, x, x-y]])

    e_map = prob5()
    
    #Test that Al = lv for each eigenvalue eigenvector pair
    for ev, evec in e_map.items():
        assert (sy.simplify(sy.expand(A@evec)) == sy.simplify(sy.expand(ev*evec)))

def test_prob6():
    mins, maxs = prob6()
    print(mins)
    print(maxs)


def test_prob7():
    print(prob7())


if __name__ == "__main__":
    #test_prob1()
    #test_prob2()
    #test_prob3()
    #test_prob4()
    #test_prob5()
    test_prob6()
    #test_prob7()