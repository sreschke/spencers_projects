"""Volume 1: The SVD and Image Compression. 
Spencer Reschke
11/25/2018
"""

import numpy as np
from scipy import linalg as la
import matplotlib.pyplot as plt
from imageio import imread


# Problem 1
def compact_svd(A, tol=1e-6):
    """Compute the truncated SVD of A.

    Parameters:
        A ((m,n) ndarray): The matrix (of rank r) to factor.
        tol (float): The tolerance for excluding singular values.

    Returns:
        ((m,r) ndarray): The orthonormal matrix U in the SVD.
        ((r,) ndarray): The singular values of A as a 1-D array.
        ((r,n) ndarray): The orthonormal matrix V^H in the SVD.

    See Algorithm 6.1
    """
    #Get eigenvalues and eigenvectors of A^H A
    AH = A.conj().T
    AHA = AH @ A
    eig_vals, eig_vects = np.linalg.eig(AHA)

    #Get singular values of A
    sing_vals = np.sqrt(eig_vals)

    #Sort singular values from greatest to least (steps 4 and 5)
    indices = np.array(list(reversed(np.argsort(sing_vals))))
    sing_vals = sing_vals[indices]
    V = eig_vects[:, indices]

    #Count number of nonzero singular values (the rank of A)
    r = (sing_vals > tol).sum()

    #Keep only the positive singular values
    S1 = sing_vals[:r]

    #Keep only the corresponding eigenvectors
    V1 = V[:, :r]

    #Construct U with array broadcasting
    U1 = (A @ V1)/S1

    return U1, S1, V1.conj().T

# Problem 2
def visualize_svd(A):
    """Plot the effect of the SVD of A as a sequence of linear transformations
    on the unit circle and the two standard basis vectors.
    """
    #Get 200 points around the unit circle
    xs = np.cos(np.linspace(0, 2*np.pi, 200))
    ys = np.sin(np.linspace(0, 2*np.pi, 200))
    S = np.vstack([xs, ys])

    E = np.array([[1, 0, 0], [0, 0, 1]])

    #Compue full SVD of A
    U, SV, Vh = la.svd(A)

    #Make plots
    plt.subplot(2, 2, 1)

    #Plot unit circle
    plt.plot(xs, ys, 'b')

    #Plot standard basis vectors
    plt.plot(E[0], E[1], "-", color="orange")    
    plt.xticks([-1, -0.5, 0, 0.5, 1.0])
    plt.axis("equal")
    plt.xlabel("(a) S")

    plt.subplot(2, 2, 2)

    #Plot unit circle
    plt.plot(xs, ys, 'b')
    plt.plot((Vh@E)[0], (Vh@E)[1], "-", color="orange")
    plt.xticks([-1, -0.5, 0, 0.5, 1.0])
    plt.axis("equal")
    plt.xlabel("(b) V^H S")

    plt.subplot(2, 2, 3)

    plt.plot((np.diag(SV)@Vh@S)[0], (np.diag(SV)@Vh@S)[1], 'b')
    plt.plot((np.diag(SV)@Vh@E)[0], (np.diag(SV)@Vh@E)[1], "-", color="orange")
    plt.xticks([-4, -2, 0, 2, 4.0])
    plt.yticks([-4, -2, 0, 2, 4])
    plt.axis("equal")
    plt.xlabel("(c) SV V^H S")

    plt.subplot(2, 2, 4)

    plt.plot((U@np.diag(SV)@Vh@S)[0], (U@np.diag(SV)@Vh@S)[1], 'b')
    plt.plot((U@np.diag(SV)@Vh@E)[0], (U@np.diag(SV)@Vh@E)[1], "-", color="orange")
    plt.xticks([-4, -2, 0, 2, 4.0])
    plt.yticks([-4, -2, 0, 2, 4])
    plt.axis("equal")
    plt.xlabel("(d) U SV V^H S")
    
    plt.show()



# Problem 3
def svd_approx(A, s):
    """Return the best rank s approximation to A with respect to the 2-norm
    and the Frobenius norm, along with the number of bytes needed to store
    the approximation via the truncated SVD.

    Parameters:
        A ((m,n), ndarray)
        s (int): The rank of the desired approximation.

    Returns:
        ((m,n), ndarray) The best rank s approximation of A.
        (int) The number of entries needed to store the truncated SVD.
    """

    #get rank of A
    r = np.linalg.matrix_rank(A)
    if s > r:
        raise ValueError("s must be less than or equal to the rank of A")

    #Get Compact SVD
    U, S, VH = compact_svd(A)

    #Strip off appropriate columns
    U1 = U[:, :s]
    S1 = S[:s]
    VH1 = VH[:s, :]

    num_vals = U1.size+S1.size+VH1.size
    A_approx = U1@np.diag(S1)@VH1



    return A_approx, num_vals


# Problem 4
def lowest_rank_approx(A, err):
    """Return the lowest rank approximation of A with error less than 'err'
    with respect to the matrix 2-norm, along with the number of bytes needed
    to store the approximation via the truncated SVD.

    Parameters:
        A ((m, n) ndarray)
        err (float): Desired maximum error.

    Returns:
        A_s ((m,n) ndarray) The lowest rank approximation of A satisfying
            ||A - A_s||_2 < err.
        (int) The number of entries needed to store the truncated SVD.
    """
    #get rank of A
    r = np.linalg.matrix_rank(A)

    #Get Compact SVD
    U, S, VH = compact_svd(A)

    if err <= S[-1]:
        raise ValueError("A cannot be approximated within the given tolerance")
    #Calculate appropriate s
    s = len(np.where(S>=err)[0])

    #Strip off appropriate columns
    U1 = U[:, :s]
    S1 = S[:s]
    VH1 = VH[:s, :]

    num_vals = U1.size+S1.size+VH1.size
    A_approx = U1@np.diag(S1)@VH1

    return A_approx, num_vals


# Problem 5
def compress_image(filename, s):
    """Plot the original image found at 'filename' and the rank s approximation
    of the image found at 'filename.' State in the figure title the difference
    in the number of entries used to store the original image and the
    approximation.

    Parameters:
        filename (str): Image file path.
        s (int): Rank of new image.
    """

    image = imread(filename)/255

    dims = len(image.shape)
    if dims == 2: #grayscale image
        best_approx, num_vals = svd_approx(image, s)
        
        #clip
        best_approx = np.clip(best_approx, 0, 1)
        
        #plot original image
        plt.subplot(1, 2, 1)
        plt.imshow(image, cmap="gray")
        plt.xlabel("Original")
        plt.xticks([])
        plt.yticks([])
        
        #plot approximation
        plt.subplot(1, 2, 2)
        plt.imshow(best_approx, cmap="gray")
        plt.xlabel("Approximation")        
        plt.xticks([])
        plt.yticks([])
        
        #Calculate number of entries saved and show figure
        entries_saved = image.size - num_vals
        plt.suptitle("Saved {} matrix entries".format(entries_saved))
        plt.tight_layout()
        plt.show()
    elif dims == 3: #RBG image
        #get RGB approximations
        red_image = image[:, :, 0]
        green_image = image[:, :, 1]
        blue_image = image[:, :, 2]
        
        red_approx, num_r_vals = svd_approx(red_image, s)
        green_approx, num_g_vals = svd_approx(green_image, s)
        blue_approx, num_b_vals = svd_approx(blue_image, s)
        
        #Clip values
        red_approx = np.clip(red_approx, 0, 1)
        green_approx = np.clip(green_approx, 0, 1)
        blue_approx = np.clip(blue_approx, 0, 1)
        
        #reconstruct approximation
        best_approx = np.dstack([red_approx, green_approx, blue_approx])
        
        #plot original image
        plt.subplot(1, 2, 1)
        plt.imshow(image)
        plt.xlabel("Original")
        plt.xticks([])
        plt.yticks([])
        
        #plot approximation
        plt.subplot(1, 2, 2)
        plt.imshow(best_approx, cmap="gray")
        plt.xlabel("Approximation")        
        plt.xticks([])
        plt.yticks([])
        
        #Calculate number of entries saved and show figure
        entries_saved = image.size - (num_r_vals+num_g_vals+num_b_vals)
        plt.suptitle("Saved {} matrix entries".format(entries_saved))
        plt.tight_layout()
        plt.show()
    else:
        assert True==False, "Error in image"


def test_compact_svd():
    A = np.array([[2, 5, 4], [6, 3, 0], [6, 3, 0], [2, 5, 4]])
    U1, S1, V1 = compact_svd(A)

    #make sure A = U1 diag(S1) V1
    assert np.allclose(A, U1@np.diag(S1)@V1)

    #make sure U1 has orthonormal columns
    assert np.allclose(U1.T @ U1, np.identity(2))

    #make sure V1 has orthonormal columns
    assert np.allclose(V1@V1.T, np.identity(2))

    #make sure number of nonzero singular values is the rank of A (2 in this case)
    assert len(S1) == np.linalg.matrix_rank(A)

    print("Passed all tests for compact_svd")

def test_visualize_svd():
    A = np.array([[3, 1], [1, 3]])
    visualize_svd(A)

def test_svd_approx():
    A = np.array([[2, 5, 4], [6, 3, 0], [6, 3, 0], [2, 5, 4]])
    A_approx, num_vals = svd_approx(A, 2)

    assert np.allclose(A_approx, A)
    assert num_vals == 16

    try:
        svd_approx(A, 3)
    except ValueError as e:
        pass

    print("All tests passed for svd_approx()")

def test_lowest_rank_approx():
    A = np.array([[2, 5, 4], [6, 3, 0], [6, 3, 0], [2, 5, 4]])
    err = 13
    A_approx, num_vals = lowest_rank_approx(A, err)

    assert np.linalg.norm(A-A_approx, ord=2) < err

    try:
        lowest_rank_approx(A, 5)
    except ValueError as e:
        pass
    print("All tets passed for lowest_rank_approx()")
    
def test_compress_image():
	 #Test gray image
    image_name = "hubble_gray.jpg"
    s=20
    compress_image(image_name, s)
    
    #Test RGB image
    image_name = "hubble.jpg"
    s=20
    compress_image(image_name, s)

if __name__ == "__main__":
    #test_compact_svd()
    #test_visualize_svd()
    #test_svd_approx()
    #test_lowest_rank_approx()
    test_compress_image()
    