# qr_decomposition.py
"""Volume 1: The QR Decomposition.
Spencer Reschke
Math 345
10/26/2018
"""

import numpy as np
from scipy import linalg as la

# Problem 1
def qr_gram_schmidt(A):
    """Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,n) ndarray): An orthonormal matrix.
        R ((n,n) ndarray): An upper triangular matrix.
    """
    #Store dimensions of A
    m=A.shape[0]
    n=A.shape[1]

    #Make a copy of A
    Q = np.copy(A)

    #initialize R
    R = np.zeros((n, n))
    for i in range(0, n):
        R[i][i] = np.linalg.norm(Q[:, i])
        Q[:, i] = Q[:, i]/R[i,i]
        for j in range(i+1, n):
            R[i, j] = np.dot(Q[:, j], Q[:, i])
            Q[:, j] = Q[:, j] - R[i, j]*Q[:, i]
    return Q, R


# Problem 2
def abs_det(A):
    """Use the QR decomposition to efficiently compute the absolute value of
    the determinant of A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) the absolute value of the determinant of A.
    """
    #economic used for reduced QR
    return abs(np.prod(np.diag(la.qr(A, mode="economic")[1])))


# Problem 3
def solve(A, b):
    """Use the QR decomposition to efficiently solve the system Ax = b.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.
        b ((n, ) ndarray): A vector of length n.

    Returns:
        x ((n, ) ndarray): The solution to the system Ax = b.
    """
    #cast to floats to preserve precision
    b = b.astype(np.float64)
    A = A.astype(np.float64)

    #get Q and R
    Q, R = la.qr(A, mode="economic")

    #cast to floats to preserve precision
    Q = Q.astype(np.float64)
    R = R.astype(np.float64)

    #calculate y = Q^Tb
    y = Q.T @ b

    #cast to floats to preserve precision
    y = y.astype(np.float64)   

    #back substitution; see textbook, previous labs, and stack exchange
    n= len(y)
    x = np.zeros_like(y).astype(np.float64)
    for i in range(n-1, -1, -1): #iterate over rows in reverse order
        for j in range(i+1, n): #iterate over rows 
            y[i] = np.subtract(y[i], R[i, j]*x[j])
        x[i] = np.divide(y[i], R[i, i])
    return x


# Problem 4
def qr_householder(A):
    """Compute the full QR decomposition of A via Householder reflections.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,m) ndarray): An orthonormal matrix.
        R ((m,n) ndarray): An upper triangular matrix.
    """
    #lambda function for sign
    sign = lambda x: 1 if x >= 0 else -1

    #get shapes of A
    m = A.shape[0]
    n = A.shape[1]

    #copy A and initialize Q
    R = np.copy(A)
    Q = np.identity(m)

    for k in range(n):
        u = np.copy(R[k:, k])
        u[0] = u[0] + sign(u[0])*np.linalg.norm(u)

        #normalize u
        u = u/np.linalg.norm(u)

        #Apply reflection to R
        R[k:, k:] = R[k:, k:] - 2*np.outer(u, (u.T @ R[k:, k:]))

        #Apply reflection to Q     
        Q[k:,:] = Q[k:, :] - 2*np.outer(u, (u.T @ Q[k:, :]))
    return Q.T, R


# Problem 5
def hessenberg(A):
    """Compute the Hessenberg form H of A, along with the orthonormal matrix Q
    such that A = QHQ^T.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.

    Returns:
        H ((n,n) ndarray): The upper Hessenberg form of A.
        Q ((n,n) ndarray): An orthonormal matrix.
    """
    #lambda function for sign
    sign = lambda x: 1 if x >= 0 else -1

    #get dimensions of A
    m = A.shape[0]
    n = m

    #initialize H and Q
    H = np.copy(A)
    Q = np.identity(m)

    for k in range(n-2):
        #(m-(k+1), 1)
        u = np.copy(H[(k+1):, k])
        u[0] = u[0] + sign(u[0])*np.linalg.norm(u)

        #normalize u
        u = u/np.linalg.norm(u)

        #Apply Q_k to H
        H[(k+1):, k:] = H[(k+1):, k:] - 2*np.outer(u, (np.dot(u, H[(k+1):, k:])))

        #Apply Q_k^T to H
        temp = np.dot(H[:, (k+1):], u)
        temp = np.reshape(temp, (len(temp), 1))
        H[:, (k+1):] = H[:, (k+1):] - 2*np.dot(temp, np.reshape(u, (len(u), 1)).T)

        #Apply Q_k to Q
        temp = np.dot(np.reshape(u, (len(u), 1)).T, Q[(k+1):, :])
        temp2 = np.dot(np.reshape(u, (len(u), 1)), temp)
        Q[(k+1):, :] = Q[(k+1):, :] - 2*temp2
    
    return H, Q.T

def test_qr_gram_schmidt():    
    #test example from textbook pg. 103
    A = np.array([[1, -2, 3.5],
                  [1, 3, -0.5],
                  [1, 3, 2.5],
                  [1, -2, 0.5]])
    m = 4
    n = 3

    #get Q and R
    Q, R = qr_gram_schmidt(A)

    #check that R is upper triangular
    assert np.allclose(np.triu(R), R), "R is not upper triangular"

    #check that Q is orthonormal
    assert np.allclose(Q.T @ Q, np.identity(n)), "Q is not orthonormal"

    #check that A = QR
    assert np.allclose(A, Q@R), "A does not equal QR"

    print("All tests passed for qr_gram_schmidt()")
    

def test_abs_det():
    """Used to test abs_det"""
    #construct a test matrix A
    A = np.array([[7, 2, 1], [0, 3, -1], [-3, 4, -2]])
    assert np.allclose(abs_det(A), abs(la.det(A))), "abs_det failed"
    print("All tests passed for abs_det()")

def test_solve():
    A = np.array([[1, 1, -1], [0, 1, 3], [0, 0, -6]])
    b = np.array([9, 3, 8])
    x = solve(A, b)
    assert np.allclose(A@x, b), "solve failed"
    print("All tests passed for solve()")

def test_qr_householder():
    #test example from textbook pg. 103
    A = np.array([[1, -2, 3.5],
                  [1, 3, -0.5],
                  [1, 3, 2.5],
                  [1, -2, 0.5]])
    m = 4
    n = 3

    #get Q and R
    Q, R = qr_householder(A)

    #check that R is upper triangular
    assert np.allclose(np.triu(R), R), "R is not upper triangular"

    #check that Q is orthonormal
    assert np.allclose(Q.T @ Q, np.identity(m)), "Q is not orthonormal"

    #check that A = QR
    assert np.allclose(A, Q@R), "A does not equal QR"

    print("All tests passed for qr_householder()")


def test_hessenberg():
    #test example from textbook pg. 103
    A = np.array([[1, -2, 3.5],
                  [1, 3, -0.5],
                  [1, 3, 2.5]])
    m = 4
    n = 3

    #get H and Q^T
    H, QT = hessenberg(A)

    #verify that H has all zeros below the first subdiagonal
    assert np.allclose(np.triu(H, -1), H), "Problem with H"

    #verify QHQ^T = A
    np.allclose(QT.T @ H @ QT, A), "Hessenberg failed"
    print("All tests passed for hessenberg()")


if __name__ == "__main__":
    #prob1
    #test_qr_gram_schmidt()

    #prob2
    #test_abs_det()

    #prob3
    #test_solve()

    #prob4
    #test_qr_householder()

    #prob5
    test_hessenberg()