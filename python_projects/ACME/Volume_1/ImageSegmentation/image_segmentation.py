# image_segmentation.py
"""Volume 1: Image Segmentation.
Spencer Reschke
Math 344
11/12/18
"""

import numpy as np
from scipy import sparse
from scipy import linalg as la
from imageio import imread
from matplotlib import pyplot as plt
import math
from scipy.sparse.csgraph import laplacian as lap
from scipy.sparse.linalg import eigsh


# Problem 1
def laplacian(A):
    """Compute the Laplacian matrix of the graph G that has adjacency matrix A.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.

    Returns:
        L ((N,N) ndarray): The Laplacian matrix of G.
    """
    #make sure A is square
    assert A.shape[0] == A.shape[1]

    #Get row sums
    diags = np.sum(A, axis=1)

    #Construct degree matrix
    D = np.diag(diags)

    return D - A


# Problem 2
def connectivity(A, tol=1e-8):
    """Compute the number of connected components in the graph G and its
    algebraic connectivity, given the adjacency matrix A of G.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.
        tol (float): Eigenvalues that are less than this tolerance are
            considered zero.

    Returns:
        (int): The number of connected components in G.
        (float): the algebraic connectivity of G.
    """
    #Get the laplacian of A
    L = laplacian(A)

    #Get the eigenvalues of L
    eigs = np.real(la.eigvals(L))

    #Apply mask
    num_components = (eigs < tol).sum()

    #Get second smallest eigenvalue
    eigs.sort()
    algConn = eigs[1]

    return num_components, algConn





# Helper function for problem 4.
def get_neighbors(index, radius, height, width):
    """Calculate the flattened indices of the pixels that are within the given
    distance of a central pixel, and their distances from the central pixel.

    Parameters:
        index (int): The index of a central pixel in a flattened image array
            with original shape (radius, height).
        radius (float): Radius of the neighborhood around the central pixel.
        height (int): The height of the original image in pixels.
        width (int): The width of the original image in pixels.

    Returns:
        (1-D ndarray): the indices of the pixels that are within the specified
            radius of the central pixel, with respect to the flattened image.
        (1-D ndarray): the euclidean distances from the neighborhood pixels to
            the central pixel.
    """
    # Calculate the original 2-D coordinates of the central pixel.
    row, col = index // width, index % width

    # Get a grid of possible candidates that are close to the central pixel.
    r = int(radius)
    x = np.arange(max(col - r, 0), min(col + r + 1, width))
    y = np.arange(max(row - r, 0), min(row + r + 1, height))
    X, Y = np.meshgrid(x, y)

    # Determine which candidates are within the given radius of the pixel.
    R = np.sqrt(((X - col)**2 + (Y - row)**2))
    mask = R < radius
    return (X[mask] + Y[mask]*width).astype(np.int), R[mask]


# Problems 3-6
class ImageSegmenter:
    """Class for storing and segmenting images."""

    # Problem 3
    def __init__(self, filename):
        """Read the image file. Store its brightness values as a flat array."""
        #read in the file and scale values
        image = imread(filename)/255

        #store the type of image (2 is grayscale 3 is color)
        self.type = len(image.shape)
        assert self.type in [2, 3], "type is off"
        self.original = image
        if len(image.shape) == 3: #if the image is in colore, calculate the brightness
            image = image.mean(axis=2)
        self.flattened = np.ravel(image)

    # Problem 3
    def show_original(self):        
        """Display the original image."""
        if self.type == 2: #if the image is grayscale
            plt.imshow(self.original, cmap="gray")
            plt.axis("off")
            plt.show()
        else: #if the image is color
            plt.imshow(self.original)
            plt.axis("off")
            plt.show()


    # Problem 4
    def adjacency(self, r=5., sigma_B2=.02, sigma_X2=3.):
        """Compute the Adjacency and Degree matrices for the image graph."""
        #get num rows and num columns
        m = self.original.shape[0]
        n = self.original.shape[1]

        A = sparse.lil_matrix((m*n, m*n))
        D = np.array([0]*m*n, dtype=float)

        for i in range(m*n): #Iterate over the vertices
            #Find all neighbors of vertex i
            Ji, Xi = get_neighbors(i, r, m, n)
            #Calculate the weights for all neighbors of vertex i (see 5.3)
            weights = np.zeros_like(Ji, dtype=float)
            for j in range(len(Ji)):
                Bi = self.flattened[i]
                Bj = self.flattened[Ji[j]]
                weights[j] = math.exp(-abs(Bi-Bj)/sigma_B2 -Xi[j]/sigma_X2)
         
            A[i, Ji] = weights
            D[i] = weights.sum()
        A = sparse.csc_matrix(A)
        return A, D

    # Problem 5
    def cut(self, A, D):
        """Compute the boolean mask that segments the image."""
        #get num rows and num columns in original image
        m = self.original.shape[0]
        n = self.original.shape[1]

        #get number of nodes
        N = A.shape[0] #m*n

        #get Laplacian of A
        L = lap(A)

        D_sqrt = sparse.diags(D**(-0.5))

        DLD = D_sqrt @ L @ D_sqrt

        #get second smallest eigenvector
        eig_vals, eig_vecs = eigsh(DLD, k=2, which="SM")
        eigv = eig_vecs.T[-1]
        assert len(eigv) == N

        eigv = eigv.reshape((m, n))
        mask = eigv > 0
        return mask


    # Problem 6
    def segment(self, r=5., sigma_B=.02, sigma_X=3.):
        """Display the original image and its segments."""
        A, D = self.adjacency()
        mask = self.cut(A, D)

        if self.type == 2: #if the original image is grayscale
            #plot original
            plt.subplot(1, 3, 1)
            plt.imshow(self.original, cmap="gray")
            plt.gca().set_title("Original")
            plt.axis("off")
            

            #plot positive segment
            plt.subplot(1, 3, 2)
            plt.imshow(self.original*mask, cmap="gray")
            plt.axis("off")
            plt.gca().set_title("Positive Segment")

            #plot negative segment
            plt.subplot(1, 3, 3)
            plt.imshow(self.original*~mask, cmap="gray")
            plt.axis("off")
            plt.gca().set_title("Negative Segment")
            
            plt.tight_layout()
            plt.show()

        else: #if the original image is color
            #Stack mask into 3-D array
            mask = np.dstack((mask, mask, mask))
            #plot original
            plt.subplot(1, 3, 1)
            plt.imshow(self.original)
            plt.gca().set_title("Original")
            plt.axis("off")

            #plot positive segment
            plt.subplot(1, 3, 2)
            plt.imshow(self.original*mask)
            plt.gca().set_title("Positive Segment")
            plt.axis("off")

            #plot negative segment
            plt.subplot(1, 3, 3)
            plt.imshow(self.original*~mask)
            plt.gca().set_title("Negative Segment")
            plt.axis("off")
            plt.show()




def test_laplacian():
    A1 = np.array([[0, 1, 0, 0, 1, 1],
                    [1, 0, 1, 0, 1, 0], 
                    [0, 1, 0, 1, 0, 0],
                    [0, 0, 1, 0, 1, 1],
                    [1, 1, 0, 1, 0, 0],
                    [1, 0, 0, 1, 0, 0]])

    Ans1 = np.array([[3, -1, 0, 0, -1, -1],
                        [-1, 3, -1, 0, -1, 0],
                        [0, -1, 2, -1, 0, 0],
                        [0, 0, -1, 3, -1, -1],
                        [-1, -1, 0, -1, 3, 0],
                        [-1, 0, 0, -1, 0, 2]])
    assert np.allclose(laplacian(A1), Ans1), "Laplacian failed"

    print("All tests passed for problem 1")

def test_connectivity():
    #test graphs from lab 
    A1 = np.array([[0, 1, 0, 0, 1, 1],
                    [1, 0, 1, 0, 1, 0], 
                    [0, 1, 0, 1, 0, 0],
                    [0, 0, 1, 0, 1, 1],
                    [1, 1, 0, 1, 0, 0],
                    [1, 0, 0, 1, 0, 0]])

    numComps, algCon = connectivity(A1)
    assert numComps == 1, "numComps should be 1"

    print("All tests passed for problem 2")

    A2 =  np.array([[0, 3, 0, 0, 0, 0],
                    [3, 0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 0, 0],
                    [0, 0, 1, 0, 2, .5],
                    [0, 0, 0, 2, 0, 1],
                    [0, 0, 0, .5, 1, 0]])

    numComps, algCon = connectivity(A2)
    assert numComps == 2, "numComps should be 2"

def testImageSegmenter():

    #test IS on dream.png

    #test constructor
    IS = ImageSegmenter("dream.png")
    assert IS.type == 3, "image should be color"
    assert IS.original.min() == 0.0
    assert IS.original.max() < 1
    assert len(IS.flattened.shape) == 1, "flattened should be 1 dimensional"

    #IS.show_original()

    #Test adjacency()
    A, D = IS.adjacency()
    assert type(A) == sparse.csc_matrix, "A should be a csc_matrix"
    assert(np.allclose(A[0].sum(), D[0])), "A and D are misaligned"
    assert(np.allclose(A[-1].sum(), D[-1])), "A and D are misaligned"

    #Test cut()
    mask = IS.cut(A, D)


    #test IS on dream_gray.png

    #test constructor
    IS = ImageSegmenter("dream_gray.png")
    assert IS.type == 2, "image should be gray"
    assert IS.original.min() >= 0.0
    assert IS.original.max() <= 1
    assert len(IS.flattened.shape) == 1, "flattened should be 1 dimensional"

    IS.show_original()

    #Test adjacency()
    A, D = IS.adjacency()
    assert(np.allclose(A[0].sum(), D[0])), "A and D are misaligned"
    assert(np.allclose(A[-1].sum(), D[-1])), "A and D are misaligned"

    #Test cut()
    mask = IS.cut(A, D)

    #Test segment()
    IS.segment()
   

    print("Passed all tests for Image Segmenter")

if __name__ == '__main__':
    #test_laplacian()
    #test_connectivity()
    testImageSegmenter()

#     ImageSegmenter("dream_gray.png").segment()
#     ImageSegmenter("dream.png").segment()
#     ImageSegmenter("monument_gray.png").segment()
#     ImageSegmenter("monument.png").segment()
