# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
Spencer Reschke
Math 345
11/5/2018
"""

# (Optional) Import functions from your QR Decomposition lab.
# import sys
# sys.path.insert(1, "../QR_Decomposition")
# from qr_decomposition import qr_gram_schmidt, qr_householder, hessenberg

import numpy as np
from matplotlib import pyplot as plt
from scipy import linalg as la
from cmath import sqrt
import cmath

# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    #grab shape of A
    m = A.shape[0]
    n = A.shape[1]

    #get reduced QR factorization
    Q, R = la.qr(A, mode="economic")

    #Make sure Q is mxn and R is nxn
    assert (Q.shape[0] == m and Q.shape[1] == n), "Q has the wrong shape"
    assert (R.shape[0] == n and R.shape[1] == n), "R has the wrong shape"

    #Calculate the solution and return it
    return la.solve_triangular(R, Q.T@b)

# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    #load data from file
    data = np.load("housing.npy")

    #Construct the matrix A and the vector b
    years = data[:, 0] #all rows in first column
    A = np.column_stack([years, np.ones(len(years))])
    b = data[:, 1] #all rows in second column

    #Find slope and y-intercept
    sol = least_squares(A, b)
    slope = sol[0]
    y_int = sol[1]

    #plot data points as a scatter plot
    plt.scatter(years, b, marker='o', label="Housing Data")
    plt.xlabel("Year since 2000")
    plt.ylabel("Price index")

    #plot least squares line with the scatter plot
    ls_line = np.array(list(map(lambda x: slope*x + y_int, years)))    
    plt.plot(years, ls_line, "r", label="Least Sqares Fit")  
    plt.legend()
    plt.title("Linear Least Squares Fit")
    plt.show()
    return


# Problem 3
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """

    #load data from file
    data = np.load("housing.npy")

    
    #Construct the vector b
    years = data[:, 0] #all rows in first column
    b = data[:, 1] #all rows in second column
    

    #constuct polynomials
    polynomials = []
    degrees = [3, 6, 9, 12]
    for degree in degrees:
        #construct the matrix A (equation 4.3)
        A = np.vander(years, degree+1)

        #solve least squares equations
        coeff = la.lstsq(A, b)[0]

        #construct the polynomial
        f = np.poly1d(coeff)

        polynomials.append(f)


    ##plot data points as a scatter plot
    #plt.scatter(years, b, marker='o', label="Housing Data")
    #plt.xlabel("Year since 2000")
    #plt.ylabel("Price index")

    #plot polynomials
    for f, color, degree, i in zip(polynomials, ["r", "b", "k", "y"], degrees, [1, 2, 3, 4]):
        plt.subplot(2, 2, i)
        plt.scatter(years, b, marker='o', label="Housing Data")
        plt.xlabel("Year since 2000")
        plt.ylabel("Price index")
        plt.plot(years, f(years), color, label="Degree {}".format(degree))
        plt.legend()

    #plt.title("Polynomial Least Squares Fit")
    #plt.legend()
    fig = plt.gcf()
    fig.tight_layout()
    plt.show()
    return


def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t, "r", label="Best Ellipse fit")
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    #load the data
    xk, yk = np.load("ellipse.npy").T

    #construct the matrix A and vector b
    A = np.column_stack([xk**2, xk, xk*yk, yk, yk**2])
    b = np.ones_like(xk)

    #get best fit parameters
    a, b, c, d, e = la.lstsq(A, b)[0]

    #plot ellipse
    plot_ellipse(a, b, c, d, e)

    #plot points
    plt.scatter(xk, yk, marker='o', label="Data Points")
    plt.title("Least Squares on an Ellipse")
    plt.legend()
    plt.show()
    return


# Problem 5
def power_method(A, N=20, tol=1e-12):
    """Compute the dominant eigenvalue of A and a corresponding eigenvector
    via the power method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The maximum number of iterations.
        tol (float): The stopping tolerance.

    Returns:
        (float): The dominant eigenvalue of A.
        ((n,) ndarray): An eigenvector corresponding to the dominant
            eigenvalue of A.
    """
    #get shape of A
    m = np.shape(A)[0]
    n = np.shape(A)[1]

    #Get a random vector of length n
    x0 = np.random.random(n)

    #normalize x0
    x0 = x0/la.norm(x0)

    x_cur = x0
    for k in range(1, N+1): #N iterations
        x_next = A @ x_cur
        x_next = x_next/la.norm(x_next)
        dif = la.norm(x_next - x_cur)
        x_cur = x_next
        if (dif < tol):            
            break
    x = x_cur
    return x.T @ (A @ x), x


# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
    """Compute the eigenvalues of A via the QR algorithm.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The number of iterations to run the QR algorithm.
        tol (float): The threshold value for determining if a diagonal S_i
            block is 1x1 or 2x2.

    Returns:
        ((n,) ndarray): The eigenvalues of A.
    """
    m = A.shape[0]
    n = A.shape[1]

    assert m==n, "A must be square"

    S = la.hessenberg(A)

    for k in range(N):
        Q, R = la.qr(S)
        S = R @ Q

    eigs = []
    i = 0
    print(S)
    while i < n:
        print(i)
        #check entry below diagonal
        if i == n-1 or abs(S[i+1, i]) < tol: #Si is 1x1
            eigs.append(S[i,i])
        else: #Si is 2x2
            print("2x2")
            a = S[i, i]
            b = S[i+1, i]
            c = S[i, i+1]
            d = S[i+1, i+1]
            l1 = ((a+d) + sqrt((a+d)**2 - 4*(1)*(a*d-b*c)))/2
            l2 = ((a+d) - sqrt((a+d)**2 - 4*(1)*(a*d-b*c)))/2
            eigs.append(l1)
            eigs.append(l2)
            i+=1
        i+=1
    return eigs




def test1():
    #test an example from the textbook
    A = np.array([[3.0, 1], 
                  [4.0, 1], 
                  [5.0, 1],
                  [6.0, 1]])

    b = np.array([7.3, 8.8, 11.1, 12.5])

    sol = least_squares(A, b)

    #check that solution is close to [1.79, 1.87]
    assert (abs(sol[0]-1.79) < 0.0001 and abs(sol[1]-1.87)<0.0001), "least_sqares failed"


def test5():
    n = 3
    A = np.random.random((n, n))
    dom, eig = power_method(A)
    assert np.allclose((A @ eig), (dom*eig)), "power_method failed"

    eigs, vecs = la.eig(A)

    loc = np.argmax(eigs)
    lamb, x, = eigs[loc], vecs[:, loc]
    assert np.allclose(A@x, lamb*x)

    assert cmath.isclose(dom, lamb, rel_tol=0.0000001), "power_method failed"

def test6():
    #Construct a random symmetric matrix
    n = 4
    B = np.random.randn(n, n)
    A = B + B.T

    eigs = qr_algorithm(A)
    eigs2, _ = la.eig(A)

    print(eigs)
    print(eigs2)



if __name__ == "__main__":
    #test1()
    #line_fit()
    #polynomial_fit()
    #ellipse_fit()
    #test5()
    #test6()
    pass

